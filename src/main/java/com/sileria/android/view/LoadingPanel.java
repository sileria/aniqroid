/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.view;

import android.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.*;
import android.widget.*;

import com.sileria.android.Resource;

/**
 * A generic class to be used as a loading panel or an empty view for a <code>ListView</code> class.
 * <p/>
 * You have ability to show a progress bar and a text message or only a text message which can be
 * used e.g. when data is being loaded for a list to display the wait cursor and a message and if
 * data was failed then only an error message.
 *
 * @author Ahmed Shakil
 * @date 4/29/12
 */
public class LoadingPanel extends LinearLayout {

	private TextView message;
	private ProgressBar progress;

	/**
	 * Factory method to create small style progress bar.
	 *
	 * @param context     The Context the view is running in, through which it can
	 *                    access the current theme, resources, etc
	 * @param orientation layout orientation. Pass HORIZONTAL or VERTICAL. Default value is HORIZONTAL
	 * @param gravity     layout gravity. Default value is CENTER
	 */
	public static LoadingPanel createSmall (Context context, int orientation, int gravity) {
		LoadingPanel panel = new LoadingPanel( context, R.attr.progressBarStyleSmall );
		panel.setOrientation( orientation );
		panel.setGravity( gravity );
		return panel;
	}

	/**
	 * Factory method to create default style progress bar.
	 *
	 * @param context     The Context the view is running in, through which it can
	 *                    access the current theme, resources, etc
	 * @param orientation layout orientation. Pass HORIZONTAL or VERTICAL. Default value is HORIZONTAL
	 * @param gravity     layout gravity. Default value is CENTER
	 */
	public static LoadingPanel createMedium (Context context, int orientation, int gravity) {
		LoadingPanel panel = new LoadingPanel( context, R.attr.progressBarStyle );
		panel.setOrientation( orientation );
		panel.setGravity( gravity );
		return panel;
	}

	/**
	 * Factory method to create large style progress bar.
	 *
	 * @param context     The Context the view is running in, through which it can
	 *                    access the current theme, resources, etc
	 * @param orientation layout orientation. Pass HORIZONTAL or VERTICAL. Default value is HORIZONTAL
	 * @param gravity     layout gravity
	 */
	public static LoadingPanel createLarge (Context context, int orientation, int gravity) {
		LoadingPanel panel = new LoadingPanel( context, R.attr.progressBarStyleLarge );
		panel.setOrientation( orientation );
		panel.setGravity( gravity );
		return panel;
	}

	/**
	 * Simple constructor to use when creating a view from code.
	 *
	 * @param context The Context the view is running in, through which it can
	 *                access the current theme, resources, etc.
	 */
	public LoadingPanel (Context context) {
		this( context, R.attr.progressBarStyleSmall );
	}

	/**
	 * Simple constructor to use when creating a view from code.
	 *
	 * @param context The Context the view is running in, through which it can
	 *                access the current theme, resources, etc.
	 */
	public LoadingPanel (Context context, int progressStyle) {
		super( context );
		createContent( context, progressStyle );
	}

	/**
	 * Constructor that is called when inflating a view from XML. This is called
	 * when a view is being constructed from an XML file, supplying attributes
	 * that were specified in the XML file.
	 *
	 * @param context The Context the view is running in, through which it can
	 *                access the current theme, resources, etc.
	 * @param attrs   The attributes of the XML tag that is inflating the view.
	 */
	public LoadingPanel (Context context, AttributeSet attrs) {
		super( context, attrs );
		createContent( context, R.attr.progressBarStyleSmall );
	}

	/**
	 * Create view contents.
	 */
	private void createContent (Context ctx, int progressStyle) {
		setId( R.id.empty );
		setGravity( Gravity.CENTER );

		// progress
		progress = progressStyle > 0 ? new ProgressBar( ctx, null, progressStyle ) : new ProgressBar( ctx );
		progress.setId( R.id.progress );
		addView( progress );

		// loading
		message = new TextView( ctx );
		message.setId( R.id.message );
		addView( message );
	}

	/**
	 * Set the progress bar text.
	 *
	 * @param text progress bar text
	 */
	public void setProgressText (String text) {
		message.setText( text );
	}

	/**
	 * Set the gap between the text and the progress bar.
	 */
	public void setTextPadding (int gap) {
		LinearLayout.LayoutParams params = (LayoutParams)message.getLayoutParams();
		if (getOrientation() == HORIZONTAL)
			params.setMargins( gap, 0, 0, 0 );
		else
			params.setMargins( 0, gap, 0, 0 );
	}

	/**
	 * Accessor to underlying <code>TextView</code> object.
	 *
	 * @return the TextView object.
	 */
	public TextView getTextView () {
		return message;
	}

	/**
	 * Accessor to underlying <code>ProgressBar</code> object.
	 *
	 * @return the ProgressBar object.
	 */
	public ProgressBar getProgressBar () {
		return progress;
	}

	/**
	 * Show a plain text message without the progress bar.
	 *
	 * @param text message to display
	 */
	public void showMessage (String text) {
		setProgress( text, false );
	}

	/**
	 * Show a plain text message without the progress bar.
	 *
	 * @param text message to display
	 */
	public void showMessage (int text) {
		setProgress( text, false );
	}

	/**
	 * Show a progress bar and specified text message.
	 *
	 * @param text progress message to display
	 */
	public void showProgress (String text) {
		setProgress( text, true );
	}

	/**
	 * Show a progress bar and specified text message.
	 *
	 * @param text progress message to display
	 */
	public void showProgress (int text) {
		setProgress( text, true );
	}

	/**
	 * Update the message and progressbar.
	 */
	private void setProgress (int text, boolean show) {
		setProgress( text == 0 ? null : Resource.getString( text ), show );
	}

	/**
	 * Update the message and progressbar.
	 */
	private void setProgress (String text, boolean show) {
		message.setText( text );
		progress.setVisibility( show ? View.VISIBLE : View.GONE );
	}

	/**
	 * Hide the progress bar.
	 */
	public void hideProgress () {
		progress.setVisibility( View.GONE );
	}
}
