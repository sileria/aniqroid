/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.graphics.*;
import android.text.TextPaint;

import com.sileria.util.ContentOptions;

/**
 * TextOptions.
 *
 * @author Ahmed Shakil
 * @date 12-27-2013
 */
public class TextOptions implements ContentOptions {

	private static final long serialVersionUID = 6340204578810309297L;

	public Typeface typeface;

	public float textSize;
	public int textColor;

	public Paint.Style style;
	public Paint.Align align;
	public float strokeWidth;

	public float shadowDx;
	public float shadowDy;
	public float shadowRadius;
	public int shadowColor;

	/**
	 * Can use an option paint object.
	 */
	public Paint paint;

	/**
	 * The resulting width of the bitmap.
	 */
	public int outWidth;

	/**
	 * The resulting height of the bitmap.
	 */
	public int outHeight;

	/**
	 * The resulting text bounds which maybe different than the actual
	 * bitmap's {@link #outWidth} and {@link #outWidth}, because bitmap
	 * in OpenGL needs to have a width and height that is a power of two.
	 */
	public Rect outBounds;

	/**
	 * If <code>true</code> then the result {@link #outBounds} will be
	 * based on the font's maximum height instead of the the text bounds.
	 */
	public boolean inFontHeight;

	/**
	 * Constructor, default.
	 */
	public TextOptions () {
	}

	/**
	 * Constructor, default.
	 * @param paint a Paint object to use.
	 *              NOTE: Because of Google's smart architectures the shadow attributes are hidden
	 *              in the <code>Paint</code> class, hence this constructor will not take the shadow
	 *              attributes into account when calculating the <code>equals()</code> or the
	 *              <code>hashCode()</code> for this options class when caching a texture bitmap.
	 *              You will have to explicitly call the {@link #setShadowLayer(float, float, float, int)}
	 *              if you want the shadow attributes to be taking in to account.
	 */
	public TextOptions (Paint paint) {
		textSize  = paint.getTextSize();
		textColor = paint.getColor();
		typeface  = paint.getTypeface();

		strokeWidth = paint.getStrokeWidth();
		align     = paint.getTextAlign();
		style     = paint.getStyle();
	}

	/**
	 * Construct options with attributes.
	 */
	public TextOptions (float textSize, int textColor, Typeface typeface) {
		this.textSize = textSize;
		this.textColor = textColor;
		this.typeface = typeface;
	}

	/**
	 * This draws a shadow layer below the main layer, with the specified
	 * offset and color, and blur radius. If radius is 0, then the shadow
	 * layer is removed.
	 */
	public void setShadowLayer (float radius, float dx, float dy, int color) {
		shadowRadius = radius;
		shadowDx = dx;
		shadowDy = dy;
		shadowColor = color;
	}

	/**
	 * Clear the shadow layer.
	 */
	public void clearShadowLayer () {
		setShadowLayer( 0, 0, 0, 0 );
	}

	/**
	 * Check if has a shadow layer.
	 */
	public boolean hasShadow () {
		return shadowRadius > 0.0f;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals (Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TextOptions that = (TextOptions)o;

		if (textColor != that.textColor) return false;
		if (Float.compare( that.textSize, textSize ) != 0) return false;
		if (typeface != null ? !typeface.equals( that.typeface ) : that.typeface != null) return false;

		if (align != that.align) return false;
		if (style != that.style) return false;
		if (Float.compare( that.strokeWidth, strokeWidth ) != 0) return false;

		if (inFontHeight != that.inFontHeight) return false;

		if (hasShadow() && ((TextOptions)o).hasShadow()) {
			if (Float.compare( that.shadowRadius, shadowRadius ) != 0) return false;
			if (Float.compare( that.shadowDx, shadowDx ) != 0) return false;
			if (Float.compare( that.shadowDy, shadowDy ) != 0) return false;
			if (shadowColor != that.shadowColor) return false;
		}

		return true;
	}

	@Override
	public int hashCode () {
		int result = typeface != null ? typeface.hashCode() : 0;
		result = 31 * result + (textSize != +0.0f ? Float.floatToIntBits( textSize ) : 0);
		result = 31 * result + textColor;
		result = 31 * result + (style != null ? style.hashCode() : 0);
		result = 31 * result + (align != null ? align.hashCode() : 0);
		result = 31 * result + (strokeWidth != +0.0f ? Float.floatToIntBits( strokeWidth ) : 0);
		result = 31 * result + (inFontHeight ? 1 : 0);
		if (hasShadow()) {
			result = 31 * result + Float.floatToIntBits( shadowRadius );
			result = 31 * result + (shadowDx != +0.0f ? Float.floatToIntBits( shadowDx ) : 0);
			result = 31 * result + (shadowDy != +0.0f ? Float.floatToIntBits( shadowDy ) : 0);
			result = 31 * result + shadowColor;
		}
		return result;
	}

	/**
	 * Convert to a paint object.
	 */
	public Paint toPaint () {
		Paint p = paint == null ? new TextPaint( TextPaint.ANTI_ALIAS_FLAG ) : new TextPaint( paint );
		if (textSize > 0) p.setTextSize( textSize );
		if (textColor != 0 ) p.setColor( textColor );
		if (typeface != null) p.setTypeface( typeface );
		if (strokeWidth > 0) p.setStrokeWidth( strokeWidth );
		if (style != null) p.setStyle( style );
		if (align != null) p.setTextAlign( align );
		if (hasShadow()) p.setShadowLayer( shadowRadius, shadowDx, shadowDy, shadowColor );
		return p;
	}

	/**
	 * Utility method to avoid boiler code.
	 */
	public static boolean hasOptions (TextOptions opt) {
		return opt != null && opt.textSize > 0 &&
				(( opt.textColor != 0)
						|| opt.typeface != null
						|| opt.style != null
						|| opt.align != null
						|| opt.strokeWidth > 0
						|| opt.hasShadow()
				);
	}
}
