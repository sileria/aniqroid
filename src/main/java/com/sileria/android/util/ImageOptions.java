/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import com.sileria.util.ContentOptions;

/**
 * ImageOptions is used to determine loading options for an image e.g. required width/height,etc...
 *
 * To use ImageOptions, you create an instance and assign values.
 * <p/>
 * <strong>Example 1: Subsample the original image</strong><br>
 * <pre>
 * {@code
 * ImageOptions opt = new ImageOptions();
 * opt.inSampleSize = 16;
 * Toolkit.getCache().get( url, preview, empty, opt );
 * }
 * </pre>
 *
 * <p/>
 * <strong>Example 2: Image Exact width/height</strong><br>
 * <pre>
 * {@code
 * ImageOptions opt = new ImageOptions();
 * opt.inExactHeight = 100;
 * opt.inExactWidth = 200;
 * Toolkit.getCache().get( url, preview, empty, opt );
 * }
 * </pre>
 *
 * <p/>
 * <strong>Example 3: Shadow Layer</strong><br>
 * <pre>
 * {@code
 * ImageOptions opt = new ImageOptions()
 * opt.setShadowLayer( 3, 2, 2, Color.BLACK )
 * Toolkit.getCache().get( url, preview, empty, opt );
 * }
 * </pre>
 *
 * <p/>
 * <strong>Example 4: Image Visibility</strong><br>
 * <pre>
 * {@code
 * ImageOptions opt = new ImageOptions()
 * opt.makeVisible = true;
 * Toolkit.getCache().get( url, preview, opt );
 * }
 * </pre>
 * <p/>
 *
 * @author Ahmed Shakil
 * @date 08-26-2012
 *
 * @see QueuedImageLoader
 * @see ImageLoader
 * @see CachedImageLoader
 */
public class ImageOptions implements ContentOptions {

	private static final long serialVersionUID = -4945136492051658717L;

	/**
	 * If set to a value > 1, requests the decoder to subsample the original
	 * image, returning a smaller image to save memory. The sample size is
	 * the number of pixels in either dimension that correspond to a single
	 * pixel in the decoded bitmap. For example, inSampleSize == 4 returns
	 * an image that is 1/4 the width/height of the original, and 1/16 the
	 * number of pixels. Any value <= 1 is treated the same as 1. Note: the
	 * decoder will try to fulfill this request, but the resulting bitmap
	 * may have different dimensions that precisely what has been requested.
	 * Also, powers of 2 are often faster/easier for the decoder to honor.
	 */
	public int inSampleSize;

	/**
	 * Desired sample width. This takes precedence over {@link #inSampleSize}
	 * Aspect ratio is preserved and the resulting image may not
	 * be of the desired inSampleWidth size. To load image in the exact
	 * size use {@link #inExactWidth} and {@link #inExactHeight}
	 */
	public int inSampleWidth;

	/**
	 * Desired sample height. This takes precedence over {@link #inSampleSize}
	 * Aspect ratio is preserved and the resulting image may not
	 * be of the desired inSampleWidth size. To load image in the exact
	 * size use {@link #inExactWidth} and {@link #inExactHeight}
	 */
	public int inSampleHeight;

	/**
	 * Required exact width. Image will be scaled to the exact width specified by this.
	 * NOTE: If {@link #inSampleSize} or {@link #inSampleWidth} are also set then
	 * this scaling will be applied after they are loaded in the sampled size.
	 *
	 * @see #inExactHeight
	 */
	public int inExactWidth;

	/**
	 * Required exact height. Image will be scaled to the exact width specified by this.
	 * NOTE: If {@link #inSampleSize} or {@link #inSampleWidth} are also set then
	 * this scaling will be applied after they are loaded in the sampled size.
	 */
	public int inExactHeight;

	/* Shadow Options */
	public float shadowDx;
	public float shadowDy;
	public float shadowRadius;
	public int shadowColor;
	public boolean shadowImage;

	/**
	 * Image filter
	 * NOTE: To use the filter with an image cache you MUST override the {@linkplain #hashCode()} method
	 * such that a <code>ImageFilter</code>for a unique url should always return the same hash code,
	 * otherwise the caching will not work properly.
	 */
	public ImageFilter filter;

	/**
	 * If loading is related to an <code>ImageView</code> and this flag
	 * is on, then it will make the image view visible after loading image.
	 */
	public boolean makeVisible;

	/**
	 * Constructor, default.
	 */
	public ImageOptions () {
	}

	/**
	 * Construct <code>ImageOptions</code> with specified dimensions.
	 * @param width required width for image being loaded
	 * @param height required height for image being loaded
	 */
	public ImageOptions (int width, int height) {
		this.inSampleWidth = width;
		this.inSampleHeight = height;
	}

	/**
	 * Construct <code>ImageOptions</code> specifying the subsampling size.
	 * @param inSampleSize subsample size
	 */
	public ImageOptions (int inSampleSize) {
		this.inSampleSize = inSampleSize;
	}

	/**
	 * This draws a shadow layer below the main layer, with the specified
	 * offset and color, and blur radius. If radius is 0, then the shadow
	 * layer is removed.
	 */
	public void setShadowLayer (float radius, float dx, float dy, int color) {
		setShadowLayer( radius, dx, dy, color, false );
	}

	/**
	 * This draws a shadow layer below the main layer, with the specified
	 * offset and color, and blur radius. If radius is 0, then the shadow
	 * layer is removed.
	 */
	public void setShadowLayer (float radius, float dx, float dy, int color, boolean imageShadow) {
		shadowRadius = radius;
		shadowDx = dx;
		shadowDy = dy;
		shadowColor = color;
		shadowImage = imageShadow;
	}

	/**
	 * Clear the shadow layer.
	 */
	public void clearShadowLayer () {
		setShadowLayer( 0, 0, 0, 0, false );
	}

	/**
	 * Check if has a shadow layer.
	 */
	public boolean hasShadow () {
		return shadowRadius > 0.0f;
	}

	/**
	 * Check if has post loading filters to be applied.
	 */
	public boolean hasFilters () {
		return inExactWidth > 0
				|| inExactHeight > 0
				|| hasShadow()
				|| filter != null;
	}

	/**
	 * Check if has any image loading options.
	 */
	public boolean hasImageOptions () {
		return (inSampleWidth > 0 && inSampleHeight > 0)
				|| inSampleSize > 1
				|| hasFilters();
	}

	/**
	 * Returns <code>true</code> if nothing is set; otherwise <code>false.</code>
	 */
	public boolean isEmpty () {
		return !hasImageOptions();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals (Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ImageOptions that = (ImageOptions)o;

		if (Math.max( 1, inSampleSize ) != Math.max( 1, that.inSampleSize)) return false;
		if (inSampleWidth != that.inSampleWidth) return false;
		if (inSampleHeight != that.inSampleHeight) return false;

		if (hasShadow() && ((ImageOptions)o).hasShadow()) {
			if (Float.compare( that.shadowRadius, shadowRadius ) != 0) return false;
			if (Float.compare( that.shadowDx, shadowDx ) != 0) return false;
			if (Float.compare( that.shadowDy, shadowDy ) != 0) return false;
			if (shadowColor != that.shadowColor) return false;
			if (shadowImage != that.shadowImage) return false;
		}

		if (filter != null ? !filter.equals( that.filter ) : that.filter != null) return false;

		return inExactWidth == that.inExactWidth && inExactHeight == that.inExactHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode () {
		int result = Math.max( 1, inSampleSize );
		result = 31 * result + inSampleWidth;
		result = 31 * result + inSampleHeight;
		result = 31 * result + inExactWidth;
		result = 31 * result + inExactHeight;

		if (hasShadow()) {
			result = 31 * result + Float.floatToIntBits( shadowRadius );
			result = 31 * result + (shadowDx != +0.0f ? Float.floatToIntBits( shadowDx ) : 0);
			result = 31 * result + (shadowDy != +0.0f ? Float.floatToIntBits( shadowDy ) : 0);
			result = 31 * result + shadowColor;
			result = 31 * result + (shadowImage ? 1 : 0);
		}
		else
			result += 31;

		result = 31 * result + (filter != null ? filter.hashCode() : 0);

		return result;
	}

	/**
	 * Utility method to avoid boiler code.
	 */
	public static boolean hasOptions (ImageOptions opt) {
		return opt != null && opt.hasImageOptions();
	}

	/**
	 * Utility method to avoid boiler code.
	 */
	public static boolean isEmpty (ImageOptions opt) {
		return opt == null || opt.isEmpty();
	}
}
