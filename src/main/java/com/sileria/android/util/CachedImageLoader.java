/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.*;
import android.widget.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sileria.android.Resource;
import com.sileria.util.*;

/**
 * Image loader that uses {@link ImageCache} implementation.
 * <ul>
 * <li>Images are loading from provided URLs and cached internally.</li>
 * <li>The URL will be used as the key to fetch these cached images from the cache.</li>
 * <li>Image can be a <code>Bitmap</code> or <code>Drawable</code></li>
 * <li>Callbacks can be either {@link BitmapCallback}, {@link BitmapCallback} or a {@link ImageView}</li>
 * <li><code>ImageView</code> callback is optimized for use with image views in any layout specially {@link ListView}s</li>
 * <li>Call one of the <code>get</code> methods in the class to start loading and caching images</li>
 * <li>You can call one of <code>cancel</code> methods to cancel a previous fetch request</li>
 * </ul>
 * <p/>
 * NOTE: ALL THE METHOD CALLS TO THIS CLASS MUST TAKE PLACE IN UI THREAD.
 * <p/>
 * Usage: Simply create and save an instance of this class and call one of the get methods to start getting results.
 * This class is not designed to have many instances. In usual case there should be one instance per app instance.
 * <p/>
 * By default the class uses {@link MemCache} but you can provide {@link DiskCache} or the {@link HybridCache} to
 * the constructor of this class. You can also provide any other {@link Cache} implementation that you like.
 *
 * @author Ahmed Shakil
 * @date Jul 1, 2012
 */
public class CachedImageLoader {

	protected final Cache<String, Bitmap> cache;
	private final LoaderQueue loader;

	private final Map<Content<Bitmap, ImageOptions>, Set<ContentCallback<?, ImageOptions>>> listeners = new HashMap<>();
	private final Map<View, ViewCallback> viewmap = new HashMap<>();   // map image view with ViewCallback object.

	private Set<String> cacheAsync;   // set containing keys for the async writes in progress to the cache

	private Drawable emptyImage;
	private Drawable brokenImage;

	private boolean cacheThread;
	private boolean makeVisible;
	private boolean emptyBeforeLoad = true;

	private boolean started;

	private ExecutorService cacheExec;

	/**
	 * Constructor, default.
	 */
	public CachedImageLoader () {
		this( new ImageCache() );
	}

	/**
	 * Constructor, default.
	 */
	public CachedImageLoader (int poolSize) {
		this( new ImageCache() );
		setPoolSize( poolSize );
	}

	/**
	 * Construct a loader class with your own <code>ImageCache</code> object.
	 */
	public CachedImageLoader (Cache<String, Bitmap> cache) {
		this.cache = cache;
		this.loader = createLoaderQueue();
		this.cacheThread = cache.isPhysical();
	}

	/**
	 * Create the queued image loader.
	 */
	protected LoaderQueue createLoaderQueue () {
		return new LoaderQueue();
	}

	/**
	 * Create the bitmap drawable loader.
	 */
	protected ContentLoader<Bitmap, ImageOptions> createCacheLoader (Handler handler, Content<Bitmap, ImageOptions> request, ContentCallback<Bitmap, ImageOptions> callback) {
		return new CacheLoader( handler, request, callback );
	}

	/**
	 * Place holder image that will be used with an <code>ImageView</code> before a load call
	 * is made or after an image load fails.
	 * <p/>
	 * By default <code>null</code> will be set to the ImageView.
	 *
	 * @param empty place holder image
	 */
	public void setEmptyImage (Drawable empty) {
		emptyImage = empty;
	}

	/**
	 * Place holder image that will be used with an <code>ImageView</code> when an image load fails.
	 * <p/>
	 * By default <code>null</code> will be set to the ImageView.
	 *
	 * @param broken place holder image
	 */
	public void setBrokenImage (Drawable broken) {
		brokenImage = broken;
	}

	/**
	 * Get the current pool size.
	 */
	public int getPoolSize () {
		return loader.getPoolSize();
	}

	/**
	 * Set the pool size to specify maximum number of image loaders to work concurrently.
	 * Default value is 2 meaning contents will be loaded serially (2 simultaneously)
	 * <p/>
	 * Note: Once the cache system is active, meaning images loading has started then
	 * this method will not take any affect.
	 *
	 * @param poolSize pool size. Cannot be less than 1.
	 * @see QueuedContentLoader#setPoolSize(int)
	 */
	public void setPoolSize (int poolSize) {
		if (started)
			throw new IllegalStateException( "Cannot call this method after image loading has started." );

		loader.setPoolSize( poolSize );
	}

	/**
	 * Checks the visibility flag for the <code>ImageView</code> on load.
	 * If the image view is not visible then it will make it visible.
	 *
	 * @param b <code>true</code> to check visibility, <code>false</code> to ignore.
	 */
	public void setShowViewOnLoad (boolean b) {
		makeVisible = b;
	}

	/**
	 * Reset the image to the empty image before loading.
	 * Default value is: <code>true</code>
	 *
	 * @param b <code>true</code> to set empty, <code>false</code> to ignore.
	 */
	public void setEmptyViewBeforeLoad (boolean b) {
		emptyBeforeLoad = b;
	}

	/**
	 * Sets the caching mechanism for disk caches to be asynchronous.
	 * This flag defaults to <code>true</code> for disk caches and to <code>false</code> for in memory caches.
	 * You can disable it (for any reason) by setting it to <code>false</code>.
	 * <p/>
	 * Note: For memory-only cache this flag does not take affect and caching
	 * will always be done synchronously and not in a separate thread.
	 * <p/>
	 * Note: Once the cache system is active, meaning images loading has started then
	 * this method will not take any affect.
	 */
	public void setSlowCacheThreaded (boolean b) {
		if (started)
			throw new IllegalStateException( "Cannot call this method after image loading has started." );

		cacheThread = b && cache.isPhysical();
	}

	/**
	 * Fetch a <code>Bitmap</code> image from specified URL and notify the <code>callback</code>.
	 * If image from the url was cached then the cached image will be sent on the callback.
	 *
	 * @param url      URL to download the image from
	 * @param callback listener that gets triggered on success or failure
	 *
	 * @return <code>true</code> if call was enqueued; otherwise <code>false</code>
	 */
	public boolean get (String url, BitmapCallback callback) {
		return enqueue( new Content<Bitmap, ImageOptions>( url ), callback );
	}

	/**
	 * Fetch a <code>Bitmap</code> image from specified URL and notify the <code>callback</code>.
	 * If image from the url was cached then the cached image will be sent on the callback.
	 *
	 * @param url      URL to download the image from
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 * @param callback listener that gets triggered on success or failure
	 *
	 * @return <code>true</code> if call was enqueued; otherwise <code>false</code>
	 */
	public boolean get (String url, ImageOptions opt, BitmapCallback callback) {
		return enqueue( new Content<Bitmap, ImageOptions>( url, opt ), callback );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and notify the <code>callback</code>.
	 * If image from the url was cached then the cached image will be sent on the callback.
	 *
	 * @param url      URL to download the image from
	 * @param callback listener that gets triggered on success or failure
	 *
	 * @return <code>true</code> if call was enqueued; otherwise <code>false</code>
	 */
	public boolean get (String url, ImageCallback callback) {
		return enqueue( new Content<Bitmap, ImageOptions>( url ), callback );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and notify the <code>callback</code>.
	 * If image from the url was cached then the cached image will be sent on the callback.
	 *
	 * @param url      URL to download the image from
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 * @param callback listener that gets triggered on success or failure
	 *
	 * @return <code>true</code> if call was enqueued; otherwise <code>false</code>
	 */
	public boolean get (String url, ImageOptions opt, ImageCallback callback) {
		return enqueue( new Content<Bitmap, ImageOptions>( url, opt ), callback );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 */
	public void get (String url, ImageView callback) {
		get( url, callback, emptyImage, brokenImage, null, null, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 */
	public void get (String url, ImageSwitcher callback) {
		get( url, callback, emptyImage, brokenImage, null, null, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it as as the background for
	 * the specified <code>View</code> object. No callback notifications are sent.
	 * If image from the url was cached then the background will be set on the View and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 */
	public void get (String url, View callback) {
		get( url, callback, emptyImage, brokenImage, null, null, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param empty    empty drawable to be set before the loading starts and in case of failure
	 */
	public void get (String url, ImageView callback, Drawable empty) {
		get( url, callback, empty, brokenImage, null, null, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param empty    empty drawable to be set before the loading starts and in case of failure
	 */
	public void get (String url, ImageSwitcher callback, Drawable empty) {
		get( url, callback, empty, brokenImage, null, null, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param progress View to show or hide while image is being loaded (e.g. a <code>ProgressBar</code>)
	 */
	public void get (String url, ImageView callback, View progress) {
		get( url, callback, emptyImage, brokenImage, progress, null, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param progress View to show or hide while image is being loaded (e.g. a <code>ProgressBar</code>)
	 */
	public void get (String url, ImageSwitcher callback, View progress) {
		get( url, callback, emptyImage, brokenImage, progress, null, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 */
	public void get (String url, ImageView callback, ImageOptions opt) {
		get( url, callback, emptyImage, brokenImage, null, opt, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 */
	public void get (String url, ImageSwitcher callback, ImageOptions opt) {
		get( url, callback, emptyImage, brokenImage, null, opt, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param cb2      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	public void get (String url, ImageView callback, BitmapCallback cb2) {
		get( url, callback, emptyImage, brokenImage, null, null, cb2 );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param cb2      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	public void get (String url, ImageSwitcher callback, BitmapCallback cb2) {
		get( url, callback, emptyImage, brokenImage, null, null, cb2 );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param empty    empty drawable to be set before the loading starts and in case of failure
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 */
	public void get (String url, ImageView callback, Drawable empty, ImageOptions opt) {
		get( url, callback, empty, brokenImage, null, opt, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param empty    empty drawable to be set before the loading starts and in case of failure
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 */
	public void get (String url, ImageSwitcher callback, Drawable empty, ImageOptions opt) {
		get( url, callback, empty, brokenImage, null, opt, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param progress View to show or hide while image is being loaded (e.g. a <code>ProgressBar</code>)
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 */
	public void get (String url, ImageView callback, View progress, ImageOptions opt) {
		get( url, callback, emptyImage, brokenImage, progress, opt, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param progress View to show or hide while image is being loaded (e.g. a <code>ProgressBar</code>)
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 */
	public void get (String url, ImageSwitcher callback, View progress, ImageOptions opt) {
		get( url, callback, emptyImage, brokenImage, progress, opt, null );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param empty    empty drawable to be set before the loading starts
	 * @param broken   drawable representing a broken link in terms of failure
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 * @param cb2      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	public void get (String url, ImageView callback, Drawable empty, Drawable broken, ImageOptions opt, BitmapCallback cb2) {
		get( url, callback, empty, broken, null, opt, cb2 );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback view that gets updated on success or failure
	 * @param empty    empty drawable to be set before the loading starts
	 * @param progress View to show or hide while image is being loaded (e.g. a <code>ProgressBar</code>)
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 * @param cb2      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	public void get (String url, ImageView callback, Drawable empty, View progress, ImageOptions opt, BitmapCallback cb2) {
		get( url, callback, empty, brokenImage, progress, opt, cb2 );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback listener that gets triggered on success or failure
	 * @param empty    empty drawable to be set before the loading starts
	 * @param broken   drawable representing a broken link in terms of failure
	 * @param progress View to show or hide while image is being loaded (e.g. a <code>ProgressBar</code>)
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 * @param cb2      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	public void get (String url, ImageView callback, Drawable empty, Drawable broken, View progress, ImageOptions opt, BitmapCallback cb2) {
		load( url, callback, empty, broken, progress, opt, cb2, imageViewAdapter );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback listener that gets triggered on success or failure
	 * @param empty    empty drawable to be set before the loading starts
	 * @param broken   drawable representing a broken link in terms of failure
	 * @param progress View to show or hide while image is being loaded (e.g. a <code>ProgressBar</code>)
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 * @param cb2      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	public void get (String url, ImageSwitcher callback, Drawable empty, Drawable broken, View progress, ImageOptions opt, BitmapCallback cb2) {
		load( url, callback, empty, broken, progress, opt, cb2, imageSwitcherAdapter );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it as as the background for
	 * the specified <code>View</code> object.
	 *
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback listener that gets triggered on success or failure
	 * @param empty    empty drawable to be set before the loading starts
	 * @param broken   drawable representing a broken link in terms of failure
	 * @param progress View to show or hide while image is being loaded (e.g. a <code>ProgressBar</code>)
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 * @param cb2      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	public void get (String url, View callback, Drawable empty, Drawable broken, View progress, ImageOptions opt, BitmapCallback cb2) {
		load( url, callback, empty, broken, progress, opt, cb2, imageBackgroundAdapter );
	}

	/**
	 * Fetch a <code>Drawable</code> image from specified URL and set it on the <code>ImageView</code>
	 * being used as <code>callback</code>. No callback notifications are sent.
	 * If image from the url was cached then the cached image will be set on the ImageView and method
	 * will return right away.
	 *
	 * @param url      URL to download the image from
	 * @param callback listener that gets triggered on success or failure
	 * @param empty    empty drawable to be set before the loading starts
	 * @param broken   drawable representing a broken link in terms of failure
	 * @param progress View to show or hide while image is being loaded (e.g. a <code>ProgressBar</code>)
	 * @param opt      Extra image options defined in <code>ImageOptions</code> class, this can be null.
	 * @param cb2      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	protected <V extends View> void load (String url, V callback, Drawable empty, Drawable broken, View progress, ImageOptions opt, BitmapCallback cb2, ViewAdapter<V> adapter) {
		if (callback == null) return;

		// check if the image view is already attached to same url being loaded
		ViewCallback<?> cb = viewmap.get( callback );
		if (cb != null && Utils.equals( cb.url, url )) {
			//adapter.setImage( callback, empty );
			return;
		}

		cancel( callback );    // cancel any previous callback

		// if empty url then set empty image and return
		if (Utils.isEmpty( url )) {
			if (progress != null && progress.getVisibility() != View.GONE)
				progress.setVisibility( View.GONE );
			adapter.setImage( callback, empty );
			return;
		}

		broken = Utils.defaultIfNull( broken, empty );
		cb = adapter.createCallback(url, callback, broken, progress, opt, cb2 );
		// if cached image in memory then set image and return
		// (ONLY do this when image is in memory otherwise load in bg)
		// (if not loaded in bg then the scroll views gets very sloppy)
		if (cache.loaded( cb.key )) {
			///Benchmark.getAniqroid().startCacheTime();
			Bitmap img = cache.get( cb.key );
			///Benchmark.getAniqroid().stopCacheTime();
			if (img != null) {
				adapter.setImage( callback, img, progress, opt );
				if (cb2 != null)
					cb2.onContentLoad( new Content<>( img, url, opt ) );
				return;
			}
		}

		// reset before loading.
		if (emptyBeforeLoad)
			adapter.setImage( callback, empty );

		// show the progress when present
		if (progress != null && progress.getVisibility() != View.VISIBLE)
			progress.setVisibility( View.VISIBLE );

		// enqueue url loading and callback
		if (enqueue( new Content<Bitmap, ImageOptions>( url, opt ), cb )) {
			viewmap.put( callback, cb );     // add to to image view map
		}
	}

	/**
	 * Set the image drawable as well as check visibility of the view.
	 */
	private void setImage (final ImageView view, final Bitmap img, View progress, ImageOptions opt) {
		if (progress != null && progress.getVisibility() != View.GONE)
			progress.setVisibility( View.GONE );

		if (view == null) return;

		view.setImageBitmap( img );

		boolean showView = opt == null ? makeVisible : makeVisible | opt.makeVisible;
		if (showView && view.getVisibility() != View.VISIBLE)
			view.setVisibility( View.VISIBLE );
	}

	/**
	 * Set the image drawable as well as check visibility of the view.
	 */
	private void setImage (final ImageSwitcher view, final Bitmap img, View progress, ImageOptions opt) {
		if (progress != null && progress.getVisibility() != View.GONE)
			progress.setVisibility( View.GONE );

		if (view == null) return;

		view.setImageDrawable( new BitmapDrawable( Resource.getResources(), img ) );

		boolean showView = opt == null ? makeVisible : makeVisible | opt.makeVisible;
		if (showView && view.getVisibility() != View.VISIBLE)
			view.setVisibility( View.VISIBLE );
	}

	/**
	 * Set the image drawable as well as check visibility of the view.
	 */
	private void setBackground (final View view, final Bitmap img, View progress, ImageOptions opt) {
		if (progress != null && progress.getVisibility() != View.GONE)
			progress.setVisibility( View.GONE );

		if (view == null) return;

		view.setBackground( new BitmapDrawable( Resource.getResources(), img ) );

		boolean showView = opt == null ? makeVisible : makeVisible | opt.makeVisible;
		if (showView && view.getVisibility() != View.VISIBLE)
			view.setVisibility( View.VISIBLE );
	}

	/**
	 * Just download and cache the image specified by the url without any callbacks.
	 *
	 * @param url URL of the image to cache
	 * @return <code>true</code> if already cached; otherwise <code>false</code>
	 */
	public boolean cache (String url) {
		return cache( url, null );
	}

	/**
	 * Just cache the image specified by the url without any callbacks.
	 *
	 * @param url URL of the image to cache
	 * @return <code>true</code> if already cached; otherwise <code>false</code>
	 */
	public boolean cache (String url, ImageOptions opt) {
		Content<Bitmap, ImageOptions> key = new Content<>( url, opt );
		String cacheKey = ImageCache.toHashKey( key );
		return cache.contains( cacheKey ) || enqueue( key, null );
	}

	/**
	 * Download only to physical cache only. If no physical cache is associated
	 * to this loader, then nothing happens.
	 *
	 * @param url URL of the image to download
	 * @return <code>true</code> if already cached; otherwise <code>false</code>
	 */
	public boolean download (String url) {
		return cache( url, null );
	}

	/**
	 * Synhronously fetch the cached <code>Bitmap</code> immediately.
	 */
	public Bitmap fetch (String url) {
		return fetch( url, null );
	}

	/**
	 * Synhronously fetch the cached <code>Bitmap</code> immediately.
	 */
	public Bitmap fetch (String url, ImageOptions opt) {
		Content<Bitmap, ImageOptions> key = new Content<>( url, opt );
		String cacheKey = ImageCache.toHashKey( key );
		return cache.get( cacheKey );
	}

	/**
	 * Clear the image cache.
	 */
	public void clear () {
		cache.clear();
	}

	/**
	 * Add the specified url and callback to the download queue.
	 *
	 * @param request  content key to download
	 * @param callback listener that gets triggered on success or failure
	 */
	protected boolean enqueue (Content<Bitmap, ImageOptions> request, ContentCallback<?, ImageOptions> callback) {
		if (!started) start();

		if (request == null || Utils.isEmpty( request.key )) return false;

		// check for existing callbacks.
		Set<ContentCallback<?, ImageOptions>> set = listeners.get( request );
		if (set == null) {
			listeners.put( request, set = new HashSet<>( 1 ) );
		}
		else if (set.contains( callback )) {
			return false;
		}

		if (set.isEmpty())                                      // no need to re-queue
			loader.enqueue( request.key, request.options );     // add to download queue

		if (callback != null)                                   // allow null callback but do not register them
			set.add( callback );
		return true;
	}

	/**
	 * Internal thread starting.
	 */
	private void start () {

		if (cacheThread) {
			cacheExec = Executors.newFixedThreadPool( loader.getPoolSize() );
			cacheAsync = Collections.synchronizedSet( new HashSet<String>() );
		}

		loader.start();
		started = true;
	}

	/**
	 * Common remove implementation.
	 */
	protected void remove (String url, ImageOptions opt, ContentCallback<?, ImageOptions> callback) {
		if (Utils.isEmpty( url )) return;

		Content<Bitmap, ImageOptions> key = new Content<>( url, opt );

		Set<ContentCallback<?, ImageOptions>> set = listeners.get( key );
		if (!Utils.isEmpty( set )) {
			set.remove( callback );
		}

		if (Utils.isEmpty( set )) {
			loader.remove( key );
			listeners.remove( key );
		}
	}

	/**
	 * Cancel a image loading for specified <code>url</code>.
	 * NOTE: All callbacks for this <code>url</code> will be removed from being notified.
	 */
	public void cancel (String url, ImageOptions opt) {
		Content<Bitmap, ImageOptions> key = new Content<>( url, opt );
		loader.remove( key );
		listeners.remove( key );
	}

	/**
	 * Cancel a listener callback for specified <code>callback</code>.
	 * If no other listeners were registered for the url then the
	 * image loading will also be cancelled.
	 */
	public void cancel (ImageView callback) {
		cancel( (View)callback );
	}

	/**
	 * Cancel a listener callback for specified <code>callback</code>.
	 * If no other listeners were registered for the url then the
	 * image loading will also be cancelled.
	 */
	public void cancel (ImageSwitcher callback) {
		cancel( (View)callback );
	}

	/**
	 * Cancel a listener callback for specified <code>callback</code>.
	 * If no other listeners were registered for the url then the
	 * image loading will also be cancelled.
	 */
	protected void cancel (View callback) {
		if (callback == null) return;

		ViewCallback<?> cb = viewmap.remove( callback );
		if (cb != null) {
			remove( cb.url, cb.opt, cb );
			if (cb.progress != null && cb.progress.getVisibility() != View.GONE)
				cb.progress.setVisibility( View.GONE );
		}
	}

	/**
	 * Cancel a listener callback for specified <code>callback</code>.
	 * If no other listeners were registered for the url then the
	 * image loading will also be cancelled.
	 */
	public void cancel (String url, ImageOptions opt, BitmapCallback callback) {
		remove( url, opt, callback );
	}

	/**
	 * Get internal cache object.
	 */
	public Cache<String, Bitmap> cache () {
		return cache;
	}

	@Override
	public String toString () {
		return "CachedImageLoader{" +
				"loader=" + loader +
				", cacheExec=" + cacheExec +
				", listeners=" + listeners.size() +
				", viewmap=" + viewmap.size() +
				", started=" + started +
				'}';
	}

	/**
	 * Image view callback.
	 */
	protected abstract class ViewCallback<V extends View> implements BitmapCallback {

		protected String url, key;
		protected V view;
		protected Drawable empty;
		protected ImageOptions opt;
		protected BitmapCallback cb;
		protected View progress;

		//protected long ms = System.currentTimeMillis();

		public ViewCallback (V view) {
			this.view = view;
		}

		public ViewCallback (String url, V view, Drawable empty, View progress, ImageOptions opt, BitmapCallback cb) {
			this.url = url;
			this.view = view;
			this.empty = empty;
			this.opt = opt;
			this.key = ImageCache.toHashKey( url, opt );
			this.progress = progress;
			this.cb = cb;
		}

		@Override
		public boolean equals (Object o) {
			return this == o || !(o == null || getClass() != o.getClass()) && view.equals( o );
		}

		@Override
		public int hashCode () {
			return view.hashCode();
		}
	}

	/**
	 * Image view callback.
	 */
	protected class ImageViewCallback extends ViewCallback<ImageView> implements BitmapCallback {

		public ImageViewCallback (String url, ImageView view, Drawable empty, View progress, ImageOptions opt, BitmapCallback cb) {
			super( url, view, empty, progress, opt, cb );
		}

		/**
		 * This method is invoked on each content load.
		 */
		public void onContentLoad (Content<Bitmap, ImageOptions> content) {
			setImage( view, content.content, progress, content.options );
			viewmap.remove( view );

			if (cb != null)
				cb.onContentLoad( content );
		}

		/**
		 * This method is invoked for each content load failure.
		 */
		public void onContentFail (Content<Throwable, ImageOptions> error) {
			view.setImageDrawable( empty );
			viewmap.remove( view );

			if (cb != null)
				cb.onContentFail( error );
		}
	}

	/**
	 * Image view callback.
	 */
	protected class ImageSwitcherCallback extends ViewCallback<ImageSwitcher> implements BitmapCallback {

		public ImageSwitcherCallback (String url, ImageSwitcher view, Drawable empty, View progress, ImageOptions opt, BitmapCallback cb) {
			super( url, view, empty, progress, opt, cb );
		}

		/**
		 * This method is invoked on each content load.
		 */
		public void onContentLoad (Content<Bitmap, ImageOptions> content) {
			setImage( view, content.content, progress, content.options );
			viewmap.remove( view );

			if (cb != null)
				cb.onContentLoad( content );
		}

		/**
		 * This method is invoked for each content load failure.
		 */
		public void onContentFail (Content<Throwable, ImageOptions> error) {
			view.setImageDrawable( empty );
			viewmap.remove( view );

			if (cb != null)
				cb.onContentFail( error );
		}
	}

	/**
	 * Image view callback.
	 */
	protected class ImageBackgroundCallback extends ViewCallback<View> implements BitmapCallback {

		public ImageBackgroundCallback (String url, View view, Drawable empty, View progress, ImageOptions opt, BitmapCallback cb) {
			super( url, view, empty, progress, opt, cb );
		}

		/**
		 * This method is invoked on each content load.
		 */
		public void onContentLoad (Content<Bitmap, ImageOptions> content) {
			setBackground( view, content.content, progress, content.options );
			viewmap.remove( view );

			if (cb != null)
				cb.onContentLoad( content );
		}

		/**
		 * This method is invoked for each content load failure.
		 */
		public void onContentFail (Content<Throwable, ImageOptions> error) {
			view.setBackground( empty );
			viewmap.remove( view );

			if (cb != null)
				cb.onContentFail( error );
		}
	}

	/**
	 * Queued image loader class.
	 */
	protected class LoaderQueue extends QueuedContentLoader<Bitmap, ImageOptions> {

		/**
		 * Create the content loader that will be used to load the specific contents from a url or filename.
		 *
		 * @param request  content request
		 * @param callback class point to the queued content loader that must be used to as a delegate
		 * @return ContentLoader implementation for your specific content type
		 */
		@Override
		protected ContentLoader<Bitmap, ImageOptions> createLoader (Handler handler, Content<Bitmap, ImageOptions> request, ContentCallback<Bitmap, ImageOptions> callback) {
			return createCacheLoader( handler, request, callback );
		}

		/**
		 * Image loaded successfully .
		 *
		 * @param result Content that was loaded
		 */
		@Override
		public void onContentLoad (Content<Bitmap, ImageOptions> result) {

			// check if any listeners
			Set<ContentCallback<?, ImageOptions>> set = listeners.get( result );
			if (set == null) return;

			Bitmap bitmap = result.content;
			Content<Bitmap, ImageOptions> bmpContent = null;
			Content<Drawable, ImageOptions> imgContent = null;

			// notify all listeners.
			for (ContentCallback<?, ImageOptions> cb : set) {
				if (cb instanceof BitmapCallback) {
					if (bmpContent == null)
						bmpContent = new Content<>( bitmap, result.id, result.key, result.options );
					((BitmapCallback)cb).onContentLoad( bmpContent );
				}
			}

			set.clear();
			listeners.remove( result );
		}

		/**
		 * Image load failed.
		 *
		 * @param error error that occurred during the load
		 */
		@Override
		public void onContentFail (Content<Throwable, ImageOptions> error) {

			Set<ContentCallback<?, ImageOptions>> set = listeners.get( error );
			if (set == null) return;

			for (ContentCallback<?, ImageOptions> cb : set) {
				cb.onContentFail( error );
			}

			set.clear();
			listeners.remove( error );
		}
	}

	/**
	 * Bitmap drawable loader.
	 */
	protected class CacheLoader extends ContentLoader<Bitmap, ImageOptions> {

		/**
		 * Constructor, default.
		 */
		public CacheLoader (Handler handler, Content<Bitmap, ImageOptions> request, ContentCallback<Bitmap, ImageOptions> callback) {
			super( handler, request, callback );
		}

		/**
		 * Load bitmap in background thread from the specified URL address.
		 *
		 * @param url   URL address
		 * @param opt   ImageOptions
		 * @param tries the number of tries that has happened so far.
		 * @return Loaded Bitmap
		 * @throws java.io.IOException in case of IO exception.
		 */
		protected Bitmap loadContent (String url, ImageOptions opt, int tries) throws IOException {

			///Benchmark.getAniqroid().startCallbackTime( url );

			String cacheKey = ImageCache.toHashKey( url, opt );

			// check the cache one more time.
			///Benchmark.getAniqroid().startCacheTime();
			Bitmap img = cache.get( cacheKey );
			///Benchmark.getAniqroid().stopCacheTime();

			if (img != null)
				return img;

			// if cache support direct download, ask to cache for it
			if(cache.isPhysical() && opt == null) {
				URL location = new URL( url );
				///Benchmark.getAniqroid().startNetworkTime();
				InputStream in = location.openStream(); // no need for buffered stream
				try { cache.write(cacheKey, in); }
				finally { IO.close( in ); }
				///Benchmark.getAniqroid().stopNetworkTime();

				img = cache.get( cacheKey );
			}
			else {
				///Benchmark.getAniqroid().startNetworkTime();
				// load bitmap from url.
				img = new BitmapSerializer().loadImage( url, opt );
				///Benchmark.getAniqroid().stopNetworkTime();

				// add to cache if loaded
				if (img != null) {
					if (!cache.contains( cacheKey )) {
						if (cacheThread) {
							if (!cacheAsync.contains( cacheKey ))
								cacheExec.submit( new CacheWriter( cacheKey, img ) );
						}
						else {
							///Benchmark.getAniqroid().startWriteTime();
							cache.put( cacheKey, img );
							///Benchmark.getAniqroid().stopWriteTime();
						}
					}
				}
			}

			return img;
		}
	}

	/**
	 * Write to cache asynchronously.
	 */
	private class CacheWriter implements Runnable {

		private final String key;
		private final Bitmap img;

		public CacheWriter (String key, Bitmap img) {
			this.key = key;
			this.img = img;

			///Benchmark.getAniqroid().startWriteTime();

			cacheAsync.add( key );
		}

		public void run () {
			cache.put( key, img );
			cacheAsync.remove( key );
			///Benchmark.getAniqroid().stopWriteTime();
		}
	}

	private final ImageViewAdapter imageViewAdapter = new ImageViewAdapter();
	private final ImageSwitcherAdapter imageSwitcherAdapter = new ImageSwitcherAdapter();
	private final ImageBackgroundAdapter imageBackgroundAdapter = new ImageBackgroundAdapter();

	/**
	 * Image setter adapter class.
	 */
	private static abstract class ViewAdapter<T> {
		abstract void setImage (T view, Drawable image);
		abstract void setImage (T view, Bitmap image);
		abstract void setImage (T view, final Bitmap img, View progress, ImageOptions opt);
		abstract ViewCallback<?> createCallback (String url, T view, Drawable broken, View progress, ImageOptions opt, BitmapCallback cb2);
	}

	/**
	 * ImageView adapter implementation.
	 */
	private class ImageViewAdapter extends ViewAdapter<ImageView> {
		void setImage (ImageView view, Drawable image) {
			view.setImageDrawable( image );
		}

		void setImage (ImageView view, Bitmap image) {
			view.setImageBitmap( image );
		}

		@Override void setImage (ImageView view, Bitmap img, View progress, ImageOptions opt) {
			CachedImageLoader.this.setImage (view, img, progress, opt);
		}

		@Override public ViewCallback<?> createCallback (String url, ImageView view, Drawable broken, View progress, ImageOptions opt, BitmapCallback cb2) {
			return new ImageViewCallback( url, view, broken, progress, opt, cb2 );
		}
	}

	/**
	 * ImageSwitcher adapter implementation.
	 */
	private class ImageSwitcherAdapter extends ViewAdapter<ImageSwitcher> {
		void setImage (ImageSwitcher view, Drawable image) {
			view.setImageDrawable( image );
		}
		void setImage (ImageSwitcher view, Bitmap image) {
			view.setImageDrawable( new BitmapDrawable( Resource.getResources(), image ) );
		}

		@Override void setImage (ImageSwitcher view, Bitmap img, View progress, ImageOptions opt) {
			CachedImageLoader.this.setImage (view, img, progress, opt);
		}

		@Override public ViewCallback<?> createCallback (String url, ImageSwitcher view, Drawable broken, View progress, ImageOptions opt, BitmapCallback cb2) {
			return new ImageSwitcherCallback( url, view, broken, progress, opt, cb2 );
		}
	}

	/**
	 * View Background image implementation.
	 */
	private class ImageBackgroundAdapter extends ViewAdapter<View> {
		void setImage (View view, Drawable image) {
			view.setBackground( image );
		}
		void setImage (View view, Bitmap image) {
			view.setBackground( new BitmapDrawable( Resource.getResources(), image ) );
		}

		@Override void setImage (View view, Bitmap img, View progress, ImageOptions opt) {
			CachedImageLoader.this.setBackground(view, img, progress, opt);
		}

		@Override public ViewCallback<?> createCallback (String url, View view, Drawable broken, View progress, ImageOptions opt, BitmapCallback cb2) {
			return new ImageBackgroundCallback( url, view, broken, progress, opt, cb2 );
		}
	}
}
