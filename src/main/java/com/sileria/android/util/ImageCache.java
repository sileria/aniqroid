/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.graphics.Bitmap;

import java.io.File;

import com.sileria.android.bc.Wrapper;
import com.sileria.util.*;

/**
 * A simple memory cache for storing Bitmaps or Drawables in memory associated with a string id or a url.
 * <p/>
 * This class extends from <code>MemCache</code> meaning it uses strong references to the bitmaps and to
 * clear the cache we need to explicitly do it. This was done because in the past, a popular memory cache
 * implementation was a SoftReference or WeakReference bitmap cache, however this is not recommended in
 * Android any more, since starting Android 2.3 (API Level 9) the garbage collector is more aggressive with
 * collecting soft/weak references which makes them fairly ineffective. In addition, prior to Android 3.0 (API Level 11),
 * the backing data of a bitmap was stored in native memory which is not released in a predictable manner,
 * potentially causing an application to briefly exceed its memory limits and crash..
 * <p/>
 * Usage: Simply create and save an instance of this class and call one of the put/get methods to start caching and fetching images.
 * <p/>
 * Note: This class is not designed to have many instances. In usual case there should be one instance per app instance.
 *
 * @author Ahmed Shakil
 * @date 08-21-2012
 */
public class ImageCache extends MemCache<String, Bitmap> {

	/**
	 * Factory method to create and return instance of this <code>ImageCache</code> class.
	 * @param maxSize max cache size in bytes. If less than 1KB defaults to 1KB.
	 * @return an instance of this <code>ImageCache</code> class
	 */
	public static MemCache<String, Bitmap> createMemCache (int maxSize) {
		return new ImageCache( maxSize );
	}

	/**
	 * Factory method to create and return instance of this <code>DiskCache</code> class.
	 * @param maxSize max cache size in bytes. If less than 10KB defaults to 10KB.
	 * @return an instance of this <code>DiskCache</code> class
	 */
	public static DiskCache<String, Bitmap> createDiskCache (int maxSize) {
		return new DiskCache<>( Wrapper.getInstance().getCacheDir( "images" ), new BitmapSerializer(), maxSize );
	}

	/**
	 * Factory method to create and return instance of this <code>HybridCache</code> class.
	 * @param maxSoft max soft cache size in bytes. If less than 1KB defaults to 1KB.
	 * @param maxDisk max disk cache size in bytes. If less than 10KB defaults to 10KB.
	 * @return an instance of this <code>HybridCache</code> class
	 */
	public static HybridCache<String, Bitmap> createHybridCache (int maxSoft, int maxDisk) {
		return new HybridCache<>( maxSoft, maxDisk, Wrapper.getInstance().getCacheDir( "images" ), new BitmapSerializer(), new ImageByteCounter() );
	}

	/**
	 * Factory method to create and return instance of this <code>HybridCache</code> class.
	 * @param maxSoft max soft cache size in bytes. If less than 1KB defaults to 1KB.
	 * @param maxDisk max disk cache size in bytes. If less than 10KB defaults to 10KB.
	 * @return an instance of this <code>HybridCache</code> class
	 */
	public static HybridCache<String, Bitmap> createHybridCache (int maxSoft, int maxDisk, File cacheFolder) {
		return new HybridCache<>( maxSoft, maxDisk, cacheFolder, new BitmapSerializer(), new ImageByteCounter() );
	}

	/**
	 * Construct ImageCache object with default cache size of 1MB.
	 */
	public ImageCache () {
		this( MemCache.DEFAULT_MAXIMUM_BYTES );
	}

	/**
	 * Construct ImageCache object with specified cache size limit.
	 * @param maxSize max cache size in bytes. If less than 1KB defaults to 1KB.
	 */
	public ImageCache (int maxSize) {
		super( DEFAULT_INITIAL_CAPACITY, Math.max( IO.ONE_KB, maxSize ) );
		super.setByteCounter( new ImageByteCounter() );
	}

	/**
	 * Get the bitmap instance from the cache if previously cached; otherwise returns <code>null</code>.
	 *
	 * @param key String id or URL.
	 * @return a previously cached <code>Bitmap</code> or <code>null</code>
	 */
	@Override
	public Bitmap get (String key) {
		Bitmap bmp = super.get( key );
		return bmp != null && !bmp.isRecycled() ? bmp : null;
	}

	/**
	 * Checks to see if the cached contains a valid image for the specified key.
	 *
	 * @param key String id or URL.
	 * @return <code>true</code> if cached; otherwise <code>false</code>
	 */
	@Override
	public boolean contains (String key) {
		return get( key ) != null;
	}

	/**
	 * Set <code>ByteCounter</code> to calculate byte of each object.
	 */
	@Override
	public void setByteCounter (ByteCounter<Bitmap> bitmapDrawableByteCounter) {
		throw new UnsupportedOperationException( "This feature is supported for ImageCache." );
	}

	/**
	 * Generate unique hash for url/opt.
	 */
	public static String toHashKey (String url, ImageOptions opt) {
		if (ImageOptions.isEmpty( opt ))
			return url;

		int result = url != null ? url.hashCode() : 0;
		result = 31 * result + opt.hashCode();
		return Integer.toHexString( result );
	}

	/**
	 * Generate unique hash for url/opt.
	 */
	public static String toHashKey (Content<?, ImageOptions> key) {
		return toHashKey( key.key, key.options );
	}

	/**
	 * Default implementation of byte counter for a bitmap drawable.
	 */
	public static class ImageByteCounter implements ByteCounter<Bitmap> {

		public int sizeOf (Bitmap bmp) {
			return bmp == null ? 0 : bmp.getByteCount();
		}
	}
}
