/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.graphics.Bitmap;
import android.os.Handler;

import java.io.File;
import java.io.IOException;

import com.sileria.util.*;

/**
 * Similar to {@link CachedImageLoader} in functionality with only one difference, i.e:
 * The images are loading from local file system and not from a URL.
 *
 * NOTE: It maybe better to only use a soft cache for this kind of image loader,
 * since a local object does not need to be saved again to the a disk. But that is
 * allowed if you insist or have some special reasoning.
 *
 * NOTE: If you are using {@link com.sileria.util.HybridCache}, you can always
 * call {@link HybridCache#getSoftCache()} to pass only the memory cache to this
 * class's constructor.
 *
 * @author Ahmed Shakil
 * @date 02-25-2016
 */
public class FileImageLoader extends CachedImageLoader {

	/**
	 * Construct a default disk image loader cache.
	 */
	public FileImageLoader () {
	}

	public FileImageLoader (int poolSize) {
		super( poolSize );
	}

	public FileImageLoader (Cache<String, Bitmap> cache) {
		super( cache );
	}

	@Override
	protected ContentLoader<Bitmap, ImageOptions> createCacheLoader (Handler handler, Content<Bitmap, ImageOptions> request, ContentCallback<Bitmap, ImageOptions> callback) {
		return new DiskLoader( handler, request, callback );
	}

	/**
	 * Bitmap drawable loader.
	 */
	protected class DiskLoader extends ContentLoader<Bitmap, ImageOptions> {

		/**
		 * Constructor, default.
		 */
		public DiskLoader (Handler handler, Content<Bitmap, ImageOptions> request, ContentCallback<Bitmap, ImageOptions> callback) {
			super( handler, request, callback );
		}

		/**
		 * Load bitmap in background thread from the specified file <code>path</code>.
		 *
		 * @param path   file path
		 * @param opt   ImageOptions
		 * @param tries the number of tries that has happened so far.
		 * @return Loaded Bitmap
		 * @throws java.io.IOException in case of IO exception.
		 */
		protected Bitmap loadContent (String path, ImageOptions opt, int tries) throws IOException {

			String cacheKey = ImageCache.toHashKey( path, opt );

			// check the cache one more time.
			Bitmap img = cache.get( cacheKey );

			if (img != null)
				return img;

			// load image
			img = new BitmapSerializer().loadImage( new File( path ), opt );

			// add to cache if loaded
			if (img != null) {
				if (!cache.contains( cacheKey )) {
					cache.put( cacheKey, img );
				}
			}

			return img;
		}
	}

}
