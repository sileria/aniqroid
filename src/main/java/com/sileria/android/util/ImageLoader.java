/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.graphics.drawable.Drawable;

import java.io.IOException;

/**
 * ImageLoader class loads images from URLs in background thread in sequention order.
 * <p/>
 * <strong>Example</strong>:
 * <blockquote><pre>
 * public static Cancellable loadImages (ImageCallback callback, String... urls) {
 *     ImageLoader loader = new ImageLoader( callback );
 *     loader.execute( urls );
 *     return loader;
 * }
 * </pre></blockquote>
 * <strong>Note</strong>: Please do not launch too many instances of this image loader class. It will run out of memory
 * with too many instances running at the same time. For that purpose use {@link QueuedImageLoader}.
 *
 * @author Ahmed Shakil
 * @date Mar 27, 2010
 */
public class ImageLoader extends ContentTask<Drawable, ImageOptions> {

	/**
	 * Constructor, default.
	 * Note: This constructor must be called from UI thread.
	 */
	public ImageLoader () {
	}

	/**
	 * Constructor specifying your own handler instance and callback listener object.
	 * Note: This constructor must be called from UI thread.
	 */
	public ImageLoader (ImageCallback callback) {
		super( callback );
	}

	/**
	 * Constructor specifying your own handler instance and callback listener object.
	 */
	protected ImageLoader (ContentCallback<Drawable,ImageOptions> callback) {
		super( callback );
	}

	/**
	 * Load drawable image in background thread from the specified URL address.
	 *
	 * @param url URL address
	 * @param opt expects an {@link ImageOptions} implementation or <code>nulll</code>
	 * @param tries the number of tries that has happened so far
	 * @return Loaded Drawable object or <code>null</code> if not able to decode
	 *
	 * @throws IOException in case of IO exception.
	 */
	protected Drawable loadContent (String url, ImageOptions opt, int tries) throws IOException {
		return new ImageSerializer().loadImage( url, opt, tries );
	}
}
