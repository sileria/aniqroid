/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.os.AsyncTask;

import com.sileria.android.Kit;
import com.sileria.util.*;

/**
 * Abstract base adpater class to make AsyncTask work with {@link com.sileria.util.AsyncObserver} or its sub-classes.
 *
 * @see com.sileria.android.util.RemoteTask
 *
 * @author Ahmed Shakil
 * @date Mar 20, 2010
 */
public abstract class AbstractWorker<Params, Progress, Result, Err extends Throwable>
        extends AsyncTask<Params, Progress, Result>
        implements Cancellable, AsyncObserver<Result, Err> {

    protected AsyncObserver<Result, Err> callback;

	protected CompletionCallback<AbstractWorker<Params, Progress, Result, Err>> doneCallback;

    protected Err error;

	protected long millis;

	protected int retries;

	protected int retryMS = 1000;

	/**
     * Constructor, default.
	 * NOTE: this contructor will set itself as the callback.
	 * Unless you specify the callback instance in the constructor or setter method.
     */
    protected AbstractWorker () {
		this.callback = this;
    }

    /**
     * Constructor specifying the actuall callback.
     */
    protected AbstractWorker (AsyncObserver<Result, Err> callback) {
        this.callback = callback;
    }

	/**
	 * Set the callback listener.
	 */
	public void setCallback (AsyncObserver<Result, Err> callback) {
		this.callback = callback;
	}

	/**
	 * Set the task completion listener for this task.
	 */
	public void setCallback (CompletionCallback<AbstractWorker<Params, Progress, Result, Err>> callback) {
		this.doneCallback = callback;
	}

	/**
	 * Set the number of retries to attempt before in case of failure due to exception.
	 * @param retries number of retries. Default is 0.
	 * @param delay in milliseconds to retry. Negative value will set to default. Default is 1000ms.
	 */
	public void setRetries (int retries, int delay) {
		this.retries = Math.max( 0, retries );
		this.retryMS = delay < 0 ? 1000 : delay;
	}

	@Override
	protected void onProgressUpdate (Progress ... values) {
		if (callback instanceof ProgressCallback && !Utils.isEmpty( values )) {
			for (Progress p : values)
				((ProgressCallback<Result, Progress>)callback).onProgress( p );
		}
	}

	/**
     * Done
     */
    @Override
    protected void onPostExecute (Result result) {
        if (callback == null) return;

        if (error != null)
            callback.onFailure( error );
        else
            callback.onSuccess( result );

		if (doneCallback != null)
			doneCallback.onComplete( this );
    }

	/**
     * Cancel the request or a thread.
     * <p/>
     * Note: This method does not guarentee immediate
     * cancellation, but may take a while to effectively
     * cancel the request.
     */
    public void cancel () {
        cancel( true );
    }

	/**
	 * Callback method called when successful.
	 *
	 * @param result Result of type {@code <T>}
	 */
	public void onSuccess (Result result) {
	}

	/**
	 * Called in case of any error.
	 *
	 * @param e Throwable exception
	 */
	public void onFailure (Err e) {
	}

	/**
	 * Get the total time in milliseconds it took for the service to complete or fail.
	 * This method is not effective unless a subclass updates the {@link #millis} variable.
	 */
	public long getExecutionTime () {
		return millis;
	}

	/**
	 * The retry sleep common method.
	 * @return <code>true</code> if thread was interrupted; otherwise <code>false</code>
	 */
	protected boolean sleepBeforeRetry () {
		if (retryMS > 0) {
			try { Thread.sleep( retryMS ); }
			catch (InterruptedException ie) {
				Log.w( Kit.TAG, ie.getLocalizedMessage(), ie );
				return true;
			}
		}
		return false;
	}

}
