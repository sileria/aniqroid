/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.os.Handler;
import android.os.Looper;

import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import com.sileria.util.*;

/**
 * ContentLoader is a generic abstract class that builds in the functionality for loading
 * any kind of user defined content from a stream, file or a webservice in a serial or pooled mechanism.
 * <p/>
 * To extend simply override {@link #createLoader(android.os.Handler, com.sileria.util.Content, ContentCallback)}
 * <p/>
 * As an example check out the {@link com.sileria.android.util.QueuedImageLoader} implementation.
 *
 * @author Ahmed Shakil
 * @date Jul 1, 2012
 *
 * @param <T> content type of any kind.
 * @param <O> ContentOption type
 */
public abstract class QueuedContentLoader<T, O extends ContentOptions> implements ContentCallback<T,O>, Cancellable {

	private ContentCallback<T,O> callback;

	private ThreadPoolExecutor executor;
	private final Map<Content<T,O>, Future<?>> tasks = new ConcurrentHashMap<>();

	private final Handler handler = new Handler( Looper.getMainLooper() );

	private volatile boolean running;

	private int poolSize = DEFAULT_POOL_SIZE;
	private int queueSize = DEFAULT_QUEUE_SIZE;

	private static int DEFAULT_POOL_SIZE = 7;
	private static int DEFAULT_QUEUE_SIZE = 1000;
	private static int DEFAULT_THREAD_PRIORITY = Thread.NORM_PRIORITY - 1;

	/**
	 * Set the pool size for all content loaders.
	 * Default value is 1 meaning contents will be loaded serially.
	 * @param poolSize pool size. Cannot be less than 1.
	 */
	public static void setDefaultPoolSize (int poolSize) {
		DEFAULT_POOL_SIZE = Math.max( 1, poolSize );
	}

	/**
	 * Set the default queue size to for all content loaders.
	 * Default value is 1000.
	 * @param queueSize queue size. Cannot be less than 1.
	 */
	public static void setDefaultQueueSize (int queueSize) {
		DEFAULT_QUEUE_SIZE = Math.max( 1, queueSize );
	}

	/**
	 * Set the default thread priority for all content loaders.
	 * Default value is Thread.NORM_PRIORITY - 1.
	 * @param priority background thread priority
	 */
	public static void setDefaultThreadPriority (int priority) {
		DEFAULT_THREAD_PRIORITY = Utils.clamp( Thread.MIN_PRIORITY, Thread.MAX_PRIORITY, priority );
	}

	/**
	 * Constructor, default.
	 */
	protected QueuedContentLoader () {
		this( null );
	}

	/**
	 * Construct an image loader with image callback.
	 */
	protected QueuedContentLoader (ContentCallback<T,O> callback) {
		this.callback = callback;
	}

	/**
	 * Starts the queue loader. More items can be added
	 * to the queue after the task has been started.
	 * This method must be invoked on the UI thread.
	 */
	public void start () {
		if (running)
			throw new IllegalStateException( "Already running, call cancel() first." );

		executor = createExecutor( new LinkedBlockingQueue<Runnable>( queueSize ) );
		running = true;
	}

	/**
	 * Create your own executor service implemenation.
	 */
	protected ThreadPoolExecutor createExecutor (BlockingQueue<Runnable> queue) {
		return new ThreadPoolExecutor(poolSize, poolSize, 1, TimeUnit.SECONDS, queue, new BackgroundThreadFactory( getClass().getSimpleName() + "-thread-" ) );
	}

	/**
	 * Executes the task. The task returns itself (this) so that the caller
	 * can keep a reference to it. This method must be invoked on the UI thread.
	 *
	 * @return Returns itself (this) so that the caller can keep a reference to it
	 */
	public QueuedContentLoader execute () {
		start();
		return this;
	}

	/**
	 * Return instance of {@link BitmapLoader} class.
	 */
	protected abstract Runnable createLoader (Handler handler, Content<T, O> request, ContentCallback<T, O> callback);

	/**
	 * Add image url to load.
	 *
	 * @param url URL of the image
	 */
	public boolean enqueue (String url) {
		return enqueue( url, 0, null );
	}

	/**
	 * Add image url to load.
	 *
	 * @param url URL of the image
	 * @param id optional index or id for the image which will be sent to the {@link ContentCallback#onContentLoad(com.sileria.util.Content)}
	 *  pass zero to ignore the id.
	 */
	public boolean enqueue (String url, int id) {
		return enqueue( url, id, null );
	}

	/**
	 * Add image url to load.
	 *
	 * @param url URL of the image
	 */
	public boolean enqueue (String url, O opt) {
		return enqueue( url, 0, opt );
	}

	/**
	 * Add image url to load.
	 *
	 * @param url URL of the image
	 * @param id optional index or id for the image which will be sent to the {@link ContentCallback#onContentLoad(com.sileria.util.Content)}
	 *  pass zero to ignore the id.
	 */
	public boolean enqueue (String url, int id, O opt) {
		return enqueue( new Content<T, O>( id, url, opt ) );
	}

	/**
	 * Add image url to load.
	 */
	boolean enqueue (Content<T,O> request) {

		if (!running)
			throw new IllegalStateException( "You must start the loader first by calling start() or execute()." );

		if (request == null || Utils.isEmpty( request.key )) return false;

		Future<?> task = executor.submit( createLoader( handler, request, this ) );
		tasks.put( request, task );

		return true;
	}

	/**
	 * Cancel content downloads and remove them from the download queue.
	 * Each download will be stopped if it was running, and no callbacks
	 * will be received for this item.
	 */
	public void remove (String url, int id, O opt) {
		remove( new Content<T,O>(id, url, opt) );
	}

	/**
	 * Cancel content downloads and remove them from the download queue.
	 * Each download will be stopped if it was running, and no callbacks
	 * will be received for this item.
	 */
	public void remove (String url, O opt) {
		remove( new Content<T,O>(0, url, opt) );
	}

	/**
	 * Cancel content downloads and remove them from the download queue.
	 * Each download will be stopped if it was running, and no callbacks
	 * will be received for this item.
	 */
	public void remove (String ... urls) {
		for (String url : urls) {
			remove( new Content<T,O>(0, url, null) );
		}
	}

	/**
	 * Cancel content downloads and remove them from the download queue.
	 * Each download will be stopped if it was running, and no callbacks
	 * will be received for this item.
	 */
	protected void remove (Content<T, O> request) {
		Future<?> task = tasks.remove( request );
		if (task != null) {
			task.cancel( true );
		}
	}

	/**
	 * Get the current pool size.
	 */
	public int getPoolSize () {
		return poolSize;
	}

	/**
	 * Set the pool size to specify maximum number of image loaders to work concurrently.
	 * Default value is 7.
	 * @param poolSize pool size. Cannot be less than 1.
	 */
	public void setPoolSize (int poolSize) {

		this.poolSize = poolSize = Math.max( 1, poolSize );

		if (executor != null) {
			executor.setCorePoolSize( poolSize );
			executor.setMaximumPoolSize( poolSize );
		}
	}

	/**
	 * Get the current queue size.
	 */
	public int getQueueSize () {
		return queueSize;
	}

	/**
	 * Set the queue size to specify maximum requests to be enqueued.
	 * Default value is 100.
	 * NOTE: This method does not affect anything if called after start() or execute() call.
	 * @param queueSize queue size. Cannot be less than 1.
	 */
	public void setQueueSize (int queueSize) {
		this.queueSize = Math.max( 1, queueSize );
	}

	/**
	 * Image loaded successfully .
	 * @param content Content that was loaded
	 */
	public void onContentLoad (Content<T, O> content) {
		if (running && callback != null && tasks.containsKey( content ))
			callback.onContentLoad( content );
	}

	/**
	 * Image load failed.
	 * @param error that occured during the load
	 */
	public void onContentFail (Content<Throwable, O> error) {
		if (running && callback != null && tasks.containsKey( error ))
			callback.onContentFail( error );
	}

	/**
	 * Cancel all calls.
	 */
	public synchronized void cancel () {
		running = false;
		executor.shutdown();
	}

	@Override
	public String toString () {
		return "QueuedContentLoader{" +
				"executor=" + executor +
				", running=" + running +
				", poolSize=" + poolSize +
				", queueSize=" + queueSize +
				'}';
	}

	/**
	 * The default thread factory
	 */
	protected static class BackgroundThreadFactory implements ThreadFactory {

		private final AtomicInteger number = new AtomicInteger( 1 );
		private final String name;

		protected BackgroundThreadFactory (String name) {
			this.name = name;
		}

		public Thread newThread (Runnable r) {
			Thread t = new Thread( r, name + number.getAndIncrement() );
			if (t.isDaemon())
				t.setDaemon( false );
			if (t.getPriority() != DEFAULT_THREAD_PRIORITY)
				t.setPriority( DEFAULT_THREAD_PRIORITY );
			return t;
		}
	}
}
