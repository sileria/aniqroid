/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.graphics.*;
import android.text.TextPaint;

import java.io.*;
import java.net.*;

import com.sileria.android.Resource;
import com.sileria.net.HttpReader;
import com.sileria.util.*;

/**
 * Default content handler implementation for a bitmap drawable loader.
 *
 * @author Ahmed Shakil
 * @date 08-21-2012
 */
public class BitmapSerializer extends ContentHandler implements ObjectSerializer<Bitmap> {

	private final int timeout                   = 0;
	private final boolean useCache              = ContentLoader.DEF_USE_CACHE;
	private final int buffSize                  = ContentLoader.DEF_BUFF_SIZE;

	private final ImageOptions opt;

	/**
	 * Constructor, default.
	 */
	public BitmapSerializer () {
		this( null );
	}

	/**
	 * Constructor specifying required width and height of the image to be loaded.
	 * @param opt ImageOptions providing more loading options like required with and height
	 */
	public BitmapSerializer (ImageOptions opt) {
		this.opt = opt;
	}

	/**
	 * Given a URL connect stream positioned at the beginning of the
	 * representation of an object, this method reads that stream and
	 * creates an object from it.
	 *
	 * @param conn a URL connection.
	 * @return the object read by the <code>ContentHandler</code>.
	 * @throws IOException if an I/O error occurs while reading the object.
	 */
	@Override
	public Bitmap getContent (URLConnection conn) throws IOException {
		return loadImage( conn, opt );
	}

	/**
	 * Given a URL connect stream positioned at the beginning of the
	 * representation of an object, this method reads that stream and
	 * creates an object from it.
	 *
	 * @param conn a URL connection.
	 * @return the object read by the <code>ContentHandler</code>.
	 * @throws IOException if an I/O error occurs while reading the object.
	 */
	public Bitmap loadBitmap (URLConnection conn, ImageOptions opt) throws IOException {
		InputStream in = null;
		Bitmap bmp;
		try {
			in = conn.getInputStream();
			bmp = decodeStream( in, opt );
		}
		finally {
			IO.close( in );
		}
		return bmp;
	}

	/**
	 * Read bitmap from the specified input stream.
	 *
	 * @param in input stream to read from
	 * @return The decoded bitmap, or null if the image data could not be decoded.
	 * @throws IOException in case of IO errors
	 */
	public Bitmap decodeStream (InputStream in) throws IOException {
		return decodeStream( in, opt );
	}

	/**
	 * Read bitmap from the specified input stream.
	 *
	 * @param in input stream to read from
	 * @return The decoded bitmap, or null if the image data could not be decoded.
	 * @throws IOException in case of IO errors
	 */
	public static Bitmap decodeStream (InputStream in, ImageOptions opt) throws IOException {
		Bitmap bmp = null;

		// load with options
		if (ImageOptions.hasOptions( opt )) {
			if (opt.inSampleWidth > 0 && opt.inSampleHeight > 0)
				bmp = decodeScaledBitmap( in, opt );
			else if (opt.inSampleSize > 1)
				bmp = decodeSampledBitmap( in, opt );
		}

		// normal bitmap load if not already loaded
		if (bmp == null)
			bmp = BitmapFactory.decodeStream( in );

		// apply filter options if any
		if (bmp != null && opt != null && opt.hasFilters()) {
			Bitmap b = applyFilters( bmp, opt, false );
			if (b != bmp) {
				bmp.recycle();
				bmp = b;
			}
		}

		return bmp;
	}

	/**
	 * Load sampled image.
	 * @throws IOException if an I/O error occurs while reading the object.
	 */
	private static Bitmap decodeSampledBitmap (InputStream in, ImageOptions opt) throws IOException {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = opt.inSampleSize;
		return BitmapFactory.decodeStream( in, null, options );
	}

	/**
	 * Create filtered bitmap base on image options and the power of two flag.
	 * NOTE: If nothing is applied then same bitmap will be returned that was passed.
	 */
	public static Bitmap applyFilters (Bitmap bmp, ImageOptions opt, boolean pow2) {
		if (bmp == null) return null;
		if ((opt == null || !opt.hasFilters()) && !pow2) return bmp;

		final Bitmap old = bmp;

		Paint paint = null;
		int x = 0, y = 0;
		int width  = bmp.getWidth();
		int height = bmp.getHeight();
		boolean shadowEffect = false;

		// apply options
		if (opt != null) {

			// custom filter
			if (opt.filter != null) {
				bmp = opt.filter.filter( bmp, opt );
			}

			// calculate new size
			width  = opt.inExactWidth  > 0 ? opt.inExactWidth  : width;
			height = opt.inExactHeight > 0 ? opt.inExactHeight : height;

			// create shadow paint
			if (opt.hasShadow()) {
				shadowEffect = !opt.shadowImage;
				paint = new Paint();
				paint.setShadowLayer( opt.shadowRadius, opt.shadowDx, opt.shadowDy, opt.shadowColor );
				width  += (opt.shadowRadius*2 + Math.abs( opt.shadowDx ) +.5f );
				height += (opt.shadowRadius*2 + Math.abs( opt.shadowDy ) +.5f );
				x = Math.max( 0, (int)(opt.shadowRadius - opt.shadowDx + .5f) );
				y = Math.max( 0, (int)(opt.shadowRadius - opt.shadowDy + .5f) );
			}
		}

		// if need an image in power of 2 size
		if (pow2) {
			width = Utils.nextPow2( width );
			height = Utils.nextPow2( height );
		}

		// create bitmap and canvas
		Bitmap bitmap = Bitmap.createBitmap( width, height, bmp.getConfig() );
		Canvas canvas = new Canvas( bitmap );

		// draw new bitmap
		if (shadowEffect) {
			canvas.drawRect( x, y, x + bmp.getWidth(), y + bmp.getHeight(), paint );
			canvas.drawBitmap( bmp, x, y, null );
		}
		else
			canvas.drawBitmap( bmp, x, y, paint );

		if (bmp != old) {
			bmp.recycle();
			bmp = null;
		}

		return bitmap;
	}

	/**
	 * Creates text into a bitmap.
	 * You can pass either Paint or TextOptions or both.
	 */
	public static Bitmap createBitmap (String text, Paint paint, TextOptions opt, boolean pow2) {

		if (paint == null)
			paint = opt == null ? new TextPaint( TextPaint.ANTI_ALIAS_FLAG ) : opt.toPaint();

		if (opt == null)
			opt = new TextOptions();

		float y = calcTextRect( text, paint, opt, pow2 );
		Rect r = opt.outBounds;

		// cannot create zero size bitmap
		if (opt.outWidth <= 0 || opt.outHeight <= 0) {
			return null;
		}

		int width = pow2 ? Utils.nextPow2( r.width() ) : r.width();
		int height = pow2 ? Utils.nextPow2( r.height() ) : r.height();

		// create bitmap and canvas
		Bitmap bitmap = Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888 );
		Canvas canvas = new Canvas( bitmap );
		canvas.drawText( text, 0, y, paint );

		return bitmap;
	}

	/**
	 * Creates text into a bitmap.
	 * You can pass either Paint or TextOptions or both.
	 */
	public static float calcTextRect (String text, Paint paint, TextOptions opt, boolean pow2) {

		if (paint == null)
			paint = opt == null ? new TextPaint( TextPaint.ANTI_ALIAS_FLAG ) : opt.toPaint();
		Rect r = new Rect();
		paint.getTextBounds( text, 0, text.length(), r );
		int bottom = r.bottom; // Only to be used on inFontHeight = false cases
		r.offset( 0, -r.top );
		Paint.FontMetrics fm = paint.getFontMetrics();
		if (opt != null && opt.inFontHeight) {
			r.bottom = (int)(fm.top*-1 + fm.bottom + .5f);
		}

		int width = pow2 ? Utils.nextPow2( r.width() ) : r.width();
		int height = pow2 ? Utils.nextPow2( r.height() ) : r.height();

		// cannot create zero size bitmap
		if (width <= 0 || height <= 0) {
			if (opt != null)
				opt.outWidth = opt.outHeight = -1;
			return -1;
		}

		// update the opt results.
		if (opt != null) {
			opt.outWidth = width;
			opt.outHeight = height;
			opt.outBounds = r;
		}

		// draw text
		float y;

		if (opt != null && opt.inFontHeight) {
			float fh = -fm.top + fm.bottom;
			y = (height + fh )/2f; 							// Center the box vertically
			r.offset( 0, Math.max( r.top, (int)(y - fh) ) );
			y -= fm.bottom; 								// Center the text vertically
		} else {
			y = (height + r.height())/2f;					// Center the box vertically
			r.offset( 0, Math.max( r.top, (int)(y - r.height()) ) );
			y -= bottom;									// Center the text vertically
		}

		return y;
	}

	/**
	 * Load scaled image.
	 * @throws IOException if an I/O error occurs while reading the object.
	 */
	private static Bitmap decodeScaledBitmap (InputStream in, ImageOptions opt) throws IOException {

		HttpReader reader = null;
		ByteArrayOutputStream baos = null;
		byte[] bytes;

		// read stream
		try {
			reader = new HttpReader();
			baos = reader.readData( in );
			bytes = baos.toByteArray();
		}
		finally {
			IO.close( baos );
			IO.close( reader );
		}

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray( bytes, 0, bytes.length, options );

		// Calculate inSampleSize
		options.inSampleSize = Resource.calcInSampleSize(options, opt.inSampleWidth, opt.inSampleHeight );

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeByteArray( bytes, 0, bytes.length, options );
	}

	/**
	 * Write to the stream.
	 *
	 * @param os	 output stream to write to
	 * @param bitmap Bitmap image to write
	 * @throws IOException in case of IO errors
	 */
	public boolean write (OutputStream os, Bitmap bitmap) throws IOException {
		if (bitmap == null) return false;

		bitmap.compress( Bitmap.CompressFormat.PNG, 100, os );
		return true;
	}

	/**
	 * Read object from to the stream.
	 *
	 * @param in output stream to write to
	 * @return object object to write
	 * @throws IOException in case of IO errors
	 */
	public Bitmap read (InputStream in) throws IOException {
		return read( in, opt );
	}

	/**
	 * Read object from to the stream.
	 *
	 * @param in output stream to write to
	 * @return newly created Bitmap or null
	 * @throws IOException in case of IO errors
	 */
	public Bitmap read (InputStream in, ImageOptions opt) throws IOException {
		Bitmap bitmap = decodeStream( in, opt );
		//return bitmap == null ? null : new Bitmap( Resource.getResources(), bitmap );
		return bitmap;
	}

	/**
	 * Read object from a URLConnection.
	 *
	 * @param conn a URL connection.
	 * @return newly created Bitmap or null
	 * @throws IOException in case of IO errors
	 */
	public Bitmap loadImage (URLConnection conn, ImageOptions opt) throws IOException {
		Bitmap bmp;
		InputStream in = null;
		try {
			in = conn.getInputStream();
			bmp = read( in, opt );
		}
		finally {
			IO.close( in );
		}
		return bmp;
	}

	/**
	 * Load bitmap in background thread from the specified URL address.
	 *
	 * @param url URL address
	 * @param opt image options
	 *
	 * @throws IOException in case of IO exception.
	 */
	public Bitmap loadImage (String url, ImageOptions opt) throws IOException {

		Bitmap img;

		// try loading the image
		URL u = new URL( url );
		URLConnection conn = u.openConnection();
		conn.setConnectTimeout( timeout );
		conn.setUseCaches( useCache );

		Object content = conn.getContent();
		if (opt == null && content instanceof Bitmap)
			img = (Bitmap)content;
		else
			img = loadImage( conn, opt );

		// follow redirects.
		if (img == null && HttpURLConnection.getFollowRedirects()) {
			u = conn.getURL();
			if (u != null)
				img = loadImage( u.toString(), opt );
		}

		return img;
	}

	/**
	 * Load bitmap in background thread from the specified URL address.
	 *
	 * @param url URL address
	 * @param opt image options
	 * @param tries the number of tries that has happened so far.
	 * @return Loaded Bitmap
	 *
	 * @throws IOException in case of IO exception.
	 */
	public Bitmap loadBitmap (String url, ImageOptions opt, int tries) throws IOException {
		Bitmap bmp = null;
		InputStream in = null;
		HttpReader reader = null;
		try {

			URL u = new URL( url );
			URLConnection conn = u.openConnection();
			conn.setConnectTimeout( timeout );
			conn.setUseCaches( useCache );

			if (opt != null) {
				bmp = loadBitmap( conn, opt );
			}
			else {
				Object content = conn.getContent();
				if (content instanceof Bitmap)
					bmp = (Bitmap)content;
				else {
					in = content instanceof InputStream ? (InputStream)content : conn.getInputStream();

					if (tries%2 == 0) {
						bmp = BitmapFactory.decodeStream( in );
					}
					else {
						reader = new HttpReader( buffSize, timeout, useCache );
						ByteArrayOutputStream bos = reader.readData( in );

						bmp = BitmapFactory.decodeByteArray( bos.toByteArray(), 0, bos.size() );
						IO.close( bos );
					}

					// follow redirects.
					if (bmp == null && HttpURLConnection.getFollowRedirects()) {
						u = conn.getURL();
						if (u != null)
							bmp = loadBitmap( u.toString(), opt, tries );
					}
				}
			}
		}
		finally {
			IO.close( in );
			IO.close( reader );
		}
		return bmp;
	}

	/**
	 * Load drawable image in background thread from the specified URL address.
	 *
	 * @param url URL address
	 * @param opt expects an {@link ImageOptions} implementation or <code>null</code>
	 * @param tries the number of tries that has happened so far
	 * @return Loaded Drawable object or <code>null</code> if not able to decode
	 *
	 * @throws IOException in case of IO exception.
	 */
	public Bitmap loadImage (String url, ImageOptions opt, int tries) throws IOException {
		Bitmap image = null;
		InputStream in = null;
		HttpReader reader = null;
		try {

			URL u = new URL( url );
			URLConnection conn = u.openConnection();
			conn.setConnectTimeout( timeout );
			conn.setUseCaches( useCache );

			if (opt != null) {
				image = new BitmapSerializer( opt ).getContent( conn );
			}
			else {
				Object content = conn.getContent();
				in = content instanceof InputStream ? (InputStream)content : conn.getInputStream();

				if (tries%2 == 0) {
					image = BitmapFactory.decodeStream( in );
				}
				else {
					reader = new HttpReader( buffSize, timeout, useCache );
					ByteArrayOutputStream bos = reader.readData( in );

					Bitmap bm = BitmapFactory.decodeByteArray( bos.toByteArray(), 0, bos.size() );
					image = bm == null ? null : bm;

					IO.close( bos );
				}

				// follow redirects.
				if (image == null && HttpURLConnection.getFollowRedirects()) {
					u = conn.getURL();
					if (u != null)
						image = loadImage( u.toString(), opt, tries );
				}
			}
		}
		finally {
			IO.close( in );
			IO.close( reader );
		}
		return image;
	}

	/**
	 * Load bitmap in background thread from the specified file <code>path</code>.
	 *
	 * @param path File path
	 * @param opt image options
	 *
	 * @throws IOException in case of IO exception.
	 */
	public Bitmap loadImage (File path, ImageOptions opt) throws IOException {

		// check if the file exists.
		if (!path.exists())
			throw new FileNotFoundException( "File not found: " + path );

		// try loading the image
		Bitmap img = null;
		InputStream stream = null;
		try {
			stream = new FileInputStream( path );
			Bitmap bitmap = decodeStream( stream, opt );
			img = bitmap == null ? null : bitmap;
		}
		finally {
			IO.close( stream );
		}

		return img;
	}

}
