/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.os.Handler;
import android.os.SystemClock;

import java.io.IOException;
import java.io.InterruptedIOException;

import com.sileria.android.Kit;
import com.sileria.util.*;

/**
 * ContentLoader is a generic abstract class that builts in the mechanism for loading
 * any kind of user defined content from the a stream or a webservice.
 * <p/>
 * To extend simply override {@link #loadContent(String, com.sileria.util.ContentOptions, int)}   method in your subclass.
 * <p/>
 * As an example check out the {@link ImageLoader} implementation.
 *
 * @author Ahmed Shakil
 * @date Jul 1, 2012
 *
 * @param <T> content type of any kind.
 * @param <O> content options type
 */
public abstract class ContentLoader<T, O extends ContentOptions> implements Runnable, Cancellable {

	private final Handler handler;
	private final Content<T,O> request;

	private ContentCallback<T,O> callback;

	protected boolean cancelled;
	protected int timeout;

	protected boolean useCache              = DEF_USE_CACHE;

	static final int DEF_BUFF_SIZE          = 8192;
	static boolean DEF_USE_CACHE            = true;

	private static final int RETRY_TIMES    = 3;
	private static final int RETRY_DELAY    = 133;

	/**
	 * Set the default cache usage setting for all requests.
	 * By default it is set to <code>true</code> for content loaders.
	 *
	 * @param useCache a <code>boolean</code> indicating whether
	 * or not to allow caching
	 */
	public static void setDefaultUseCache (boolean useCache) {
		DEF_USE_CACHE = useCache;
	}

    /**
     * Constructor, default.
     */
    protected ContentLoader (Handler handler, String url) {
		this( handler, new Content<T,O>(url), null );
    }

    /**
     * Constructor specifying a callback
     */
    protected ContentLoader (Handler handler, String url, ContentCallback<T, O> callback) {
        this( handler, new Content<T,O>(url), callback );
    }

	/**
	 * Executes the task with a single url to load and a tagging index.
	 *
	 * @param url URL of the content
	 * @param opt ContentOptions implementation, can be null if not options needed or provided.
	 */
	public ContentLoader (Handler handler, String url, O opt, ContentCallback<T, O> callback) {
		this( handler, new Content<T,O>( url, opt ), callback );
	}

	/**
	 * Construct the loader with a specified content <code>request</code> and <code>callback</code>
	 *
	 * @param request requested data
	 */
	public ContentLoader (Handler handler, Content<T, O> request, ContentCallback<T, O> callback) {
		this.handler = handler;
		this.request = request;
		this.callback = callback;
	}

	/**
	 * Sets a specified timeout value, in milliseconds, to be used when opening
	 * a communications link to the resource referenced by this URLConnection.
	 * <p/>
	 * If the timeout expires before the connection can be established,
	 * a java.net.SocketTimeoutException is raised. A timeout of zero is
	 * interpreted as an infinite timeout.
	 * <p/>
	 * Note: This parameter will only take affect if default {@link com.sileria.net.HttpReader} or
	 * one if it's subclasses are used as the <code>RemoteReader</code>. If you provide
	 * your own implementation of <code>RemoteReader</code> set the parameters directly
	 * into that custom class.
	 * <p/>
	 * @param millis timeout value in milliseconds
	 */
	public void setTimeout (int millis) {
		this.timeout = millis;
	}

	/**
	 * Sets the value of the <code>useCaches</code> field of this
	 * <code>URLConnection</code> to the specified value.
	 *
	 * @param useCache a <code>boolean</code> indicating whether
	 * or not to allow caching
	 */
	public void setUseCache (boolean useCache) {
		this.useCache = useCache;
	}

	/**
	 * Set the content callback listener.
	 */
	public void setCallback (ContentCallback<T,O> callback) {
		this.callback = callback;
	}

	/**
     * Load contents in background and notify the callback on the EDT.
     */
    public void run () {
        if (request.key == null)
            return;

        // load one by one.
		final String u = request.key;
		final int id = request.id;

		T content = null;
		Throwable t = null;

		// keep trying couple of times in-case of error.
		for (int tries = 0; tries < RETRY_TIMES && !cancelled; ) {
			try {
				content = loadContent( u, request.options, tries );
				break;
			}
			catch (InterruptedIOException e) {
				cancelled = true;
				Log.v( Kit.TAG, "Cancelled loading content: " + u );
				break;
			}
			catch (Throwable e) {
				Log.e( Kit.TAG, "Error loading content: " + u, t = e );
				System.gc();
			}

			if (++tries < RETRY_TIMES) {
				Log.w( Kit.TAG, "Content Load Failed, Will retry in " + RETRY_DELAY + "ms." );
				SystemClock.sleep( RETRY_DELAY );
			}
		}

		if (callback != null && !cancelled) {
			if (content != null) {
				handler.post( new SuccessPost( new Content<>( content, id, u, request.options ) ) );
			}
			else {
				if (t == null)
					t = new RuntimeException( "Unknown error loading: " + u );
				handler.post( new FailurePost( new Content<>( t, id, u, request.options ) ) );
			}
		}
    }

	/**
	 * Load content in background thread from the specified URL address.
	 *
	 * @param url URL address or Filename
	 * @param opt implementation of <code>ContentOptions</code>
	 *@param tries the number of tries that has happened so far.
	 *  @return Loaded content from the url or filename
	 *
	 * @throws java.io.IOException in case of IO exception.
	 */
	protected abstract T loadContent (String url, O opt, int tries) throws IOException;

	/**
	 * URL can be accessed after the execute call was made.
	 * @return url string that was passed to the execute method
	 */
	public String getURL () {
		return request == null ? null : request.key;
	}

	/**
	 * ID can be accessed after the execute call was made.
	 * @return id that was passed to the execute method
	 */
	public int getID () {
		return request == null ? -1 : request.id;
	}

	/**
	 * ID can be accessed after the execute call was made.
	 * @return id that was passed to the execute method
	 */
	Content<T, O> getRequest () {
		return request;
	}

	/**
	 * Cancel the call.
	 */
	public void cancel () {
		this.cancelled = true;
	}

	/**
	 * Handler post success.
	 */
	private class SuccessPost implements Runnable {

		private Content<T, O> content;

		private SuccessPost (Content<T, O> content) {
			this.content = content;
		}

		public void run () {
			if (callback != null && !cancelled)
				callback.onContentLoad( content );
			content = null;
		}
	}

	/**
	 * Handler post failure.
	 */
	private class FailurePost implements Runnable {

		private Content<Throwable, O> error;

		private FailurePost (Content<Throwable, O> error) {
			this.error = error;
		}

		public void run () {
			if (callback != null && !cancelled)
				callback.onContentFail( error );
			error = null;
		}
	}
}
