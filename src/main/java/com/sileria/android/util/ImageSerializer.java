/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.io.*;
import java.net.*;

import com.sileria.android.Resource;
import com.sileria.net.HttpReader;
import com.sileria.util.IO;
import com.sileria.util.ObjectSerializer;

/**
 * Default content handler implementation for a bitmap drawable loader.
 *
 * @author Ahmed Shakil
 * @date 08-21-2012
 */
public class ImageSerializer extends ContentHandler implements ObjectSerializer<BitmapDrawable> {

	private final int timeout                   = 0;
	private final boolean useCache              = ContentLoader.DEF_USE_CACHE;
	private final int buffSize                  = ContentLoader.DEF_BUFF_SIZE;

	private final ImageOptions opt;

	/**
	 * Constructor, default.
	 */
	public ImageSerializer () {
		this( null );
	}

	/**
	 * Constructor specifying required width and height of the image to be loaded.
	 * @param opt ImageOptions providing more loading options like required with and height
	 */
	public ImageSerializer (ImageOptions opt) {
		this.opt = opt;
	}

	/**
	 * Given a URL connect stream positioned at the beginning of the
	 * representation of an object, this method reads that stream and
	 * creates an object from it.
	 *
	 * @param conn a URL connection.
	 * @return the object read by the <code>ContentHandler</code>.
	 * @throws java.io.IOException if an I/O error occurs while reading the object.
	 */
	@Override
	public BitmapDrawable getContent (URLConnection conn) throws IOException {
		return loadImage( conn, opt );
	}

	/**
	 * Write to the stream.
	 *
	 * @param os	 output stream to write to
	 * @param image bitmap drawable image to write
	 * @throws java.io.IOException in case of IO errors
	 */
	public boolean write (OutputStream os, BitmapDrawable image) throws IOException {
		if (image == null) return false;

		Bitmap bitmap = image.getBitmap();
		if (bitmap == null) return false;

		bitmap.compress( Bitmap.CompressFormat.PNG, 100, os );
		return true;
	}

	/**
	 * Read object from to the stream.
	 *
	 * @param in output stream to write to
	 * @return object object to write
	 * @throws java.io.IOException in case of IO errors
	 */
	public BitmapDrawable read (InputStream in) throws IOException {
		return read( in, opt );
	}

	/**
	 * Read object from to the stream.
	 *
	 * @param in output stream to write to
	 * @return newly created BitmapDrawable or null
	 * @throws java.io.IOException in case of IO errors
	 */
	public BitmapDrawable read (InputStream in, ImageOptions opt) throws IOException {
		Bitmap bitmap = BitmapSerializer.decodeStream( in, opt );
		return bitmap == null ? null : new BitmapDrawable( Resource.getResources(), bitmap );
	}

	/**
	 * Read object from a URLConnection.
	 *
	 * @param conn a URL connection.
	 * @return newly created BitmapDrawable or null
	 * @throws java.io.IOException in case of IO errors
	 */
	public BitmapDrawable loadImage (URLConnection conn, ImageOptions opt) throws IOException {
		BitmapDrawable bmp;
		InputStream in = null;
		try {
			in = conn.getInputStream();
			bmp = read( in, opt );
		}
		finally {
			IO.close( in );
		}
		return bmp;
	}

	/**
	 * Load bitmap in background thread from the specified URL address.
	 *
	 * @param url URL address
	 * @param opt image options
	 *
	 * @throws IOException in case of IO exception.
	 */
	public BitmapDrawable loadImage (String url, ImageOptions opt) throws IOException {

		BitmapDrawable img;

		// try loading the image
		URL u = new URL( url );
		URLConnection conn = u.openConnection();
		conn.setConnectTimeout( timeout );
		conn.setUseCaches( useCache );

		Object content = conn.getContent();
		if (opt == null && content instanceof BitmapDrawable)
			img = (BitmapDrawable)content;
		else
			img = loadImage( conn, opt );

		// follow redirects.
		if (img == null && HttpURLConnection.getFollowRedirects()) {
			u = conn.getURL();
			if (u != null)
				img = loadImage( u.toString(), opt );
		}

		return img;
	}

	/**
	 * Load drawable image in background thread from the specified URL address.
	 *
	 * @param url URL address
	 * @param opt expects an {@link ImageOptions} implementation or <code>null</code>
	 * @param tries the number of tries that has happened so far
	 * @return Loaded Drawable object or <code>null</code> if not able to decode
	 *
	 * @throws IOException in case of IO exception.
	 */
	public Drawable loadImage (String url, ImageOptions opt, int tries) throws IOException {
		Drawable image = null;
		InputStream in = null;
		HttpReader reader = null;
		try {

			URL u = new URL( url );
			URLConnection conn = u.openConnection();
			conn.setConnectTimeout( timeout );
			conn.setUseCaches( useCache );

			if (opt != null) {
				image = new ImageSerializer( opt ).getContent( conn );
			}
			else {
				Object content = conn.getContent();
				if (content instanceof Drawable)
					image = (Drawable)content;
				else {
					in = content instanceof InputStream ? (InputStream)content : conn.getInputStream();

					if (tries%2 == 0) {
						image = Drawable.createFromStream( in, "Image" );
					}
					else {
						reader = new HttpReader( buffSize, timeout, useCache );
						ByteArrayOutputStream bos = reader.readData( in );

						Bitmap bm = BitmapFactory.decodeByteArray( bos.toByteArray(), 0, bos.size() );
						image = bm == null ? null : new BitmapDrawable( Resource.getResources(), bm );

						IO.close( bos );
					}

					// follow redirects.
					if (image == null && HttpURLConnection.getFollowRedirects()) {
						u = conn.getURL();
						if (u != null)
							image = loadImage( u.toString(), opt, tries );
					}
				}
			}
		}
		finally {
			IO.close( in );
			IO.close( reader );
		}
		return image;
	}

	/**
	 * Load bitmap in background thread from the specified file <code>path</code>.
	 *
	 * @param path File path
	 * @param opt image options
	 *
	 * @throws IOException in case of IO exception.
	 */
	public BitmapDrawable loadImage (File path, ImageOptions opt) throws IOException {

		// check if the file exists.
		if (!path.exists())
			throw new FileNotFoundException( "File not found: " + path );

		// try loading the image
		BitmapDrawable img = null;
		InputStream stream = null;
		try {
			stream = new FileInputStream( path );
			Bitmap bitmap = BitmapSerializer.decodeStream( stream, opt );
			img = bitmap == null ? null : new BitmapDrawable( Resource.getResources(), bitmap );
		}
		finally {
			IO.close( stream );
		}

		return img;
	}

}
