/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.util;

import android.graphics.Bitmap;

/**
 * Image filter interface used for providing custom filters
 * that can be used by setting it to {@link com.sileria.android.util.ImageOptions}
 * and passing it to {@link com.sileria.android.util.CachedImageLoader}'s get methods.
 *
 * NOTE: To use the loader with the TextureCache you MUST override the {@code #hashCode()} method
 * such that a <code>ImageFilter</code>for a unique url should always return the same hash code,
 * otherwise the caching will not work properly.
 *
 * @author Ahmed Shakil
 * @date 01-14-2014
 *
 * @see com.sileria.android.util.ImageOptions
 */
public interface ImageFilter {

	/**
	 * Apply user defined filters specified Bitmap.
	 *
	 * @param bitmap Raw/filtered bitmap to be loaded as a texture
	 *
	 * @return Texture handle id of the newly generated texture
	 */
	Bitmap filter (Bitmap bitmap, ImageOptions opt);

}
