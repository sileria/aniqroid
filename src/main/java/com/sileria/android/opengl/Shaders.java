/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.opengl;

import com.sileria.android.Kit;
import com.sileria.android.Resource;
import com.sileria.util.Log;

import static android.opengl.GLES20.*;

/**
 * Shaders utils class contains functionality to compile, build and link programs.
 *
 * @author Ahmed Shakil
 * @date 12-09-2013
 */
public class Shaders {

	private static final String TAG = Kit.TAG + ":ShaderHelper";

	/**
	 * Loads and compiles a vertex shader, returning the OpenGL object ID.
	 */
	public static int compileVertexShader (String shaderCode) {
		return compileShader( GL_VERTEX_SHADER, shaderCode );
	}

	/**
	 * Loads and compiles a fragment shader, returning the OpenGL object ID.
	 */
	public static int compileFragmentShader (String shaderCode) {
		return compileShader( GL_FRAGMENT_SHADER, shaderCode );
	}

	/**
	 * Compiles a shader, returning the OpenGL object ID.
	 */
	private static int compileShader (int type, String shaderCode) {
		// Create a new shader object.
		final int shaderObjectId = glCreateShader( type );

		if (shaderObjectId == 0) {
			Log.e( TAG, "Could not create new shader.\n"+shaderCode+"\n" );
			return 0;
		}

		// Pass in the shader source.
		glShaderSource( shaderObjectId, shaderCode );

		// Compile the shader.
		glCompileShader( shaderObjectId );

		// Get the compilation status.
		final int[] compileStatus = new int[1];
		glGetShaderiv( shaderObjectId, GL_COMPILE_STATUS, compileStatus, 0 );

		// Verify the compile status.
		if (compileStatus[0] == 0) {
			// If it failed, delete the shader object.
			glDeleteShader( shaderObjectId );
			Log.e( TAG, "Compilation of shader failed.\n"+shaderCode+"\n" );
			return 0;
		}

		// Return the shader object ID.
		return shaderObjectId;
	}

	/**
	 * Links a vertex shader and a fragment shader together into an OpenGL
	 * program. Returns the OpenGL program object ID, or 0 if linking failed.
	 */
	public static int linkProgram (int vertexShaderId, int fragmentShaderId) {

		// Create a new program object.
		final int programObjectId = glCreateProgram();

		if (programObjectId == 0) {
			Log.e( TAG, "Could not create new program." );
			return 0;
		}

		// Attach the vertex shader to the program.
		glAttachShader( programObjectId, vertexShaderId );

		// Attach the fragment shader to the program.
		glAttachShader( programObjectId, fragmentShaderId );

		// Link the two shaders together into a program.
		glLinkProgram( programObjectId );

		// Get the link status.
		final int[] linkStatus = new int[1];
		glGetProgramiv( programObjectId, GL_LINK_STATUS, linkStatus, 0 );

		// Verify the link status.
		if (linkStatus[0] == 0) {
			// If it failed, delete the program object.
			glDeleteProgram( programObjectId );
			Log.e( TAG, String.format("Linking of program failed for vertex-shader[%d] and fragment-shader[%d].", vertexShaderId, fragmentShaderId) );
			return 0;
		}

		// Return the program object ID.
		return programObjectId;
	}

	/**
	 * Validates an OpenGL program. Should only be called when developing the application.
	 */
	public static boolean validateProgram (int programObjectId) {
		final int[] validateStatus = new int[1];
		glValidateProgram( programObjectId );
		glGetProgramiv( programObjectId, GL_VALIDATE_STATUS, validateStatus, 0 );
		Log.v( TAG, "Results of validating program: " + validateStatus[0] + "\nLog:" + glGetProgramInfoLog( programObjectId ) );

		return validateStatus[0] != 0;
	}

	/**
	 * Helper function that compiles the shaders, links and validates the
	 * program, returning the program ID.
	 */
	public static int buildProgram (int vertexShaderId, int fragmentShaderId) {
		return buildProgram( Resource.readRawFile( vertexShaderId ), Resource.readRawFile( fragmentShaderId ) );
	}

	/**
	 * Helper function that compiles the shaders, links and validates the
	 * program, returning the program ID.
	 */
	public static int buildProgram (String vertexShaderSource, String fragmentShaderSource) {
		// Compile the shaders.
		int vertexShader = compileVertexShader( vertexShaderSource );
		int fragmentShader = compileFragmentShader( fragmentShaderSource );

		// Link them into a shader program.
		return linkProgram( vertexShader, fragmentShader );

	}
}
