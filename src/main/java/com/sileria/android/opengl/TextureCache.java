/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.opengl;

import android.graphics.Bitmap;
import android.graphics.Paint;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import com.sileria.android.Resource;
import com.sileria.android.util.*;
import com.sileria.util.*;

/**
 * Texture caching mechanism built on top of existing {@link com.sileria.android.util.ImageCache}
 * and {@link com.sileria.android.util.CachedImageLoader} classes.
 * </p>
 * Lets you create textures from bitmaps that are coming from urls or android resources.
 * </p>
 * Also lets you create string textures from text.
 *
 * @see com.sileria.android.opengl.TextureCallback
 * @see com.sileria.android.opengl.TextureObject
 * @see com.sileria.android.opengl.TextureOptions
 *
 * @author Ahmed Shakil
 * @date 12-18-2013
 */
public class TextureCache {

	private final Map<String, Integer> texCache = new HashMap<>();
	private final Map<String, Integer> strCache = new HashMap<>();

	private final Cache<String, Bitmap> imgCache;
	private final CachedImageLoader imgLoader;

	private final Map<String, ObjectCallback> listeners = new ConcurrentHashMap<>();

	// map image view with ObjectCallback object.
	private final Map<TextureObject, ObjectCallback> objmap = new ConcurrentHashMap<>();

	private SurfaceQueue surface;

	/**
	 * Constructs a loader that used disk cache in standard location with specified size.
	 */
	public TextureCache (int maxDiskSize) {
		this( ImageCache.createDiskCache( maxDiskSize ) );
	}

	/**
	 * Construct a loader class with your own <code>ImageCache</code> object.
	 * @param cache The internal cache to be used, ideally a {@link com.sileria.util.DiskCache}
	 */
	public TextureCache (Cache<String, Bitmap> cache) {
		this.imgCache = cache;
		this.imgLoader = new CachedImageLoader( cache );
	}

	/**
	 * Construct a loader class with your own <code>ImageCache</code> object.
	 * @param loader The internal cache and loader to be used.
	 */
	public TextureCache (CachedImageLoader loader) {
		this.imgLoader = loader;
		this.imgCache  = loader.cache();
	}

	/**
	 * Set the default surface to be used with the get methods
	 * that does not take surface object.
	 */
	public void setSurface (SurfaceQueue surface) {
		this.surface = surface;
	}

	/**
	 * Get cached texture for specified url.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param resId resource id for a bitmap
	 * @param object an OpenGL object that implements <code>TextureObject</code>
	 * @return texture id or 0.
	 */
	public int get (int resId, TextureObject object) {
		return get( resId, null, object );
	}

	/**
	 * Get cached texture for specified url.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param resId resource id for a bitmap
	 * @param opt      Extra image options defined in <code>TextureOptions</code> class, this can be null.
	 * @param object an OpenGL object that implements <code>TextureObject</code>
	 * @return texture id or 0.
	 */
	public int get (int resId, TextureOptions opt, TextureObject object) {
		if (resId == 0) return 0;
		String key = String.valueOf( resId );
		int texture;
		Integer texi = texCache.get( key );
		if (texi == null) {
			texture = Textures.load( resId, opt );
			if (texture != 0)
				texCache.put( key, texture );
		}
		else
			texture = texi;

		if (object != null)
			object.setTexture( texture );

		return texture;
	}

	/**
	 * Load specified bitmap as a texture and cache the texture into the texture id maps.
	 * @param key Unique string key to be used to query the bitmap next time. If there
	 *            is already a texture in memory with that key, then no new texture will be loaded.
	 * @param bitmap Bitmap to load as a texture
	 * @param object an OpenGL object that implements <code>TextureObject</code>
	 * @return texture id or 0.
	 */
	public int get (String key, Bitmap bitmap, TextureObject object) {
		return get (key, bitmap, null, object, false );
	}

	/**
	 * Load specified bitmap as a texture and cache the texture into the texture id maps.
	 * @param key Unique string key to be used to query the bitmap next time. If there
	 *            is already a texture in memory with that key, then no new texture will be loaded.
	 * @param bitmap Bitmap to load as a texture
	 * @param opt      Extra image options defined in <code>TextureOptions</code> class, this can be null.
	 * @param object an OpenGL object that implements <code>TextureObject</code>
	 * @param cacheBitmapAlso if you also want the bitmap to be cached to the memory or disk cache provided
	 *                        in the constructor of this class, then you can make this true.
	 * @return texture id or 0.
	 */
	public int get (String key, Bitmap bitmap, TextureOptions opt, TextureObject object, boolean cacheBitmapAlso) {
		if (bitmap == null) return 0;

		key = ImageCache.toHashKey( key, opt );
		int texture;
		Integer texi = texCache.get( key );
		if (texi == null) {
			texture = Textures.load( bitmap, opt );
			if (texture != 0) {
				texCache.put( key, texture );
				if (cacheBitmapAlso)
					imgCache.put( key, bitmap );
			}
		}
		else
			texture = texi;

		if (object != null)
			object.setTexture( texture );

		return texture;
	}

	/**
	 * Get cached texture for specified url.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param url image url
	 * @param object an OpenGL object that implements <code>TextureObject</code>
	 * @return texture id or 0.
	 */
	public int get (String url, TextureObject object) {
		return get( url, object, 0, 0, null, null, surface );
	}

	/**
	 * Get cached texture for specified url.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param url image url
	 * @param callback texture callback
	 * @return texture id or 0.
	 */
	public int get (String url, TextureCallback callback) {
		return get( url, null, 0, 0, null, callback, surface );
	}

	/**
	 * Get cached texture for specified url.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param url image url
	 * @param callback texture callback
	 * @param surface SurfaceQueue instance to be used for queuing events
	 * @return texture id or 0.
	 */
	public int get (String url, TextureCallback callback, SurfaceQueue surface) {
		return get( url, null, 0, 0, null, callback, surface );
	}

	/**
	 * Get cached texture for specified url.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param url image url
	 * @param empty empty texture id
	 * @param object an OpenGL object that implements <code>TextureObject</code>
	 * @return texture id or 0.
	 */
	public int get (String url, int empty, TextureObject object) {
		return get( url, object, empty, 0, null, null, surface );
	}

	/**
	 * Get cached texture for specified url.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param url image url
	 * @param empty empty texture id
	 * @param opt      Extra image options defined in <code>TextureOptions</code> class, this can be null.
	 * @param object an OpenGL object that implements <code>TextureObject</code>
	 * @return texture id or 0.
	 */
	public int get (String url, int empty, TextureOptions opt, TextureObject object) {
		return get( url, object, empty, 0, opt, null, surface );
	}

	/**
	 * Get cached texture for specified url.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param url image url
	 * @param empty empty texture id
	 * @param object an OpenGL object that implements <code>TextureObject</code>
	 * @param surface SurfaceQueue instance to be used for queuing events
	 * @return texture id or 0.
	 */
	public int get (String url, int empty, TextureObject object, SurfaceQueue surface) {
		return get( url, object, empty, 0, null, null, surface );
	}

	/**
	 * Get cached texture for specified url.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param url image url
	 * @param empty empty texture id
	 * @param opt      Extra image options defined in <code>TextureOptions</code> class, this can be null.
	 * @param object an OpenGL object that implements <code>TextureObject</code>
	 * @param surface SurfaceQueue instance to be used for queuing events
	 * @return texture id or 0.
	 */
	public int get (String url, int empty, TextureOptions opt, TextureObject object, SurfaceQueue surface) {
		return get( url, object, empty, 0, opt, null, surface );
	}

	/**
	 * Fetch a texture image from specified URL and set it on the <code>TextureObject</code>
	 * being used as <code>callback</code>.
	 *
	 * @param url      URL to download the image from
	 * @param object listener that gets triggered on success or failure
	 * @param empty    empty drawable to be set before the loading starts
	 * @param broken   drawable representing a broken link in terms of failure
	 * @param opt      Extra image options defined in <code>TextureOptions</code> class, this can be null.
	 * @param callback      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	public int get (String url, TextureObject object, int empty, int broken, TextureOptions opt, TextureCallback callback) {
		return get(url, object, empty, broken, opt, callback, surface );
	}

	/**
	 * Fetch a texture image from specified URL and set it on the <code>TextureObject</code>
	 * being used as <code>callback</code>.
	 *
	 * @param url      URL to download the image from
	 * @param object listener that gets triggered on success or failure
	 * @param empty    empty drawable to be set before the loading starts
	 * @param broken   drawable representing a broken link in terms of failure
	 * @param opt      Extra image options defined in <code>TextureOptions</code> class, this can be null.
	 * @param callback      Optional secondary callback if you want to do some extra work after image is successfully set (either
	 *                 from the cache or from url load) or in case of image load failure.
	 *                 NOTE: you do not need to set the ImageView drawable on callbacks because that will be taken care of.
	 *                 This can be null
	 */
	public int get (String url, TextureObject object, int empty, int broken, TextureOptions opt, TextureCallback callback, SurfaceQueue surface) {
		if (object == null && callback == null) return 0;

		String key = ImageCache.toHashKey( url, opt );

		// texture already cached
		Integer texture = texCache.get( key );
		if (texture != null) {
			setTexture( texture, object, callback );
			return texture;
		}

		// check if the object is already attached to same url being loaded
		ObjectCallback ocb = objmap.get( object );
		if (ocb != null && Utils.equals( ocb.key, key )) {
			//setTexture( texture, callback, cb2 );
			return 0;
		}

		cancel( object );    // cancel any previous callback

		// if empty url then set empty image and return
		if (Utils.isEmpty( url )) {
			if (object!=null) object.setTexture( empty );
			return 0;
		}

		// bitmap in memory cache
		if (imgCache.loaded( key )) {
			Bitmap bitmap = imgCache.get( key );
			if (bitmap != null) {
				texture = Textures.loadTexture( bitmap, opt, false );
				if (texture != 0) {
					texCache.put( key, texture );
					setTexture( texture, object, callback );
					return texture;
				}
			}
		}

		// enqueue url loading and callback
		ocb = listeners.get( key );
		broken = broken == 0 ? empty : broken;
		boolean newListener = ocb == null || ocb.done;
		if (newListener)
			ocb = new ObjectCallback( url, object, broken, opt, callback, key, surface );
		else
			ocb.add( object, callback );

		// put in the maps.
		if (imgLoader.get( url, opt, ocb )) {
			if (newListener)
				listeners.put( key, ocb );
			objmap.put( object, ocb );
		}

		return 0;
	}

	/**
	 * Get cached texture for specified string resource id or create a new one.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param strId resource id for a string
	 * @return texture id or 0.
	 */
	public int str (int strId, TextOptions opt, Object ... args) {
		String key = toHashKey( strId, opt );
		if (key == null) return 0;

		return str( Resource.getString( strId, args ), opt );
	}

	/**
	 * Get cached texture for specified string resource id or create a new one.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param strId resource id for a string
	 * @return texture id or 0.
	 */
	public int str (int strId, Paint paint, Object ... args) {
		String key = toHashKey( strId, paint );
		if (key == null) return 0;

		return str( Resource.getString( strId, args ), paint );
	}

	/**
	 * Get cached texture for specified text or create a new one.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param text String to be converted to a texture
	 * @return texture id or 0.
	 */
	public int str (String text, Paint paint) {
		return str( text, paint, null );
	}

	/**
	 * Get cached texture for specified text or create a new one.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param text String to be converted to a texture
	 * @return texture id or 0.
	 */
	public int str (String text, TextOptions opt) {
		return str( text, opt, null );
	}

	/**
	 * Get cached texture for specified text or create a new one.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param text String to be converted to a texture
	 * @return texture id or 0.
	 */
	protected int str (String text, TextOptions opt, String key) {
		if (Utils.isEmpty( text )) return 0;
		if (key == null)
			key = toHashKey( text, opt );

		int texture;
		Integer texi = strCache.get( key );
		if (texi == null) {
			texture = Textures.load( text, opt );
			if (texture != 0)
				strCache.put( key, texture );
		}
		else {
			BitmapSerializer.calcTextRect( text, null, opt, true );
			texture = texi;
		}

		return texture;
	}

	/**
	 * Get cached texture for specified text or create a new one.
	 * If cached texture is not available then it will start caching request
	 * and return zero.
	 * @param text String to be converted to a texture
	 * @return texture id or 0.
	 */
	protected int str (String text, Paint paint, String key) {
		if (Utils.isEmpty( text )) return 0;
		if (key == null)
			key = toHashKey( text, paint );

		int texture;
		Integer texi = strCache.get( key );
		if (texi == null) {
			texture = Textures.load( text, paint );
			if (texture != 0)
				strCache.put( key, texture );
		}
		else {
			BitmapSerializer.calcTextRect( text, paint, null, true );
			texture = texi;
		}

		return texture;
	}

	/**
	 * Put texture handle into cache.
	 * Note: This does not add the texture to the mem or disk cache.
	 * @param key URL address or key
	 * @param texId texture handle id
	 */
	public void cache (String key, int texId) {
		texCache.put( key, texId );
	}

	/**
	 * Remove the texture related to the url from the cache.
	 * NOTE: this method does not delete the texture itself.
	 *
	 * @param key URL or key for the texture used for loading
	 *
	 * @return the texture that was removed or 0
	 */
	public int remove (String key) {
		Integer tex = texCache.remove( key );
		if (tex == null)
			tex = strCache.remove( key );
		return tex == null ? 0 : tex;
	}

	/**
	 * Remove the texture from the string cache only.
	 * NOTE: this method does not delete the texture itself.
	 *
	 * @param key URL or key for the texture used for loading
	 *
	 * @return the texture that was removed or 0
	 */
	public int removeTex (String key) {
		Integer tex = texCache.remove( key );
		return tex == null ? 0 : tex;
	}

	/**
	 * Remove the texture from the string cache only.
	 * NOTE: this method does not delete the texture itself.
	 *
	 * @param key URL or key for the texture used for loading
	 *
	 * @return the texture that was removed or 0
	 */
	public int removeStr (String key) {
		Integer tex = strCache.remove( key );
		return tex == null ? 0 : tex;
	}

	/**
	 * Removes the texture specified by the text id.
	 * NOTE: this method does not delete the texture itself.
	 * Call {@link #delete(int)} to delete and remove the texture together.
	 *
	 * @param texId texture id handle
	 */
	public boolean remove (int texId) {
		Set<Map.Entry<String, Integer>> entries = texCache.entrySet();
		for (Map.Entry<String, Integer> me : entries)
			if (me.getValue() == texId) {
				texCache.remove( me.getKey() );
				return true;
			}
		entries = strCache.entrySet();
		for (Map.Entry<String, Integer> me : entries)
			if (me.getValue() == texId) {
				strCache.remove( me.getKey() );
				return true;
			}
		return false;
	}

	/**
	 * Clears the specified text from the cache as well as deletes
	 * it from the OpenGL texture buffer.
	 * @param texId texture id to be deleted
	 */
	public void delete (int texId) {
		remove( texId );
		Textures.delete( texId );
	}

	/**
	 * Cancel a listener callback for specified <code>callback</code>.
	 * If no other listeners were registered for the url then the
	 * image loading will also be cancelled.
	 */
	protected void cancel (TextureObject object) {
		if (object == null) return;

		ObjectCallback cb = objmap.remove( object );
		if (cb != null) {
			cb.remove( object );
			if (cb.isEmpty())
				listeners.remove( cb.key );
		}
	}

	/**
	 * Cancel a image loading for specified <code>url</code>.
	 * NOTE: All callbacks for this <code>url</code> will be removed from being notified.
	 */
	public void cancel (String url) {
		cancel( url, null, null );
	}

	/**
	 * Cancel a image loading for specified <code>url</code>.
	 * NOTE: All callbacks for this <code>url</code> will be removed from being notified.
	 */
	public void cancel (String url, TextureOptions opt) {
		cancel( url, opt, null );
	}

	/**
	 * Cancel a listener callback for specified <code>callback</code>.
	 * If no other listeners were registered for the url then the
	 * image loading will also be cancelled.
	 */
	public void cancel (String url, TextureOptions opt, TextureCallback callback) {
		String key = ImageCache.toHashKey( url, opt );

		if (callback == null) {
			ObjectCallback cb = listeners.remove( key );
			if (cb != null) cb.cancelAll();
		}
		else {
			ObjectCallback cb = listeners.get( key );
			if (cb != null) {
				cb.remove( callback );
				if (cb.isEmpty())
					listeners.remove( cb.key );
			}
		}
	}

	/**
	 * Clears the texture cache.
	 * NOTE: this method does not delete the texture itself.
	 * Call {@link #purge()} to delete and remove all the texture together.
	 */
	public void clear () {
		texCache.clear();
		strCache.clear();
	}

	/**
	 * Clears the specified text from the cache as well as deletes
	 * it from the OpenGL texture buffer.
	 */
	public void purge () {
		if (texCache.isEmpty() && strCache.isEmpty()) return;

		int[] texs = new int[texCache.size()+strCache.size()];
		Collection<Integer> values = texCache.values();
		int i = 0;
		for (Integer v : values)
			texs[i++] = v;
		texCache.clear();

		values = strCache.values();
		for (Integer v : values)
			texs[i++] = v;
		strCache.clear();

		Textures.delete( texs );
	}

	/**
	 * Generate unique hash for text/opt.
	 */
	public static String toHashKey (String text, TextOptions opt) {
		if (text == null || text.length()==0)
			return null;

		int result = text.hashCode();
		if (TextOptions.hasOptions( opt ))
			result = 31 * result + opt.hashCode();

		return Integer.toHexString( result );
	}

	/**
	 * Generate unique hash for text/opt.
	 */
	public static String toHashKey (int text, TextOptions opt) {
		if (text == 0) return null;

		if (TextOptions.hasOptions( opt ))
			text = 31 * text + opt.hashCode();

		return Integer.toHexString( text );
	}

	/**
	 * Generate unique hash for text/opt.
	 */
	protected static String toHashKey (String text, Paint paint) {
		if (text == null || text.length()==0)
			return null;

		return toHashKey( text, new TextOptions( paint ) );
	}

	/**
	 * Generate unique hash for text/opt.
	 */
	protected static String toHashKey (int text, Paint paint) {
		return text == 0 ? null : toHashKey( text, new TextOptions( paint ) );
	}

	/**
	 * Set texture to the object and trigger the callback.
	 */
	private static void setTexture (int texture, TextureObject callback, TextureCallback cb2) {
		if (callback != null)
			callback.setTexture( texture );
		if (cb2 != null)
			cb2.onContentLoad( new Content<Integer, TextureOptions>( texture ) );
	}

	/**
	 * Texture object callback.
	 */
	protected class ObjectCallback implements BitmapCallback, Runnable {

		protected String url, key;
		protected Set<TextureObject> objs;
		protected int empty;
		protected TextureOptions opt;
		protected Set<TextureCallback> tcbs;
		private boolean done;
		private final Object lock = new Object();
		private Bitmap bitmap;

		private SurfaceQueue surface;

		public ObjectCallback (String url, TextureObject objs, int empty, TextureOptions opt, TextureCallback tb, String key, SurfaceQueue surface) {
			this.url = url;
			this.empty = empty;
			this.opt = opt;
			this.key = key;
			add( objs, tb );
			this.surface = surface;
		}

		@Override
		public boolean equals (Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			ObjectCallback that = (ObjectCallback)o;

			return key.equals( that.key );
		}

		@Override
		public int hashCode () {
			return key.hashCode();
		}

		@Override
		public void onContentLoad (Content<Bitmap, ImageOptions> content) {
			done = true;
			bitmap = content.content;

			surface.queueEvent( this );

			listeners.remove( key );
			removeObjs();
		}

		@Override
		@SuppressWarnings( "unchecked" )
		public void onContentFail (Content<Throwable, ImageOptions> error) {
			done = true;

			surface.queueEvent( new BatchFailure((Content)error, objs, tcbs, empty) );

			listeners.remove( key );
			removeObjs();
		}

		/** Load a texture on GL thread */
		@Override public void run () {
			Integer texi = texCache.get( key );     // check one more time before loading a new texture.
			int texture;
			if (texi == null) {
				texture = Textures.loadTexture( bitmap, opt, false );  // load a new texture
				if (texture != 0)
					texCache.put( key, texture );
			}
			else
				texture = texi;

			if (objs != null)
				for (TextureObject obj : objs)
					obj.setTexture( texture );
			if (tcbs != null) {
				Content<Integer, TextureOptions> content = new Content<Integer, TextureOptions>( texture, key, opt );
				for (TextureCallback cb : tcbs)
					cb.onContentLoad( content );
			}

			surface.requestRender();
		}

		public void add (TextureObject object, TextureCallback callback) {
			synchronized (lock) {
				if (object != null) {
					if (objs == null) objs = new HashSet<TextureObject>( 1 );
					objs.add( object );
				}
				if (callback != null) {
					if (tcbs == null) tcbs = new HashSet<TextureCallback>( 1 );
					tcbs.add( callback );
				}
			}
		}

		public void remove (TextureObject object) {
			synchronized (lock) {
				if (objs != null)
					objs.remove( object );
			}
		}

		public void remove (TextureCallback tb) {
			synchronized (lock) {
				if (tcbs != null)
					tcbs.remove( tb );
			}
		}

		public boolean isEmpty () {
			return (objs == null || objs.isEmpty()) && (tcbs == null || tcbs.isEmpty());
		}

		public boolean isDone () {
			return done;
		}

		private void removeObjs () {
			if (objs != null)
				for (TextureObject o : objs)
					objmap.remove( o );
		}

		private void cancelAll () {
			synchronized (lock) {
				removeObjs();
				objs.clear();
				tcbs.clear();
				objs = null;
				tcbs = null;
			}
		}
	}

	private class BatchFailure implements Runnable {

		Content<Throwable, TextureOptions> error;
		Set<TextureObject> objs; Set<TextureCallback> tcbs;
		int empty;

		public BatchFailure (Content<Throwable, TextureOptions> error, Set<TextureObject> objs, Set<TextureCallback> tcbs, int empty) {
			this.error = error;
			this.objs = objs;
			this.tcbs = tcbs;
			this.empty = empty;
		}

		@Override public void run () {
			if (tcbs != null)
				if (objs != null)
					for (TextureObject obj : objs)
						obj.setTexture( empty );
			if (tcbs != null) {
				for (TextureCallback cb : tcbs)
					cb.onContentFail( error );
			}
			surface.requestRender();
		}
	}
}
