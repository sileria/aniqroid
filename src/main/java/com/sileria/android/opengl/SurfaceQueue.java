/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.opengl;

/**
 * EventQueue interface used by {@link com.sileria.android.opengl.TextureCache}.
 *
 * The purpose of this interface is to be able to provide a queuing mechanism that
 * will execute run() method on the GL thread. If you are using the existing
 * {@link android.opengl.GLSurfaceView}, then simply extend that class with your
 * own subclass and implement this interface on it.
 *
 * If you are using a custom surface view, then you will have to handle this
 * yourself also by providing your own implementation.
 *
 *
 * @author Ahmed Shakil
 * @date 01-02-2014
 */
public interface SurfaceQueue {

	/**
	 * Queue a runnable to be run on the GL rendering thread. This can be used
	 * to communicate with the Renderer on the rendering thread.
	 * Must not be called before a renderer has been set.
	 *
	 * @param r the runnable to be run on the GL rendering thread.
	 */
	void queueEvent (Runnable r);

	/**
	 * Request that the renderer render a frame.
	 */
	void requestRender ();
}
