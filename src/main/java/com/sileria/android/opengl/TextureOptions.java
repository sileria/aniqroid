/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.opengl;

import android.opengl.GLES20;

import com.sileria.android.util.ImageOptions;
import com.sileria.util.Utils;

/**
 * TextureOptions is used to determine loading options for a texture e.g. glMinFilter, glWrap, etc...
 *
 * @author Ahmed Shakil
 * @date 12-31-2013
 */
public class TextureOptions extends ImageOptions {

	/**
	 * Custom texture loader
	 */
	public TextureLoader loader;

	/** Param value for {@link GLES20#GL_TEXTURE_MIN_FILTER} */
	public int glMinFilter;

	/** Param value for {@link android.opengl.GLES20#GL_TEXTURE_MAG_FILTER} */
	public int glMagFilter;

	/** Param value for {@link android.opengl.GLES20#GL_TEXTURE_WRAP_S} */
	public int glWrapS = Utils.defaultIfZero( DEFAULT_TEX_WRAP, GLES20.GL_REPEAT );

	/** Param value for {@link android.opengl.GLES20#GL_TEXTURE_WRAP_T} */
	public int glWrapT = Utils.defaultIfZero( DEFAULT_TEX_WRAP, GLES20.GL_REPEAT );

	/** Default texture wrap parameter for ST. */
	public static int DEFAULT_TEX_WRAP = GLES20.GL_REPEAT;

	/**
	 * Check if has loading options.
	 */
	public boolean hasTexOptions () {
		return  (glWrapS != GLES20.GL_REPEAT
					|| glWrapT != GLES20.GL_REPEAT
					|| glMinFilter != 0
					|| glMagFilter != 0
					|| loader != null
				);
	}

	/**
	 * Returns <code>true</code> if nothing is set; otherwise <code>false.</code>
	 */
	@Override
	public boolean isEmpty () {
		return super.isEmpty() && !hasTexOptions();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals (Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals( o )) return false;

		TextureOptions that = (TextureOptions)o;

		if (glWrapS != that.glWrapS) return false;
		if (glWrapT != that.glWrapT) return false;
		if (glMinFilter != that.glMinFilter) return false;
		if (glMagFilter != that.glMagFilter) return false;

		if (loader != null ? !loader.equals( that.loader ) : that.loader != null) return false;

		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode () {
		int result = super.hashCode();
		result = 31 * result + glWrapS;
		result = 31 * result + glWrapT;
		result = 31 * result + glMinFilter;
		result = 31 * result + glMagFilter;
		result = 31 * result + (loader != null ? loader.hashCode() : 0);
		return result;
	}

	/**
	 * Utility method to avoid boiler code.
	 */
	public static boolean hasOptions (TextureOptions opt) {
		return opt != null && (opt.hasImageOptions() || opt.hasTexOptions());
	}
}
