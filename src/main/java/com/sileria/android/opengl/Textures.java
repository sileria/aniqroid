/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.opengl;

import android.graphics.*;
import android.opengl.GLU;
import android.text.TextPaint;

import com.sileria.android.Kit;
import com.sileria.android.Resource;
import com.sileria.android.util.BitmapSerializer;
import com.sileria.android.util.TextOptions;
import com.sileria.util.Log;
import com.sileria.util.Utils;

import static android.opengl.GLES20.*;
import static android.opengl.GLUtils.texImage2D;

/**
 * OpenGL Textures Utilities.
 *
 * @author Ahmed Shakil
 * @date 12-09-2013
 */
public class Textures {

	private static final String TAG = Kit.TAG+":TextureHelper";

	/**
	 * Loads a texture from a resource ID, returning the OpenGL ID for that
	 * texture. Returns 0 if the load failed.
	 */
	public static int load (int resourceId) {
		return load( resourceId, null );
	}

	/**
	 * Loads a texture from a resource ID, returning the OpenGL ID for that
	 * texture. Returns 0 if the load failed.
	 */
	public static int load (int resourceId, TextureOptions opt) {

		Bitmap bitmap = createBitmap( resourceId );

		if (bitmap == null) {
			Log.e( TAG, "Resource ID " + resourceId + " could not be decoded." );
			return 0;
		}

		int texture = load( bitmap, opt );  // load the texture from the bitmap

		bitmap.recycle(); // Recycle the bitmap, since its data has been loaded into OpenGL.

		return texture;
	}

	/**
	 * Loads a texture from a resource ID, returning the OpenGL ID for that
	 * texture. Returns 0 if the load failed.
	 */
	public static int load (Bitmap bitmap) {
		return loadTexture( bitmap, null, false );
	}

	/**
	 * Loads a texture from a resource ID, returning the OpenGL ID for that
	 * texture. Returns 0 if the load failed.
	 */
	public static int load (Bitmap bitmap, TextureOptions opt) {
		if (opt != null && opt.loader != null)
			return opt.loader.load( bitmap, opt );

		return loadTexture( bitmap, opt, opt != null );
	}

	/**
	 * Loads a texture from a resource ID, returning the OpenGL ID for that
	 * texture. Returns 0 if the load failed.
	 */
	protected static int loadTexture (Bitmap bitmap, TextureOptions opt, boolean applyFilters) {

		Bitmap old = bitmap;

		// create bitmap of correct size and check for filters
		bitmap = applyFilters && opt != null && opt.hasFilters() ?
				BitmapSerializer.applyFilters( bitmap, opt, true ) :
					createBitmap( bitmap );
		if (bitmap == null) {
			Log.e( TAG, "Null bitmap cannot be loaded as texture." );
			return 0;
		}

		// create texture
		final int[] textureObjectIds = new int[1];
		glGenTextures( 1, textureObjectIds, 0 );

		if (textureObjectIds[0] == 0) {
			int err = glGetError();
			Log.e( TAG, "glGenTextures() error: [" + err + "]  " + GLU.gluErrorString( err )  + ", in " + Thread.currentThread() );
			return 0;
		}

		// Bind to the texture in OpenGL
		glBindTexture( GL_TEXTURE_2D, textureObjectIds[0] );

		// Set the default or opt params
		setTexParams( opt );

		// Load the bitmap into the bound texture.
		texImage2D( GL_TEXTURE_2D, 0, bitmap, 0 );

		// Note: Following code may cause an error to be reported in the
		// ADB log as follows: E/IMGSRV(20095): :0: HardwareMipGen:
		// Failed to generate texture mipmap levels (error=3)
		// No OpenGL error will be encountered (glGetError() will return
		// 0). If this happens, just squash the source image to be
		// square. It will look the same because of texture coordinates,
		// and mipmap generation will work.

		glGenerateMipmap( GL_TEXTURE_2D );

		// Unbind from the texture.
		glBindTexture( GL_TEXTURE_2D, 0 );

		// recycle bitmap if created locally
		if (old != bitmap) {
			bitmap.recycle();
			bitmap = null;
		}

		return textureObjectIds[0];
	}

	private static void setTexParams (TextureOptions opt) {

		if (opt == null) {
			// Set filtering: a default must be set, or the texture will be black.
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

			// Set texture wrap parameter if not default.
			if (TextureOptions.DEFAULT_TEX_WRAP != 0 && TextureOptions.DEFAULT_TEX_WRAP != GL_REPEAT) {
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, TextureOptions.DEFAULT_TEX_WRAP );
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, TextureOptions.DEFAULT_TEX_WRAP );
			}
		}
		else {
			// Set filtering: a default must be set, or the texture will be black.
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, Utils.defaultIfZero( opt.glMinFilter, GL_NEAREST ) );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, Utils.defaultIfZero( opt.glMagFilter, GL_NEAREST ) );

			// Set texture wrap parameter: default is GL_REPEAT.
			if (opt.glWrapS != 0 && opt.glWrapS != GL_REPEAT)
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, opt.glWrapS );
			if (opt.glWrapT != 0 && opt.glWrapT != GL_REPEAT)
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, opt.glWrapT );
		}
	}

	/**
	 * Loads text as a bitmap texture.
	 */
	public static int load (int strId, TextOptions opt, Object ... args) {
		return strId == 0 ? 0 : load( createBitmap( Resource.getString( strId, args ), opt ), strTexOpt() );
	}

	/**
	 * Loads text as a bitmap texture.
	 */
	public static int load (int strId, Paint paint, Object ... args) {
		return strId == 0 ? 0 : load( createBitmap( Resource.getString( strId, args ), paint, null ), strTexOpt() );
	}

	/**
	 * Loads text as a bitmap texture.
	 */
	public static int load (String text, TextOptions opt) {
		return Utils.isEmpty( text ) ? 0 : load( createBitmap( text, opt ), strTexOpt() );
	}

	/**
	 * Loads text as a bitmap texture.
	 */
	public static int load (String text, Typeface font, int textSize, int textColor) {
		return Utils.isEmpty( text ) ? 0 : load( createBitmap( text, font, textSize, textColor ), strTexOpt() );
	}

	/**
	 * Loads text as a bitmap texture.
	 */
	public static int load (String text, Paint paint) {
		return load( createBitmap( text, paint, null ), strTexOpt() );
	}

	private static TextureOptions strTexOpt () {
		TextureOptions opt = new TextureOptions();
		opt.glMinFilter = GL_LINEAR_MIPMAP_LINEAR;
		opt.glMagFilter = GL_LINEAR;
		return opt;
	}

	/**
	 * Scales a bitmap into an area that is a power of 2, so that it can be used for OpenGL.
	 */
	public static Bitmap createBitmap (int bmpId) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;

		// Read in the resource
		Bitmap bitmap = Resource.getBitmap( bmpId, options );
		if (bitmap == null) return null;

		// Create bitmap
		Bitmap texture = createBitmap( bitmap );

		// Recycle if a new one was created.
		if (bitmap != texture) {
			bitmap.recycle();
			bitmap = null;
		}

		return texture;
	}

	/**
	 * Scales a bitmap into an area that is a power of 2, so that it can be used for OpenGL.
	 */
	public static Bitmap createBitmap (Bitmap bitmap) {
		if (bitmap == null) return null;

		int bw = bitmap.getWidth();
		int bh = bitmap.getHeight();

		int width = Utils.nextPow2( bw );
		int height = Utils.nextPow2( bh );

		return (bw == width && bh == height) ? bitmap : Bitmap.createScaledBitmap( bitmap, width, height, true );
	}

	/**
	 * Creates text into a bitmap
	 */
	public static Bitmap createBitmap (String text, TextOptions opt) {
		return createBitmap( text, null, opt );
	}

	/**
	 * Creates text into a bitmap
	 */
	protected static Bitmap createBitmap (String text, Paint paint, TextOptions opt) {
		return BitmapSerializer.createBitmap( text, paint, opt, true );
	}

	/**
	 * Creates text into a bitmap
	 */
	protected static Bitmap createBitmap (String text, Typeface font, float textSize, int textColor) {

		Paint paint = new TextPaint( Paint.ANTI_ALIAS_FLAG );
		paint.setTypeface( font );
		paint.setTextSize( textSize );
		paint.setColor( textColor );
		paint.setTextAlign( Paint.Align.LEFT );

		return BitmapSerializer.createBitmap( text, paint, null, true );
	}

	/**
	 * Loads a texture from a resource ID, returning the OpenGL ID for that
	 * texture. Returns 0 if the load failed.
	 */
	public static boolean delete (int ... texture) {
		glDeleteTextures(1, texture, 0);
		return true;
	}
}
