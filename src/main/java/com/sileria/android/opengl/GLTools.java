/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.opengl;

import java.nio.*;

import com.sileria.android.Kit;
import com.sileria.util.Log;

/**
 * GL utils class with a few handy methods.
 *
 * @author Ahmed Shakil
 * @date 12-21-2013
 */
public class GLTools {

	public static void copyM (float[] dst, float[] src) {
		System.arraycopy( src, 0, dst, 0, dst.length );
	}

	public static ByteBuffer allocaleBuffer (byte[] array) {
		ByteBuffer buff = ByteBuffer.allocateDirect( array.length )
				.order( ByteOrder.nativeOrder() )
				.put( array );
		buff.position( 0 );
		return buff;
	}

	public static ShortBuffer allocaleBuffer (short[] array) {
		ShortBuffer buff = ByteBuffer.allocateDirect( array.length * (Short.SIZE>>3) )
				.order( ByteOrder.nativeOrder() )
				.asShortBuffer()
				.put( array );
		buff.position( 0 );
		return buff;
	}

	public static IntBuffer allocaleBuffer (int[] array) {
		IntBuffer buff = ByteBuffer.allocateDirect( array.length * (Integer.SIZE>>3) )
				.order( ByteOrder.nativeOrder() )
				.asIntBuffer()
				.put( array );
		buff.position( 0 );
		return buff;
	}

	public static LongBuffer allocaleBuffer (long[] array) {
		LongBuffer buff = ByteBuffer.allocateDirect( array.length * (Long.SIZE>>3) )
				.order( ByteOrder.nativeOrder() )
				.asLongBuffer()
				.put( array );
		buff.position( 0 );
		return buff;
	}

	public static FloatBuffer allocaleBuffer (float[] array) {
		FloatBuffer buff = ByteBuffer.allocateDirect( array.length * (Float.SIZE>>3) )
				.order( ByteOrder.nativeOrder() )
				.asFloatBuffer()
				.put( array );
		buff.position( 0 );
		return buff;
	}

	public static DoubleBuffer allocaleBuffer (double[] array) {
		DoubleBuffer buff = ByteBuffer.allocateDirect( array.length * (Double.SIZE>>3) )
				.order( ByteOrder.nativeOrder() )
				.asDoubleBuffer()
				.put( array );
		buff.position( 0 );
		return buff;
	}

	/**
	 * Print given matrix to log
	 * @param matrix the matrix
	 * @param coordNumber coordinates in a row
	 * @param desc Description of what is being printed to the log. If null, defaults to "Matrix"
	 */
	public static String toString (float[] matrix, int coordNumber, String desc) {
		StringBuilder output = new StringBuilder(desc != null? desc: "Matrix").append( ": \n" );
		for(int i=0; i < matrix.length; i++) {
			output.append( matrix[i] ).append( "," );
			if (i > 0 &&  (i+1) % coordNumber == 0)
				output.append( "\n" );
		}
		output.append( "\n" );
		return output.toString();
	}

	/**
	 * Print given matrix to log
	 * @param matrix the matrix
	 */
	public static void logMatrix (float[] matrix) {
		logMatrix( matrix, 4, null );
	}

	/**
	 * Print given matrix to log
	 * @param matrix the matrix
	 * @param coordNumber coordinates in a row
	 * @param desc Description of what is being printed to the log. If null, defaults to "Matrix"
	 */
	public static void logMatrix (float[] matrix, int coordNumber, String desc) {
		Log.d( Kit.TAG, toString( matrix, coordNumber, desc ) );
	}


}
