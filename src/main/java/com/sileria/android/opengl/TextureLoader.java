/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.opengl;

import android.graphics.Bitmap;

/**
 * Texture loader interface used for providing custom loaders
 * that can be used by setting it to {@link com.sileria.android.opengl.TextureOptions}
 * and passing it to {@link com.sileria.android.opengl.TextureCache}'s get methods.
 *
 * A general loader is provided, but if you need more control then use this interface.
 *
 * @author Ahmed Shakil
 * @date 12-31-2013
 *
 * @see com.sileria.android.opengl.TextureOptions
 * @see com.sileria.android.util.ImageFilter
 */
public interface TextureLoader {

	/**
	 * Load a texture from specified Bitmap.
	 *
	 * This method will be called on the GL thread.
	 * Do not forget to rescale the bitmap to a power of 2.
	 *
	 * @param bitmap Raw/filtered bitmap to be loaded as a texture
	 *
	 * @return Texture handle id of the newly generated texture
	 */
	int load (Bitmap bitmap, TextureOptions opt);

}
