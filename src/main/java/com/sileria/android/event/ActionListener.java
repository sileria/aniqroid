/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.event;

import java.util.EventListener;

/**
 * The listener interface for receiving action events.
 *
 * The class that is interested in processing an action event
 * implements this interface, and the object created with that
 * class is registered with a component, using the component's
 * <code>addActionListener</code> method. When the action event
 * occurs, that object's <code>onAction</code> method is invoked.
 *
 * @author Ahmed Shakil
 * @date Nov 4, 2010
 */
public interface ActionListener extends EventListener {

	/**
	 * Invoked when an action occurs.
	 */
	public void onAction ();
}
