/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.bc;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.*;
import android.view.*;

import java.io.*;
import java.util.*;

import com.sileria.android.Kit;
import com.sileria.util.*;

/**
 * Wrapper.
 *
 * @author Ahmed Shakil
 * @date Dec 16, 2010
 */
public abstract class Wrapper {

	private static Wrapper instance;

	/**
	 * Always allow a user to over-scroll this view, provided it is a
	 * view that can scroll.
	 */
	public static final int OVER_SCROLL_ALWAYS = 0;

	/**
	 * Allow a user to over-scroll this view only if the content is large
	 * enough to meaningfully scroll, provided it is a view that can scroll.
	 */
	public static final int OVER_SCROLL_IF_CONTENT_SCROLLS = 1;

	/**
	 * Never allow a user to over-scroll this view.
	 */
	public static final int OVER_SCROLL_NEVER = 2;

	/**
	 * Layout direction constants.
	 */
	public enum LayoutDirection {
		/**
		 * Horizontal layout direction of this view is from Left to Right.
		 * Use with {@link #setLayoutDirection}.
		 */
		LAYOUT_DIRECTION_LTR (0),

		/**
		 * Horizontal layout direction of this view is from Right to Left.
		 * Use with {@link #setLayoutDirection}.
		 */
		LAYOUT_DIRECTION_RTL (1),

		/**
		 * Horizontal layout direction of this view is inherited from its parent.
		 * Use with {@link #setLayoutDirection}.
		 */
		LAYOUT_DIRECTION_INHERIT (2),

		/**
		 * Horizontal layout direction of this view is from deduced from the default language
		 * script for the locale. Use with {@link #setLayoutDirection}.
		 */
		LAYOUT_DIRECTION_LOCALE (3);

		final int value;

		private LayoutDirection (int value) {
			this.value = value;
		}
	}

	/**
	 * Text direction constants.
	 */
	public enum TextDirection {
		/**
		 * Text direction is inherited thru {@link ViewGroup}
		 */
		TEXT_DIRECTION_INHERIT (0),

		/**
		 * Text direction is using "first strong algorithm". The first strong directional character
		 * determines the paragraph direction. If there is no strong directional character, the
		 * paragraph direction is the view's resolved layout direction.
		 */
		TEXT_DIRECTION_FIRST_STRONG (1),

		/**
		 * Text direction is using "any-RTL" algorithm. The paragraph direction is RTL if it contains
		 * any strong RTL character, otherwise it is LTR if it contains any strong LTR characters.
		 * If there are neither, the paragraph direction is the view's resolved layout direction.
		 */
		TEXT_DIRECTION_ANY_RTL (2),

		/**
		 * Text direction is forced to LTR.
		 */
		TEXT_DIRECTION_LTR (3),

		/**
		 * Text direction is forced to RTL.
		 */
		TEXT_DIRECTION_RTL (4),

		/**
		 * Text direction is coming from the system Locale.
		 */
		TEXT_DIRECTION_LOCALE (5);

		final int value;

		private TextDirection (int value) {
			this.value = value;
		}
	}

	/**
	 * Text alignment constants.
	 */
	public enum TextAlignment {
		/**
		 * Default text alignment. The text alignment of this View is inherited from its parent.
		 */
		TEXT_ALIGNMENT_INHERIT (0),

		/**
		 * Default for the root view. The gravity determines the text alignment, ALIGN_NORMAL,
		 * ALIGN_CENTER, or ALIGN_OPPOSITE, which are relative to each paragraph’s text direction.
		 */
		TEXT_ALIGNMENT_GRAVITY (1),

		/**
		 * Align to the start of the paragraph, e.g. ALIGN_NORMAL.
		 */
		TEXT_ALIGNMENT_TEXT_START (2),

		/**
		 * Align to the end of the paragraph, e.g. ALIGN_OPPOSITE.
		 */
		TEXT_ALIGNMENT_TEXT_END (3),

		/**
		 * Center the paragraph, e.g. ALIGN_CENTER.
		 */
		TEXT_ALIGNMENT_CENTER (4),

		/**
		 * Align to the start of the view, which is ALIGN_LEFT if the view’s resolved
		 * layoutDirection is LTR, and ALIGN_RIGHT otherwise.
		 */
		TEXT_ALIGNMENT_VIEW_START (5),

		/**
		 * Align to the end of the view, which is ALIGN_RIGHT if the view’s resolved
		 * layoutDirection is LTR, and ALIGN_LEFT otherwise.
		 */
		TEXT_ALIGNMENT_VIEW_END (6);

		final int value;

		private TextAlignment (int value) {
			this.value = value;
		}
	}

	/**
	 * Singleton instance.
	 */
	public static Wrapper getInstance () {
		if (instance == null) {
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.DONUT)
				throw new UnsupportedOperationException( "Wrappers prior to 1.6 not available." );

			String latest;
			HashMap<Integer, String> map = new HashMap<>();
			map.put( 15, "com.sileria.android.bc.IceCream"             );
			map.put( 16, "com.sileria.android.bc.JellyBean"            );
			map.put( 17, "com.sileria.android.bc.JellyBeanMR1"         );
			map.put( 18, "com.sileria.android.bc.JellyBeanMR2"         );
			map.put( 19, "com.sileria.android.bc.KitKat"               );
			map.put( 20, "com.sileria.android.bc.KitKat"               );
			map.put( 21, "com.sileria.android.bc.LolliPop"             );
			map.put( 22, "com.sileria.android.bc.LolliPop"             );
			map.put( 23, "com.sileria.android.bc.Marshmallow"          );
			map.put( 24, latest = "com.sileria.android.bc.Nougat"      );

			String classname = Utils.defaultIfNull( map.get( Build.VERSION.SDK_INT ), latest );
			try {
				instance = (Wrapper)Class.forName( classname ).newInstance();
			}
			catch (Exception e) {
				throw new RuntimeException( "Serious error in Wrapper class", e );
			}
		}
		return instance;
	}

	/**
	 * Wrapper for {@link View#setPaddingRelative(int, int, int, int)}.
	 * For pre API 17 the {@link View#setPadding(int, int, int, int)} will be used.
	 *
	 * @param start the start padding in pixels (left for API 16 and below)
	 * @param top the top padding in pixels
	 * @param end the end padding in pixels (right for API 16 and below)
	 * @param bottom the bottom padding in pixels
	 */
	public void setPaddingRelative (View view, int start, int top, int end, int bottom) {
		view.setPadding( start, top, end, bottom );
	}

	/**
	 * Wrapper for {@link View#setLayoutDirection(int)}.
	 */
	public void setLayoutDirection (View view, LayoutDirection direction) {}

	/**
	 * Wrapper for {@link View#setTextDirection(int)}.
	 */
	public void setTextDirection (View view, TextDirection direction) {}

	/**
	 * Wrapper for {@link View#setTextAlignment(int)}.
	 */
	public void setTextAlignment (View view, TextAlignment alignment) {}

	/**
	 * Wrapper for {@link Drawable#setAutoMirrored(boolean)}.
	 */
	public void setAutoMirrored (Drawable d, boolean autoMirror) {}

	/**
	 * Wrapper for {@link android.app.AlarmManager#set(int, long, android.app.PendingIntent)}
	 */
	public void setExact (AlarmManager mgr, int type, long triggerAtMillis, PendingIntent operation) {
		mgr.set( type, triggerAtMillis, operation );
	}

	/**
	 * Creates a unique subdirectory of the designated app cache directory.
	 * Tries to use external but if not mounted, falls back on internal storage.
	 * NOTE: Supported API 14 and above.
	 * @param subdir sub directory to create
	 * @return a unique subdirectory of the designated app cache directory
	 */
	public File getCacheDir (String subdir) {
		Context context = Kit.getAppContext();

		// Check if media is mounted or storage is built-in, if so, try and use external cache dir
		// otherwise use internal cache dir
		File cacheFolder = Environment.MEDIA_MOUNTED.equals( Environment.getExternalStorageState() )
				|| !Environment.isExternalStorageRemovable() ?
				context.getExternalCacheDir() : context.getCacheDir();

		if (cacheFolder == null)
			cacheFolder = context.getCacheDir();

		return subdir == null ? cacheFolder : new File( cacheFolder, subdir );
	}

	/**
	 * Creates a unique subdirectory of the designated app files directory.
	 * Tries to use external but if not mounted, falls back on internal storage.
	 * NOTE: Supported API 14 and above.
	 * @param subdir sub directory to create
	 * @return a unique subdirectory of the designated app files directory
	 */
	public File getFilesDir (String subdir) {
		Context context = Kit.getAppContext();

		// Check if media is mounted or storage is built-in, if so, try and use external files dir
		// otherwise use internal files dir
		File filesFolder = Environment.MEDIA_MOUNTED.equals( Environment.getExternalStorageState() )
				|| !Environment.isExternalStorageRemovable() ?
				context.getExternalFilesDir(null) : context.getFilesDir();

		if (filesFolder == null)
			filesFolder = context.getFilesDir();

		return subdir == null ? filesFolder : new File( filesFolder, subdir );
	}

	/**
	 * Get the real exteneral sd card mount directory.
	 * Will return <code>null</code> whenever external sd card is not supported or mounted.
	 * @return external sd card mount directory
	 */
	public File getSDCardDir () {
		if (Environment.isExternalStorageRemovable() && Environment.MEDIA_MOUNTED.equals( Environment.getExternalStorageState() ))
			return Environment.getExternalStorageDirectory();

		File[] dirs = listStorageDirs( true );
		return dirs.length == 0 ? null : dirs[0];
	}

	/**
	 * Get possible exteneral sd card storage folders. The Environment class only
	 * returns one storage directory. If you have an extended SD card, it does not
	 * return the directory path. Here we are trying to return all of them.
	 *
	 * NOTE: This method only tries to find external sd cards and skips the Environment.getExternalStorageDirectory()
	 * when it is not removable.
	 *
	 * @param skipExternalStorage if <code>true</code> then the result will not include the
	 *                            environemnt's default "external" storage path returned by
	 *                            {@linkplain android.os.Environment#getExternalStorageDirectory()}
	 */
	public File[] listStorageDirs (boolean skipExternalStorage) {
		File[] dirs = IO.EMPTY_FILE_ARRAY;

		BufferedReader reader = null;
		try {
			File file = null;
			reader = new BufferedReader(new FileReader("/proc/mounts"));
			ArrayList<File> list = new ArrayList<File>();

			for (String line = reader.readLine(); line != null; line = reader.readLine(), file = null) {
				if (line.contains("vfat") || line.contains("/mnt")) {
					StringTokenizer tokens = new StringTokenizer(line, " ");
					String s = tokens.nextToken();
					s = tokens.nextToken(); // Take the second token, i.e. mount point

					if (s.equals( Environment.getExternalStorageDirectory().getAbsolutePath())) {
						if (!skipExternalStorage || Environment.isExternalStorageRemovable())
							file = Environment.getExternalStorageDirectory();
					}
					else if (line.contains("/dev/block/vold")) {
						if (!line.contains( "/mnt/secure" ) && !line.contains( "/mnt/asec" ) && !line.contains( "/mnt/obb" ) && !line.contains( "/dev/mapper" ) && !line.contains( "tmpfs" ))
							file = new File( s );
					}

					if (file != null && file.exists())
						list.add( file );
				}
			}

			dirs = list.toArray( new File[list.size()] );
		}
		catch (IOException e) {
			Log.e( Kit.TAG, "Unable to read /proc/mounts: " + e.getLocalizedMessage() );
		}
		finally {
			IO.close( reader );
		}

		return dirs;
	}

	/**
	 * {@link AppWidgetManager#getAppWidgetOptions(int)} introduced in API 16.
	 */
	public Bundle getAppWidgetOptions (AppWidgetManager appWidgetMgr, int appWidgetId) {
		return new Bundle();
	}

}