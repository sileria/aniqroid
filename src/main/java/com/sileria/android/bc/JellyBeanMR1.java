/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.bc;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.*;

/**
 * JellyBean 4.2 WRAPPER.  API Level - 17.
 *
 * @author Ahmed Shakil
 * @date Jan 16, 2012
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
class JellyBeanMR1 extends JellyBean {

	/**
	 * {@inheritDoc}
	 */
	public void setPaddingRelative (View view, int start, int top, int end, int bottom) {
		view.setPaddingRelative( start, top, end, bottom );
	}

	/**
	 * {@inheritDoc}
	 */
	public void setLayoutDirection (View view, LayoutDirection direction) {
		view.setLayoutDirection( direction.value );
	}

	/**
	 * {@inheritDoc}
	 */
	public void setTextDirection (View view, TextDirection direction) {
		view.setTextDirection( direction.value );
	}

	/**
	 * {@inheritDoc}
	 */
	public void setTextAlignment (View view, TextAlignment alignment) {
		view.setTextAlignment( alignment.value );
	}

}
