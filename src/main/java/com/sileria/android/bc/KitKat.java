/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.bc;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.graphics.drawable.Drawable;
import android.os.Build;

/**
 * JellyBean 4.4 WRAPPER.  API Level - 19.
 *
 * @author Ahmed Shakil
 * @date Nov 1, 2013
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
class KitKat extends JellyBeanMR2 {

	/**
	 * {@inheritDoc}
	 */
	public void setExact (AlarmManager mgr, int type, long triggerAtMillis, PendingIntent operation) {
		mgr.setExact( type, triggerAtMillis, operation );
	}

	/**
	 * {@inheritDoc}
	 */
	public void setAutoMirrored (Drawable d, boolean autoMirror) {
		d.setAutoMirrored( autoMirror );
	}

}
