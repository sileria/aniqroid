/*
 * Copyright (c) 2001 - 2016 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android;


import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.sileria.util.Utils;

/**
 * Compositing & Special Effects.
 *
 * @author Ahmed Shakil
 * @date Oct 27, 2007
 */

public final class Effects {

	/**
	 * Creates a translucent <code>Drawable</code> from the
	 * specified <code>image</code> with reflection on the bottom
	 * where the reflected area will be of same height and scale
	 * as the original image.
	 *
	 * @param image <code>Drawable</code> to create reflection of
	 * @return Newly created image with reflection
	 */
	public static Drawable createReflection (int image) {
		return createReflection( image, 1.0f );
	}

	/**
	 * Creates a translucent <code>Drawable</code> from the specified <code>image</code>
	 * and the reflected area height being the percent specified by <code>size</code>.
	 * <p/>
	 * By default the reflected image will not be scaled.
	 *
	 * @param image <code>Drawable</code> to create reflection of
	 * @param size  Reflected area height in percent of the original image height.
	 *              <code>1.0f</code> being 100% meaning the same height as the original image.
	 *              Valid argument must be greater than zero.
	 * @return Newly created image with reflection
	 * @see #createReflection(int, float, float, int)
	 */
	public static Drawable createReflection (int image, float size) {
		return createReflection( image, size, size, 0 );
	}

	/**
	 * Creates a translucent <code>Drawable</code> from <code>image</code>
	 * with specified <code>size</code> and <code>scale</code>.
	 *
	 * @param image <code>Drawable</code> to create reflection of
	 * @param size  Reflected area height in percent of the original image height.
	 *              <code>1.0f</code> being 100% meaning the same height as the original image.
	 *              Valid argument must be between zero and one. (with zero being exclusive)
	 * @param scale the reflection scale ratio to the original image height
	 *              <code>1.0f</code> being 100% meaning the same height as the original image
	 *              Valid argument must be between zero and one for scaling down the reflect,
	 *              and between 1 and 10 to scale up. (zero being exclusive)
	 * @param gap   padding to put between image bottom and reflection start.
	 * @return Newly created image with reflection
	 * @throws IllegalArgumentException if <code>size</code> or <code>scale</code>
	 *                                  are zero or negative value.
	 */
	public static Drawable createReflection (int image, float size, float scale, int gap) {
		Bitmap bmp = Resource.getBitmap( image );
		Drawable img = new BitmapDrawable( Resource.getResources(), createReflection( bmp, size, scale, gap ) );
		bmp.recycle();
		return img;
	}

	/**
	 * Creates a translucent <code>Bitmap</code> from the
	 * specified <code>image</code> with reflection on the bottom
	 * where the reflected area will be of same height and scale
	 * as the original image.
	 *
	 * @param image <code>Bitmap</code> to create reflection of
	 * @return Newly created image with reflection
	 */
	public static Bitmap createReflection (Bitmap image) {
		return createReflection( image, 1.0f );
	}

	/**
	 * Creates a translucent <code>Bitmap</code> from the specified <code>image</code>
	 * and the reflected area height being the percent specified by <code>size</code>.
	 * <p/>
	 * By default the reflected image will not be scaled.
	 *
	 * @param image <code>Bitmap</code> to create reflection of
	 * @param size  Reflected area height in percent of the original image height.
	 *              <code>1.0f</code> being 100% meaning the same height as the original image.
	 *              Valid argument must be greater than zero.
	 * @return Newly created image with reflection
	 * @see #createReflection(Bitmap, float, float, int)
	 */
	public static Bitmap createReflection (Bitmap image, float size) {
		return createReflection( image, size, size, 0 );
	}

	/**
	 * Creates a translucent <code>Bitmap</code> from <code>image</code>
	 * with specified <code>size</code> and <code>scale</code>.
	 *
	 * @param image <code>Bitmap</code> to create reflection of
	 * @param size  Reflected area height in percent of the original image height.
	 *              <code>1.0f</code> being 100% meaning the same height as the original image.
	 *              Valid argument must be between zero and one. (with zero being exclusive)
	 * @param scale the reflection scale ratio to the original image height
	 *              <code>1.0f</code> being 100% meaning the same height as the original image
	 *              Valid argument must be between zero and one for scaling down the reflect,
	 *              and between 1 and 10 to scale up. (zero being exclusive)
	 * @param gap   padding to put between image bottom and reflection start.
	 * @return Newly created image with reflection
	 * @throws IllegalArgumentException if <code>size</code> or <code>scale</code>
	 *                                  are zero or negative value.
	 */
	public static Bitmap createReflection (Bitmap image, float size, float scale, int gap) {

		if (size <= 0.f || size > 1f)
			throw new IllegalArgumentException( "size must be > 0.0 and < 1.0" );

		if (scale <= 0.f || scale > 10f)
			throw new IllegalArgumentException( "scale must be > 0.0 and < 10." );

		final int width = image.getWidth();
		final int height = image.getHeight();
		final int refSize = (int)(height * size);

		//This will not scale but will flip on the Y axis
		Matrix matrix = new Matrix();
		matrix.preScale( 1, -scale );

		//Create a Bitmap with the flip matix applied to it.
		//We only want the bottom half of the image
		Bitmap reflectionImage = Bitmap.createBitmap( image, 0, height - refSize, width, refSize, matrix, false );
		final int refHeight = reflectionImage.getHeight();

		//Create a new bitmap with same width but taller to fit reflection
		Bitmap newImage = Bitmap.createBitmap( width, height + refHeight + gap, Bitmap.Config.ARGB_8888 );

		//Create a new Canvas with the bitmap that's big enough for
		//the image plus gap plus reflection
		Canvas canvas = new Canvas( newImage );

		//Draw in the original image
		canvas.drawBitmap( image, 0, 0, null );

		//Draw in the reflection
		canvas.drawBitmap( reflectionImage, 0, height + gap, null );

		//Create a shader that is a linear gradient that covers the reflection
		LinearGradient shader = new LinearGradient( 0, image.getHeight(), 0,
				newImage.getHeight() + gap, 0x70ffffff, 0x00ffffff,
				Shader.TileMode.CLAMP );

		//Set the paint to use this shader (linear gradient)
		Paint paint = new Paint();
		paint.setShader( shader );

		//Set the Transfer mode to be porter duff and destination in
		paint.setXfermode( new PorterDuffXfermode( PorterDuff.Mode.DST_IN ) );

		//Draw a rectangle using the paint with our linear gradient
		canvas.drawRect( 0, height, width, newImage.getHeight() + gap, paint );

		// recycle unused
		reflectionImage.recycle();

		return newImage;
	}

	/**
	 * Creates and returns a translucent <code>Bitmap</code> from <code>image</code>
	 * with specified <code>size</code> and <code>scale</code> containing only the
	 * reflected piece and not the full image.
	 *
	 * @param image <code>Bitmap</code> to create reflection of
	 * @param size  Reflected area height in percent of the original image height.
	 *              <code>1.0f</code> being 100% meaning the same height as the original image.
	 *              Valid argument must be between zero and one. (with zero being exclusive)
	 * @param scale the reflection scale ratio to the original image height
	 *              <code>1.0f</code> being 100% meaning the same height as the original image
	 *              Valid argument must be between zero and one for scaling down the reflect,
	 *              and between 1 and 10 to scale up. (zero being exclusive)
	 * @return Newly created image with reflection
	 * @throws IllegalArgumentException if <code>size</code> or <code>scale</code>
	 *                                  are zero or negative value.
	 */
	public static Bitmap createReflectionOnly (Bitmap image, float size, float scale) {

		if (size <= 0.f || size > 1f)
			throw new IllegalArgumentException( "size must be > 0.0 and <= 1.0" );

		if (scale <= 0.f || scale > 10f)
			throw new IllegalArgumentException( "scale must be > 0.0 and <= 10." );

		final int width = image.getWidth();
		final int height = image.getHeight();
		final int refSize = (int)(height * size);

		//This will not scale but will flip on the Y axis
		Matrix matrix = new Matrix();
		matrix.preScale( 1, -scale );

		//Create a Bitmap with the flip matix applied to it.
		//We only want the bottom half of the image
		Bitmap newImage = Bitmap.createBitmap( image, 0, height - refSize, width, refSize, matrix, true );
		final int refHeight = newImage.getHeight();

		//Create a shader that is a linear gradient that covers the reflection
		LinearGradient shader = new LinearGradient( 0, 0, 0, refHeight, 0x70ffffff, 0x00ffffff, Shader.TileMode.CLAMP );

		//Set the paint to use this shader (linear gradient)
		Paint paint = new Paint();
		paint.setShader( shader );

		//Set the Transfer mode to be porter duff and destination in
		paint.setXfermode( new PorterDuffXfermode( PorterDuff.Mode.DST_IN ) );

		//Create a new Canvas with the bitmap that's big enough for
		//the image plus gap plus reflection
		Canvas canvas = new Canvas( newImage );

		//Draw a rectangle using the paint with our linear gradient
		canvas.drawRect( 0, 0, width, refHeight, paint );

		return newImage;
	}

	/**
	 * Crop the source bitmap in to a circular bitmap.
	 *
	 * @param src source bitmap
	 * @param w output width
	 * @param h output height
	 *
	 * @return newly created circular cropped bitmap
	 */
	public static Bitmap createCircularCrop (Bitmap src, int w, int h) {

		// create new bitmap
		int width = src.getWidth();
		int height = src.getHeight();
		Bitmap bitmap;
		if (w <= 0 || w != width || h <= 0 || h <= height)
			bitmap = Bitmap.createBitmap( width, height, src.getConfig() );
		else
			bitmap = Bitmap.createScaledBitmap( src, w, h, true );

		// canvas and paint
		final Paint paint = new Paint( Paint.ANTI_ALIAS_FLAG );
		final Canvas g = new Canvas( bitmap );
		final Rect rect = new Rect( 0, 0, width, height );

		// paint the white cirle
		paint.setColor( Color.WHITE );
		g.drawCircle( width >> 1, height >> 1, (width + height) >> 2, paint );

		// crop the image
		paint.setXfermode( new PorterDuffXfermode( PorterDuff.Mode.SRC_IN ) );
		g.drawBitmap( src, rect, rect, paint );
		return bitmap;
	}

	/**
	 * Crop the source bitmap in to a circular bitmap.
	 * @param src source bitmap
	 * @return newly created circular cropped bitmap
	 */
	public static Bitmap createCircularCrop (Bitmap src) {
		return createCircularCrop( src, -1, -1 );
	}

	/**
	 * Creates and returns a new <code>Bitmap</code> after applying the blue screen composite
	 * to the provided image where the specified <code>blue</code> color is replaced with <code>color</code>
	 *
	 * @param bmp        source bitmap
	 * @param blue       color of blue screen
	 * @param color      new color to be replaced
	 * @param tolerance  tolerance value between 0 - 1
	 *                   Tolerance near 0: avoid any colors even remotely similar to the op-color
	 *                   Tolerance near 1: avoid only colors nearly identical to the op-color
	 * @param outWidth   new width of the final
	 * @param outHeight  new height
	 * @return blue screen composited image
	 * @see AvoidXfermode
	 */
	public static Bitmap createChromaKeyComposite (Bitmap bmp, int blue, int color, float tolerance, int outWidth, int outHeight) {

		Bitmap overlay = createChromaKeyComposite( bmp, blue, color, tolerance );
		Bitmap image = Bitmap.createScaledBitmap( overlay, outWidth, outHeight, true );
		overlay.recycle();
		return image;
	}

	/**
	 * Creates and returns a new <code>Bitmap</code> after applying the blue screen composite
	 * to the provided image where the specified <code>blue</code> color is replaced with <code>color</code>
	 *
	 * NOTE: Since AvoidXfermode is deprecated and remove, this method no longer uses that method
	 * and the implementation is in Java. Please use a native implementation for better performance.
	 *
	 * @param src       source bitmap
	 * @param key       chroma key color
	 * @param color     new color to be replaced
	 * @param tolerance tolerance value between 0 - 1
	 *                  Tolerance near 0: avoid any colors even remotely similar to the op-color
	 *                  Tolerance near 1: avoid only colors nearly identical to the op-color
	 * @return blue screen composited image
	 */
	public static Bitmap createChromaKeyComposite (Bitmap src, int key, int color, float tolerance) {
		final int w = src.getWidth();
		final int h = src.getHeight();

		// create empty bitmap
		Bitmap dst = Bitmap.createBitmap( w, h, src.getConfig() );
		int[] pixels = new int[w * h];
		src.getPixels( pixels, 0, w, 0, 0, w, h );

		int tl = Utils.clamp( 0, 255, (int)(tolerance * 255) );
		int sR = Color.red( key );
		int sG = Color.green( key );
		int sB = Color.blue( key );
		int tR = Color.red( color );
		int tG = Color.green( color );
		int tB = Color.blue( color );
		float[] hsv = new float[3];
		Color.RGBToHSV( tR, tG, tB, hsv );
		float targetHue = hsv[0];
		float targetSat = hsv[1];
		float targetVal = hsv[2];

		for (int i = 0; i < pixels.length; ++i) {
			int pixel = pixels[i];

			if (pixel == key) {
				pixels[i] = color;
			}
			else {
				int pR = Color.red( pixel );
				int pG = Color.green( pixel );
				int pB = Color.blue( pixel );

				int deltaR = Math.abs( pR - sR );
				int deltaG = Math.abs( pG - sG );
				int deltaB = Math.abs( pB - sB );

				if (deltaR <= tl && deltaG <= tl && deltaB <= tl) {
					Color.RGBToHSV( pR, pG, pB, hsv );
					hsv[0] = targetHue;
					hsv[1] = targetSat;
					hsv[2] *= targetVal;

					int mixTrgColor = Color.HSVToColor( Color.alpha( pixel ), hsv );
					pixels[i] = mixTrgColor;
				}
			}
		}

		dst.setPixels( pixels, 0, w, 0, 0, w, h );
		return dst;
	}

	/**
	 * Creates and returns a new <code>Bitmap</code> after replace all non-transparent
	 * pixels with the specified <code>color</code>.
	 *
	 * @param src       source bitmap
	 * @param color     new color to be replaced
	 * @see PorterDuffXfermode
	 */
	public static Bitmap createTint (Bitmap src, int color) {
		final int w = src.getWidth();
		final int h = src.getHeight();

		// create empty bitmap
		Bitmap tint = Bitmap.createBitmap( w, h, src.getConfig() );

		// create empty canvas & paint
		Canvas g = new Canvas( tint );
		Paint p = new Paint();

		// create the filter to change color.
		p.setColorFilter( new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN) );

		// draw src bitmap on canvas
		g.drawBitmap( src, 0, 0, p );

		return tint;
	}

	/**
	 * Create a new image with gaussian blur applied to it.
	 *
	 * Stack Blur v1.0 from
	 * http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
	 *
	 * Java Author: Mario Klingemann <mario at quasimondo.com>
	 * http://incubator.quasimondo.com
	 * created Feburary 29, 2004
	 * Android port : Yahel Bouaziz <yahel at kayenko.com>
	 * http:*www.kayenko.com
	 * ported april 5th, 2012

	 * This is a compromise between Gaussian Blur and Box blur
	 * It creates much better looking blurs than Box Blur, but is
	 * 7x faster than my Gaussian Blur implementation.
	 *
	 * I called it Stack Blur because this describes best how this
	 * filter works internally: it creates a kind of moving stack
	 * of colors whilst scanning through the image. Thereby it
	 * just has to add one new block of color to the right side
	 * of the stack and remove the leftmost color. The remaining
	 * colors on the topmost layer of the stack are either added on
	 * or reduced by one, depending on if they are on the right or
	 * on the left side of the stack.
	 *
	 * If you are using this algorithm in your code please add
	 * the following line:
	 *
	 * Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>

	 * @param src Source Bitmap
	 * @return blurred Bitmap
	 */
	public static Bitmap createStackBlur (Bitmap src, int radius) {

		if (radius < 1) return null;

		Bitmap bitmap = src.copy( src.getConfig(), true );
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();

		int[] pix = new int[w * h];
		bitmap.getPixels( pix, 0, w, 0, 0, w, h );

		int wm = w - 1;
		int hm = h - 1;
		int wh = w * h;
		int div = radius + radius + 1;

		int r[] = new int[wh];
		int g[] = new int[wh];
		int b[] = new int[wh];
		int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
		int vmin[] = new int[Math.max( w, h )];

		int divsum = (div + 1) >> 1;
		divsum *= divsum;
		int dv[] = new int[256 * divsum];
		for (i = 0; i < 256 * divsum; i++) {
			dv[i] = (i / divsum);
		}

		yw = yi = 0;

		int[][] stack = new int[div][3];
		int stackpointer;
		int stackstart;
		int[] sir;
		int rbs;
		int r1 = radius + 1;
		int routsum, goutsum, boutsum;
		int rinsum, ginsum, binsum;

		for (y = 0; y < h; y++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			for (i = -radius; i <= radius; i++) {
				p = pix[yi + Math.min( wm, Math.max( i, 0 ) )];
				sir = stack[i + radius];
				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);
				rbs = r1 - Math.abs( i );
				rsum += sir[0] * rbs;
				gsum += sir[1] * rbs;
				bsum += sir[2] * rbs;
				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				}
				else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}
			}
			stackpointer = radius;

			for (x = 0; x < w; x++) {

				r[yi] = dv[rsum];
				g[yi] = dv[gsum];
				b[yi] = dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (y == 0) {
					vmin[x] = Math.min( x + radius + 1, wm );
				}
				p = pix[yw + vmin[x]];

				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[(stackpointer) % div];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi++;
			}
			yw += w;
		}
		for (x = 0; x < w; x++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			yp = -radius * w;
			for (i = -radius; i <= radius; i++) {
				yi = Math.max( 0, yp ) + x;

				sir = stack[i + radius];

				sir[0] = r[yi];
				sir[1] = g[yi];
				sir[2] = b[yi];

				rbs = r1 - Math.abs( i );

				rsum += r[yi] * rbs;
				gsum += g[yi] * rbs;
				bsum += b[yi] * rbs;

				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				}
				else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}

				if (i < hm) {
					yp += w;
				}
			}
			yi = x;
			stackpointer = radius;
			for (y = 0; y < h; y++) {
				// Preserve alpha channel: ( 0xff000000 & pix[yi] )
				pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (x == 0) {
					vmin[y] = Math.min( y + r1, hm ) * w;
				}
				p = x + vmin[y];

				sir[0] = r[p];
				sir[1] = g[p];
				sir[2] = b[p];

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[stackpointer];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi += w;
			}
		}

		bitmap.setPixels( pix, 0, w, 0, 0, w, h );

		return (bitmap);
	}

	/**
	 * Create a new image with gaussian blur applied to it.
	 *
	 * @param src Source Bitmap
	 * @return blurred Bitmap
	 */
	public static Bitmap createGaussianBlur (Bitmap src, int radius) {
		double[][] GaussianBlurConfig = new double[][]{
				{1, 2, 1},
				{2, 4, 2},
				{1, 2, 1}
		};
		ConvolutionMatrix convMatrix = new ConvolutionMatrix( 3 );
		convMatrix.applyConfig( GaussianBlurConfig );
		convMatrix.Factor = radius;
		convMatrix.Offset = 0;
		return ConvolutionMatrix.computeConvolution3x3( src, convMatrix );
	}

	/**
	 * Create an border image of <code>borderColor</code> and <code>borderSize</code>
	 * with inside area cut empty and rounded with radius specified by <code>cornerRadius</code>.
	 *
	 * @param width        width of the new bitmap
	 * @param height       height of the new bitmap
	 * @param borderColor  border color
	 * @param borderSize   border size
	 * @param cornerRadius inner rounded border radius
	 * @return newly created image with inner rounded empty area
	 */
	public static Bitmap createRoundedMatte (int width, int height, int borderColor, int borderSize, float cornerRadius) {

		// Create a transparent bitmap
		Bitmap matte = Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888 );
		Canvas g = new Canvas( matte ); // image canvas to draw

		// draw outer border
		Paint p = new Paint( Paint.ANTI_ALIAS_FLAG );
		p.setColor( borderColor );
		g.drawRect( new RectF( 0, 0, width, height ), p );

		// cut inner section using PorterDuffXfermode filter
		// A out B http://en.wikipedia.org/wiki/File:Alpha_compositing.svg
		p.setColor( Color.BLACK );
		p.setXfermode( new PorterDuffXfermode( PorterDuff.Mode.SRC_OUT ) );
		RectF rect = new RectF( borderSize, borderSize, width - borderSize, height - borderSize );
		g.drawRoundRect( rect, cornerRadius, cornerRadius, p );

		return matte;
	}

    /**
     * Returns brighter version of specified <code>color</code>.
     */
    public static int brighter (int color, float factor) {
        int a = Color.alpha( color );
        int r = Color.red( color );
        int g = Color.green( color );
        int b = Color.blue( color );

        /* From 2D group:
         * 1. black.brighter() should return grey
         * 2. applying brighter to blue will always return blue, brighter
         * 3. non pure color (non zero rgb) will eventually return white
         */
        int i = (int)(1f/(1f-factor));
        if ( r == 0 && g == 0 && b == 0) {
            return Color.argb( a, i, i, i );
        }
        if ( r > 0 && r < i ) r = i;
        if ( g > 0 && g < i ) g = i;
        if ( b > 0 && b < i ) b = i;

        return Color.argb(a,
                Math.min((int)(r / factor), 255),
                Math.min((int)(g / factor), 255),
                Math.min((int)(b / factor), 255));
    }

    /**
     * Returns darker version of specified <code>color</code>.
     */
    public static int darker (int color, float factor) {
        int a = Color.alpha( color );
        int r = Color.red( color );
        int g = Color.green( color );
        int b = Color.blue( color );

        return Color.argb( a,
                Math.max( (int)(r * factor), 0 ),
                Math.max( (int)(g * factor), 0 ),
                Math.max( (int)(b * factor), 0 ) );
    }

	/**
	 * Calculate the bounds of an image to fit inside a view after scaling and keeping the aspect ratio.
	 *
	 * @param vw  container view width
	 * @param vh  container view height
	 * @param iw  image width
	 * @param ih  image height
	 * @param out Rect that is provided to receive the result. If <code>null</code> then a new rect will be created
	 * @return Same rect object that was provided to the method or a new one if <code>out</code> was <code>null</code>
	 */
	public static Rect calcCenter (int vw, int vh, int iw, int ih, Rect out) {
		return calcCenter( vw, vh, iw, ih, false, out );
	}

	/**
	 * Calculate the bounds of an image to fit inside a view after scaling and keeping the aspect ratio.
	 *
	 * @param vw  container view width
	 * @param vh  container view height
	 * @param iw  image width
	 * @param ih  image height
	 * @param out Rect that is provided to receive the result. If <code>null</code> then a new rect will be created
	 * @return Same rect object that was provided to the method or a new one if <code>out</code> was <code>null</code>
	 */
	public static RectF calcCenter (int vw, int vh, int iw, int ih, RectF out) {
		return calcCenter( vw, vh, iw, ih, false, out );
	}

	/**
	 * Calculate the bounds of an image to fit inside a view after scaling and keeping the aspect ratio.
	 *
	 * @param vw           container view width
	 * @param vh           container view height
	 * @param iw           image width
	 * @param ih           image height
	 * @param neverScaleUp if <code>true</code> then it will scale images down but never up when fiting
	 * @param out          Rect that is provided to receive the result. If <code>null</code> then a new rect will be created
	 * @return Same rect object that was provided to the method or a new one if <code>out</code> was <code>null</code>
	 */
	public static Rect calcCenter (int vw, int vh, int iw, int ih, boolean neverScaleUp, Rect out) {

		double scale = Math.min( (double)vw / (double)iw, (double)vh / (double)ih );

		int h = (int)(!neverScaleUp || scale < 1.0 ? scale * ih : ih);
		int w = (int)(!neverScaleUp || scale < 1.0 ? scale * iw : iw);
		int x = ((vw - w) >> 1);
		int y = ((vh - h) >> 1);

		if (out == null)
			out = new Rect( x, y, x + w, y + h );
		else
			out.set( x, y, x + w, y + h );

		return out;
	}

	/**
	 * Calculate the bounds of an image to fit inside a view after scaling and keeping the aspect ratio.
	 *
	 * @param vw           container view width
	 * @param vh           container view height
	 * @param iw           image width
	 * @param ih           image height
	 * @param neverScaleUp if <code>true</code> then it will scale images down but never up when fiting
	 * @param out          Rect that is provided to receive the result. If <code>null</code> then a new rect will be created
	 * @return Same rect object that was provided to the method or a new one if <code>out</code> was <code>null</code>
	 */
	public static RectF calcCenter (int vw, int vh, int iw, int ih, boolean neverScaleUp, RectF out) {

		float scale = Math.min( (float)vw / (float)iw, (float)vh / (float)ih );

		float h = !neverScaleUp || scale < 1.0 ? scale * ih : ih;
		float w = !neverScaleUp || scale < 1.0 ? scale * iw : iw;
		float x = (vw - w) / 2;
		float y = (vh - h) / 2;

		if (out == null)
			out = new RectF( x, y, x + w, y + h );
		else
			out.set( x, y, x + w, y + h );

		return out;
	}

	/**
	 * Convolution Matrix
	 */
	public static class ConvolutionMatrix {

		public static final int SIZE = 3;

		public double[][] Matrix;
		public double Factor = 1;
		public double Offset = 1;

		//Constructor with argument of size
		public ConvolutionMatrix (int size) {
			Matrix = new double[size][size];
		}

		public void setAll (double value) {
			for (int x = 0; x < SIZE; ++x) {
				for (int y = 0; y < SIZE; ++y) {
					Matrix[x][y] = value;
				}
			}
		}

		public void applyConfig (double[][] config) {
			for (int x = 0; x < SIZE; ++x) {
				for (int y = 0; y < SIZE; ++y) {
					Matrix[x][y] = config[x][y];
				}
			}
		}

		public static Bitmap computeConvolution3x3 (Bitmap src, ConvolutionMatrix matrix) {
			int width = src.getWidth();
			int height = src.getHeight();
			Bitmap result = Bitmap.createBitmap( width, height, src.getConfig() );

			int A, R, G, B;
			int sumR, sumG, sumB;
			int[][] pixels = new int[SIZE][SIZE];

			for (int y = 0; y < height - 2; ++y) {
				for (int x = 0; x < width - 2; ++x) {

					// get pixel matrix
					for (int i = 0; i < SIZE; ++i) {
						for (int j = 0; j < SIZE; ++j) {
							pixels[i][j] = src.getPixel( x + i, y + j );
						}
					}

					// get alpha of center pixel
					A = Color.alpha( pixels[1][1] );

					// init color sum
					sumR = sumG = sumB = 0;

					// get sum of RGB on matrix
					for (int i = 0; i < SIZE; ++i) {
						for (int j = 0; j < SIZE; ++j) {
							sumR += (Color.red( pixels[i][j] ) * matrix.Matrix[i][j]);
							sumG += (Color.green( pixels[i][j] ) * matrix.Matrix[i][j]);
							sumB += (Color.blue( pixels[i][j] ) * matrix.Matrix[i][j]);
						}
					}

					// get final Red
					R = (int)(sumR / matrix.Factor + matrix.Offset);
					if (R < 0) { R = 0; }
					else if (R > 255) { R = 255; }

					// get final Green
					G = (int)(sumG / matrix.Factor + matrix.Offset);
					if (G < 0) { G = 0; }
					else if (G > 255) { G = 255; }

					// get final Blue
					B = (int)(sumB / matrix.Factor + matrix.Offset);
					if (B < 0) { B = 0; }
					else if (B > 255) { B = 255; }

					// apply new pixel
					result.setPixel( x + 1, y + 1, Color.argb( A, R, G, B ) );
				}
			}

			// final image
			return result;
		}
	}

//	/**
//	 * Creates an opaque <code>BufferedImage</code> from the
//	 * specified <code>image</code> with reflection on the bottom
//	 * where the reflected area will be of same height and scale
//	 * as the original image. The new image will have the specified
//	 * background color.
//	 *
//	 * @param image <code>BufferedImage</code> to create reflection of
//	 * @return Newly created image with reflection
//	 *
//	 * @see #createReflection(java.awt.image.BufferedImage, float, float)
//	 */
//	public static BufferedImage createReflection (BufferedImage image, Color bg) {
//		return makeOpaque( createReflection( image ), bg );
//	}
//
//	/**
//	 * Creates an opaque <code>BufferedImage</code> from the specified <code>image</code>
//	 * and the reflected area height being the percent specified by <code>size</code>.
//	 * The opaque image will have the background color specified by <code>bg</code>.
//	 * <p/>
//	 * By default the reflected image will not be scaled.
//	 *
//	 * @param image <code>BufferedImage</code> to create reflection of
//	 * @param size Reflected area height in percent of the original image height.
//	 * 			<code>1.0f</code> being 100% meaning the same height as the original image.
//	 * 			Valid argument must be greater than zero.
//	 *
//	 * @return Newly created image with reflection
//	 *
//	 * @see #createReflection(java.awt.image.BufferedImage, float, float)
//	 */
//	public static BufferedImage createReflection (BufferedImage image, float size, Color bg) {
//		return makeOpaque( createReflection( image, size ), bg );
//	}
//
//	/**
//	 * Creates an opaque <code>BufferedImage</code> from the <code>image</code>
//	 * with specified <code>size</code> and <code>scale</code>.
//	 * The new opaque image will have the specified background color.
//	 *
//	 * @param image <code>BufferedImage</code> to create reflection of
//	 * @param size Reflected area height in percent of the original image height.
//	 * 			<code>1.0f</code> being 100% meaning the same height as the original image.
//	 * 			Valid argument must be greater than zero.
//	 * @param scale Reflected image vertical scale ratio to the original image height
//	 * 			<code>1.0f</code> being 100% meaning the same height as the original image
//	 * 			Valid argument must be greater than zero.
//	 * @return Newly created image with reflection
//	 *
//	 * @throws IllegalArgumentException if <code>size</code> or <code>scale</code>
//	 * 			are zero or negative value.
//	 *
//	 * @see #createReflection(java.awt.image.BufferedImage, float, float)
//	 */
//	public static BufferedImage createReflection (BufferedImage image, float size, float scale, Color bg) {
//		return makeOpaque( createReflection( image, size, scale), bg );
//	}
//
//
}
