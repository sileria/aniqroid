/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.adapter;

import android.content.Context;

import java.util.Arrays;
import java.util.List;

import com.sileria.util.CheckList;

/**
 * CheckListAdapter.
 *
 * @author Ahmed Shakil
 * @date 10-07-2012
 */
public abstract class CheckListAdapter<T> extends AbstractListAdapter<T> {

	private final CheckList<T> checklist;

	/**
	 * Constructor
	 */
	protected CheckListAdapter (Context ctx) {
		this( ctx, null );
	}

	/**
	 * Constructor
	 */
	protected CheckListAdapter (Context ctx, List<T> data) {
		super( ctx, data == null ? new CheckList<T>() : new CheckList<>( data ) );
		checklist = ((CheckList<T>)this.data);
	}

	@Override
	public void setData (List<T> data) {
		setData( data, false );
	}

	public void setData (List<T> data, boolean preserveSelections) {
		checklist.setListData( data, preserveSelections );
		notifyDataSetChanged();
	}

	@Override
	public List<T> getData () {
		return checklist.getListData();
	}

	/**
	 * Get the number of rows that have the selected check.
	 * @return selection count
	 */
	public int getSelectionCount () {
		return checklist.getSelectionCount();
	}

	/**
	 * Get an array of selected objects.
	 *
	 * @return Returns a non null array object
	 */
	public Object[] getSelections () {
		return checklist.getSelections();
	}

	/**
	 * Get selected objects into an array of the specified type.
	 * @param classType class
	 * @return Returns a non null array object
	 */
	public T[] getSelections (Class<?> classType) {
		return checklist.getSelections( classType );
	}

	/**
	 * Select elements from the specified array of objects.
	 */
	@SafeVarargs
	public final void setSelections (T ... selections) {
		setSelections( Arrays.asList( selections ) );
	}

	/**
	 * Select elements from the specified list of objects.
	 */
	public void setSelections (List<T> selections) {
		checklist.setSelections( selections );
		notifyDataSetChanged();
	}

	/**
	 * Unselect elements from the specified array of objects.
	 */
	@SafeVarargs
	public final void removeSelections (T ... selections) {
		removeSelections( Arrays.asList( selections ) );
	}

	/**
	 * Unselect elements from the specified list of objects.
	 */
	public void removeSelections (List<T> selections) {
		checklist.removeSelections( selections );
		notifyDataSetChanged();
	}

	/**
	 * Set the selected state of specified row <code>item</code>.
	 * @param item Item to select or deselect
	 * @param selected selected state
	 * @return the previous selected state
	 */
	public boolean setSelected (T item, boolean selected) {
		boolean b = checklist.setSelected( item, selected );
		selection = selected ? item : null;
		notifyDataSetChanged();
		return b;
	}

	/**
	 * Set the row <code>index</code> to the specifed <code>selected</code> state.
	 * @param index row index
	 * @param selected selected state
	 * @return the previous selected state
	 */
	public boolean setSelected (int index, boolean selected) {
		return setSelected( checklist.get( index ), selected );
	}

	/**
	 * Toggle the selection state of sepcirow <code>index</code> betwee
	 * @param item Item to select or deselect
	 * @return the new selected state
	 */
	public boolean setToggle (T item) {
		boolean selected = checklist.setToggle( item );
		selection = selected ? item : null;
		notifyDataSetChanged();
		return selected;
	}

	/**
	 * Toggle the selection state of specified row <code>index</code>.
	 * @param index row index
	 * @return the new selected state
	 */
	public boolean setToggle (int index) {
		return setToggle( checklist.get( index ) );
	}

	/**
	 * Selects or deselects all rows in the list based on the specified <code>selected</code> state.
	 * @param selected selection state
	 */
	public void setSelectedAll (boolean selected) {
		checklist.setSelectedAll( selected );
		notifyDataSetChanged();
	}

	/**
	 * Returns <tt>true</tt> if specified <code>obj</code> is selected
	 * @return <tt>true</tt> if specified <code>obj</code> is selected
	 */
	public boolean isSelected (T obj) {
		return checklist.isSelected( obj );
	}

	/**
	 * Returns <tt>true</tt> if specified row <code>index</code> is selected
	 * @return <tt>true</tt> if specified row <code>index</code> is selected
	 */
	public boolean isSelected (int index) {
		return checklist.isSelected( index );
	}

	/**
	 * Clears all the selected checks.
	 */
	public void clearSelections () {
		selection = null;
		checklist.clearSelections();
		notifyDataSetChanged();
	}

	/**
	 * Clear all items and selection
	 */
	@Override
	public void clear () {
		selection = null;
		checklist.clear();
		super.clear();
	}

	/**
	 * Get the last selected item or first selected item.
	 */
	@Override
	public T getSelection () {
		if (selection != null)
			return selection;

		if (checklist.getSelectionCount() > 0)
			return (selection = checklist.selectionIterator().next());

		return null;
	}

	/**
	 * Returns <code>true</code> if has any selections; otherwise <code>false</code>
	 */
	public boolean hasSelection () {
		return checklist.hasSelection();
	}
}
