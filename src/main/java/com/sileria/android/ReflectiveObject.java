/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android;

import com.sileria.util.LazyObject;

/**
 * ReflectiveObject is an extension of {@link com.sileria.util.LazyObject} class
 * that lets you create an object from a class name, instead of a class object.
 *
 * This class can be used to delay loading of the Class for the
 * instance to be created.
 *
 * The improved performance is at the cost of a slight performance
 * reduction the first time <code>get</code> (since Reflection APIs are used)
 *
 * By default, once an object is created its stored for future retreivals.
 *
 * This behavior can be changed by calling the alternate constructor
 * In that case it will create a new instance everytime <code>get</code>
 * is called at cost of slight performance reduction at each call.
 * <p/>
 * <strong>Example 1: Lazy Object with Constructor Parameter</strong><br>
 * <pre>
 * {@code
 * LazyObject<User> lazyObj = new ReflectiveObject<>( "com.sileria.aniqroid.samples.model.User" );
 * String i = lazyObject.get().getName();                // Same as: creating an object of the User Class and getting its name
 * System.out.println( i );                             // Outputs: the name
 * }
 *
 * </pre>
 * <p/>
 *
 * Example 2: Singleton Object with Constructor Parameter<br>
 * <pre>
 * {@code
 * LazyObject<Integer> lazyObject = new ReflectiveObject<>( "java.lang.Integer", true, 1000 );
 * Integer i = lazyObject.get();              // Returns singleton Integer object with 1000 value;
 * System.out.println( i );                   // Outputs: 1000
 * }
 * </pre>
 * <p/>
 *
 * Example 3: Object with fixed Parameters in the Constructor <br>
 * <pre>
 * {@code
 * ReflectiveObject<Integer> fixedParamObj   = new ReflectiveObject<>( "java.lang.Integer", false, 500 );
 * Integer i = fixedParamObj.get();            // Same as: new Integer( 500 );
 * System.out.println( i );                    // Outputs: 500
 * }
 * </pre>
 * <p/>
 *
 * Example 4: Object with dynamics Parameters in the Constructor <br>
 * <pre>
 * {@code
 * ReflectiveObject<Integer> dynamicParamObj = new ReflectiveObject<>( "java.lang.Integer" );
 * Integer i = dynamicParamObj.newInstance( ++counter );	 		// It increases one with each event
 * System.out.println(dynamicParamObj.newInstance( ++counter );		// Outputs: 1,2,3,4,5...
 * }
 * </pre>
 *  <p/>
 *
 * @author Ahmed Shakil
 * @date Oct 26, 2008
 *
 * @param <T> Return type of the <code>get</code> method.
 */

public class ReflectiveObject<T> extends LazyObject<T> {

	private final String classname;

	/**
	 * Construct a proxy for the specified <code>clazz</code>.
	 * @param classname Class to be generated.
	 */
	public ReflectiveObject (String classname) {
        this( classname, false );
	}

	/**
	 * Construct a proxy for the specified <code>clazz</code>.
     * @param classname Class to be generated.
	 * @param singleton Default behaviour of this proxy class.
	 */
	public ReflectiveObject (String classname, boolean singleton) {
        super( null, singleton );
        this.classname = classname;
	}

	/**
	 * Construct a proxy for the specified <code>clazz</code> with provided constructor arguments.
     * @param classname Class to be generated.
	 * @param singleton Default behaviour of this proxy class.
	 * @param args Constructor arguments
	 */
	public ReflectiveObject (String classname, boolean singleton, Object ... args) {
		super( null, singleton, args );
        this.classname = classname;
	}

    /**
     * Returns the class for the object to be instantiated.
     * @return the class for the object to be instantiated.
     */
    @SuppressWarnings ("unchecked")
    protected Class<T> getClazz () throws ClassNotFoundException {
        return (Class<T>) Class.forName( classname );
    }

    /**
     * Returns the class name for the object to be instantiated.
     * @return the class name for the object to be instantiated.
     */
    protected String getClassName () {
        return classname;
    }


}