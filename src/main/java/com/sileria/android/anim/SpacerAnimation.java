/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.android.anim;

import android.view.animation.*;

/**
 * Blank filler animation that only contains a duration.
 *
 * @author Ahmed Shakil
 * @date 4/21/11
 */
public class SpacerAnimation extends Animation {

	/**
	 * Construct an Animation object with the specified <code>duration</code>.
	 */
	public SpacerAnimation (long duration) {
		setDuration( duration );
	}

}
