/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.net;

import java.io.*;
import java.net.URL;

import com.sileria.util.ParseException;

/**
 * A WebService request that retrieves the requested data into a byte array.
 * <p/>
 * The retrieved data can be then parsed into desired object by providing a parser to this class
 * or the raw byte[] array can be retrieved by calling {@link #getRaw()}.
 * <p/>
 *
 * <strong>Example:</strong>
 * <blockquote><pre>
 *  public static Cancellable loadImage (URL webserviceUrl, RemoteCallback callback) {
 *      BinaryRequest req = new BinaryRequest (webServiceUrl);
 *      req.setParser(new DrawableParser());   // parser that will convert bytes into a Drawable image.
 *      return new RemoteTask(callback).execute( req );
 *  }
 * </pre></blockquote>
 * <p/>
 * Parser can be provided in form of a {@link com.sileria.util.DataParser} or a {@link com.sileria.util.ParserFactory}.
 *
 * @author Ahmed Shakil
 * @date Mar 18, 2010
 *
 * @param <T> parsed data type
 */
public class StreamRequest<T> extends ParsedRequest<T, InputStream> {

    /**
     * Constructor, default.
     */
    public StreamRequest () {
    }

	/**
	 * Construct a remote request with the specified callback.
	 * @param callback a {@link com.sileria.net.RemoteCallback}
	 */
    public StreamRequest (RemoteCallback<T> callback) {
        super( callback );
    }

	/**
	 * Construct a remote request with the specified url.
	 * @param url request url.
	 */
    public StreamRequest (URL url) {
        super( url, null );
    }

	/**
	 * Construct a remote request with the specified url and callback.
	 * @param url request url.
	 * @param callback a {@link com.sileria.net.RemoteCallback}
	 */
    public StreamRequest (URL url, RemoteCallback<T> callback) {
        super( url, callback );
    }

	/**
	 * Read and parse into data of type &lt;T&gt; from the http url connection.
	 * @return Object of type &lt;T&gt;
	 *
	 * @throws java.io.IOException in case of error
	 */
	@Override
	protected T readData (URL url) throws IOException, ParseException {
		HttpSerializer<T> reader = new HttpSerializer<T>( new ParserAdapter() );
		this.reader = setupReader( reader );
		reader.setCheckReponseBeforeRead( true );
		return reader.readData( url );
	}

	/**
	 * Try to get response code from reader if possible while still processing the data,
	 * since the read will be happening while the processData() call is made.
	 */
	public int getResponseCode () {
		int responseCode = super.getResponseCode();
		if (responseCode < 0 && reader instanceof HttpSerializer)
			responseCode = ((HttpSerializer)reader).getResponseCode();
		return responseCode;
	}

	/** Overriden to not throw extra exceptions and ignore raw data. */
	@Override
	protected T processData (InputStream data) throws ParseException, RemoteException {
		return parser == null ? null : parser.parse( data );
	}

	/** Forbidden. */
	@Override
	protected final ByteArrayOutputStream readBytes (URL url) {
		throw new IllegalStateException( "Should never come here." );
	}

	/** Forbidden. */
	@Override
	protected final T processData (ByteArrayOutputStream data) {
		throw new IllegalStateException( "Should never come here." );
	}

	/** Forbidden. */
	@Override
	protected final T processData (byte[] data) {
		throw new IllegalStateException( "Should never come here." );
	}

	/** Forbidden. Raw data will always be null for this class.*/
	@Override
	public final InputStream executeRaw () {
		throw new UnsupportedOperationException( "StreamRequest does not support this method." );
	}

	/** Forbidden. Raw data will always be null for this class.*/
	@Override
	public final InputStream getRaw () {
		throw new UnsupportedOperationException( "StreamRequest does not support this method." );
	}

	private class ParserAdapter implements RemoteParser<T, InputStream> {

		@Override
		public T parse (InputStream data) throws ParseException, RemoteException {
			return processData( data );
		}
	}

}