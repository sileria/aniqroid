/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.net;

import java.io.*;

import com.sileria.util.Utils;

/**
 * Default implementation of RemoteReader.
 *
 * @author Ahmed Shakil
 */
public class HttpReader extends HttpSerializer<ByteArrayOutputStream> implements RemoteReader {

	/**
	 * Constructor, default.
	 */
	public HttpReader () {
	}

	/**
	 * Constuctor specifing initial buffer size and http time out.
	 * @param buffSize initial read buffer size
	 * @param timeout timeout in milliseconds.
	 */
	public HttpReader (int buffSize, int timeout, boolean useCache) {
		super( buffSize, timeout, useCache );
	}

	/**
	 * Method provided for backward compatibility.
	 *
	 * @see #readData(java.io.InputStream)
	 */
	public ByteArrayOutputStream readBytes (InputStream in) throws IOException {
		return readData( in );
	}

	/**
	 * Read a <code>String</code> from specified <code>InputStream</code>.
	 *
	 * @param in InputStream
	 * @return Bytes read
	 * @throws IOException in case of error
	 */
	public String readString (InputStream in) throws IOException {
		return in == null ? null : readData( in ).toString();
	}

	/**
	 * Read an <code>InputStream</code> into a ByteArrayOutputStream.
	 *
	 * @param in InputStream
	 * @return Bytes read
	 * @throws IOException in case of error
	 */
	@Override
	public ByteArrayOutputStream readData (InputStream in) throws IOException {

		ByteArrayOutputStream bos = null;
		read = true;

		if (in == null)
			return bos;

		int chunkSize = Utils.clamp( Math.min( 32, buffSize ), Math.max( 32, buffSize ), in.available() );

		bos = new ByteArrayOutputStream( chunkSize );
		int result = in.read();
		while (result != -1 && read) {
			byte b = (byte) result;
			bos.write( b );
			result = in.read();
		}

		return bos;
	}

}
