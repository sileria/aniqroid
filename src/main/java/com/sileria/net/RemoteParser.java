/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.net;

import com.sileria.util.ParseException;

/**
 * RemoteParser.  Similar to {@link com.sileria.util.DataParser} except that the parse method
 * can throw both; {@linkplain ParseException} as well as {@linkplain RemoteException}.
 *
 * @author Ahmed Shakil
 * @date August 28, 2013
 *
 * @param <T> Parsed object type which is returned
 * @param <R> Raw data type
 */
public interface RemoteParser<T, R> {

	/**
	 * Parse search result into object.
	 *
	 * @throws com.sileria.util.ParseException in case of any unexpected parsing problem
	 */
	public T parse (R data) throws ParseException, RemoteException;

}
