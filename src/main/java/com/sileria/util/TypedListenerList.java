/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.util;

import java.io.*;
import java.lang.reflect.Array;

/**
 * TypedListenerList; typed version of {@link EventListenerList}.
 * <p/>
 * A class that holds a list of EventListeners.  A single instance
 * can be used to hold all listeners (of all types) for the instance
 * using the list.  It is the responsiblity of the class using the
 * EventListenerList to provide type-safe API (preferably conforming
 * to the JavaBeans spec) and methods which dispatch event notification
 * methods to appropriate Event Listeners on the list.
 * <p/>
 * The main benefits that this class provides are that it is relatively
 * cheap in the case of no listeners, and it provides serialization for
 * event-listener lists in a single place, as well as a degree of MT safety
 * (when used correctly).
 * <p/>
 * Usage example:
 * Say one is defining a class that sends out FooEvents, and one wants
 * to allow users of the class to register FooListeners and receive
 * notification when FooEvents occur.  The following should be added
 * to the class definition:
 * <pre>
 * public enum FooEvent { START, STOP, DONE }
 *
 * TypedListenerList&lt;FooListener, FooEvent&gt; listenerList
 *                   = new TypedListenerList&lt;FooListener, FooEvent&gt;(FooListener.class);
 *
 * public void addFooListener(FooListener l, FooEvent ev) {
 *     listenerList.add(l, ev);
 * }
 *
 * public void removeFooListener(FooListener l, FooEvent ev) {
 *     listenerList.remove(l, ev);
 * }
 *
 *
 * // Notify all listeners that have registered interest for
 * // notification on this event type.  The event instance
 * // is lazily created using the parameters passed into
 * // the fire method.
 *
 * protected void fireFooEvent (Event event) {
 *     // Guaranteed to return a non-null array
 *     Object[] listeners = listenerList.getListenerList();
 *     // Process the listeners last to first, notifying
 *     // those that are interested in this event
 *     for (int i = listeners.length-2; i>=0; i-=2) {
 *         if (listeners[i]==event || listeners[i]==null) {  // null means all event listener.
 *             ((FooListener)listeners[i+1]).onFooEvent(event);
 *		 }
 *	 }
 * }
 * </pre>
 * foo should be changed to the appropriate name, and fireFooXxx to the
 * appropriate method name.  One fire method should exist for each
 * notification method in the FooListener interface.
 * <p/>
 *
 * @author Ahmed Shakil
 * @date Aug 19, 2013
 * @param <T> tag type
 * @param <C> class type
 */
public class TypedListenerList<C, T> implements Serializable {

	private static final long serialVersionUID = -6599326220745118320L;

	/** The list of ListenerType - Listener pairs **/
	protected transient Object[] listenerList = Utils.EMPTY_OBJECT_ARRAY;

	protected Class<C> type;

	/**
	 * Construct a typed listener list.
	 * @param type listener class type
	 */
	public TypedListenerList (Class<C> type) {
		if (type == null)
			throw new NullPointerException( "Class-type cannot be null." );
		this.type = type;
	}

	/**
	 * Passes back the event listener list as an array  of ListenerType-listener pairs.
	 * Note that for performance reasons, this implementation passes back the actual data
	 * structure in which the listener data is stored internally!
	 *
	 * This method is guaranteed to pass back a non-null array, so that no null-checking
	 * is required in fire methods.  A zero-length array of Object should be returned if there
	 * are currently no listeners.
	 * <p/>
	 * WARNING!!! Absolutely NO modification of the data contained in this array should be made -- if
	 * any such manipulation is necessary, it should be done on a copy of the array returned rather
	 * than the array itself.
	 */
	public Object[] getListenerList () {
		return listenerList;
	}

	/**
	 * Return an array of all the listeners of the given type.
	 *
	 * @return all of the listeners of the specified type.
	 * @throws ClassCastException if the supplied class is not assignable to EventListener
	 */
	@SuppressWarnings( "unchecked" )
	public C[] getListeners (T tag) {
		Object[] lList = listenerList;
		int n = getListenerCount( lList, tag );
		C[] result = (C[])Array.newInstance( type, n );
		for (int i = lList.length - 2, j=0; i >= 0; i -= 2) {
			if (equals(lList[i], tag ))
				result[j++] = (C)lList[i + 1];
		}
		return result;
	}

	/**
	 * Returns the total number of listeners for this listener list.
	 */
	public int getListenerCount () {
		return listenerList.length / 2;
	}

	/**
	 * Returns the total number of listeners of the supplied type
	 * for this listener list.
	 */
	public int getListenerCount (T tag) {
		Object[] lList = listenerList;
		return getListenerCount( lList, tag );
	}

	/**
	 * Listener count
	 */
	private int getListenerCount (Object[] list, T tag) {
		int c = 0;
		for (int i = 0; i < list.length; i += 2) {
			if (equals(list[i], tag ))
				c++;
		}
		return c;
	}

	/**
	 * Adds the listener as a listener of the specified type.
	 *
	 * NOTE: This method does not allow same listener for
	 * the same class type to be added more than once.
	 * Same listener can be added for a different tag.
	 *
	 * @param listener the listener to be added
	 * @param tags the type of the listener to be added
	 */
	public synchronized void add (C listener, T ... tags) {
		if (Utils.isEmpty( tags ))
			add( listener, (T)null );
		else
			for (T tag : tags)
				add( listener, tag );
	}

	/**
	 * Adds the listener as a listener of the specified type.
	 *
	 * NOTE: This method does not allow same listener for
	 * the same class type to be added more than once.
	 * Same listener can be added for a different tag.
	 *
	 * @param listener the listener to be added
	 * @param tag the type of the listener to be added
	 */
	public synchronized void add (C listener, T tag) {
		if (listener == null) return;

		if (listenerList == Utils.EMPTY_OBJECT_ARRAY) {
			listenerList = new Object[]{tag, listener};    // if this is the first listener added, initialize the lists
		}
		else {
			if (indexOf( listener, tag ) >= 0) return;     // if already exist then do not do anything.

			// Otherwise copy the array and add the new listener
			int i = listenerList.length;
			Object[] tmp = new Object[i + 2];
			System.arraycopy( listenerList, 0, tmp, 0, i );

			tmp[i] = tag;
			tmp[i + 1] = listener;

			listenerList = tmp;
		}
	}

	/**
	 * Removes the listener as a listener of the specified type.
	 *
	 * @param listener the listener to be removed
	 * @param tag the type of the listener to be removed
	 */
	public synchronized void remove (C listener, T tag) {
		if (listener == null) return;

		int index = indexOf( listener, tag );   // Is l on the list?

		// If so,  remove it
		if (index != -1) {
			Object[] tmp = new Object[listenerList.length - 2];

			// Copy the list up to index
			System.arraycopy( listenerList, 0, tmp, 0, index );

			// Copy from two past the index, up to the end of tmp
			// (which is two elements shorter than the old list)
			if (index < tmp.length)
				System.arraycopy( listenerList, index + 2, tmp, index, tmp.length - index );

			// set the listener array to the new array or null
			listenerList = (tmp.length == 0) ? Utils.EMPTY_OBJECT_ARRAY : tmp;
		}
	}
	/**
	 * Removes the listener as a listener of the specified type.
	 *
	 * @param listener the listener to be removed
	 * @param tags the type of the listener to be removed
	 */
	public synchronized void remove (C listener, T ... tags) {
		if (Utils.isEmpty( tags ))
			remove( listener, (T)null );
		else
			for (T tag : tags)
				remove( listener, tag );

	}

	/**
	 * Remove all listeners.
	 */
	public synchronized void clear () {
		listenerList = Utils.EMPTY_OBJECT_ARRAY;
	}

	/**
	 * Is l on the list?
	 */
	private int indexOf (C l, T t) {
		for (int i = listenerList.length - 2; i >= 0; i -= 2) {
			if (equals(listenerList[i], t) && (listenerList[i + 1].equals( l )))
				return i;
		}

		return -1;
	}

	private static boolean equals (Object t1 , Object t2) {
		return t1 == t2 || (t1 != null && t1.equals( t2 ));
	}

	// Serialization support.

	private void writeObject (ObjectOutputStream s) throws IOException {
		Object[] lList = listenerList;
		s.defaultWriteObject();
		s.writeObject( type.getName() );

		// Save the non-null event listeners:
		for (int i = 0; i < lList.length; i += 2) {
			Object t = lList[i];
			Object l = lList[i + 1];
			if (l instanceof Serializable && (t instanceof Serializable || t == null)) {
				s.writeObject( l );
				s.writeObject( t );
			}
		}

		s.writeObject( null );
	}

	@SuppressWarnings( "unchecked" )
	private void readObject (ObjectInputStream s) throws IOException, ClassNotFoundException {
		listenerList = Utils.EMPTY_OBJECT_ARRAY;
		s.defaultReadObject();

		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		type = (Class<C>)Class.forName( (String)s.readObject(), true, cl );

		for (C l=(C)s.readObject(); l != null; l = (C)s.readObject())
			add( l, (T)s.readObject() );
	}

	/**
	 * Returns a string representation of the EventListenerList.
	 */
	public String toString () {
		Object[] lList = listenerList;
		StringBuilder s = new StringBuilder( "TypedListenerList: (" );
		s.append( type.getName() ).append(") ");
		s.append(lList.length / 2).append( " listeners: " );
		for (int i = 0; i <= lList.length - 2; i += 2) {
			s.append(" tag ").append( lList[i] );
			s.append(" listener ").append( lList[i + 1]);
		}
		return s.toString();
	}
}
