/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.util;

import java.io.PrintStream;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * System.out/System.err implementation of {@link Log} class.
 *
 * @author Ahmed Shakil
 * @date 09-15-2013
 */
public class Syslog extends Log {

	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");

	/**
	 * Send a DEBUG log message and log the exception.
	 */
	@Override
	protected final void debug (String tag, String msg, Throwable e) {
		print( System.out, Level.DEBUG, tag, msg, e );
	}

	/**
	 * Send a WARN log message and log the exception.
	 */
	@Override
	protected final void warn (String tag, String msg, Throwable e) {
		print( System.err, Level.WARN, tag, msg, e );
	}

	/**
	 * Send a ERROR log message and log the exception.
	 */
	@Override
	protected final void error (String tag, String msg, Throwable e) {
		print( System.err, Level.ERROR, tag, msg, e );
	}

	/**
	 * Send a INFO log message and log the exception.
	 */
	@Override
	protected final void info (String tag, String msg, Throwable e) {
		print( System.out, Level.INFO, tag, msg, e );
	}

	/**
	 * Send a VERBOSE log message and log the exception.
	 */
	@Override
	protected final void verbose (String tag, String msg, Throwable e) {
		print( System.out, Level.VERBOSE, tag, msg, e );
	}

	/**
	 * Common print implementation.
	 */
	protected void print (PrintStream out, Level level, String tag, String msg, Throwable e) {
		out.println( getTimeStamp() + ": " + level + "/" + tag + ": " + msg + (e==null ? "" : '\n' + getStackTraceString( e ) ) );
	}

	/**
	 * Timestamp string format.
	 */
	private static String getTimeStamp () {
		return TIME_FORMAT.format( new Date() );
	}

	/**
	 * Handy function to get a loggable stack trace from a Throwable
	 * @param tr An exception to log
	 */
	public static String getStackTraceString (Throwable tr) {
		if (tr == null) {
			return "";
		}

		// This is to reduce the amount of log spew that apps do in the non-error
		// condition of the network being unavailable.
		Throwable t = tr;
		while (t != null) {
			if (t instanceof UnknownHostException) {
				return "";
			}
			t = t.getCause();
		}

		return Utils.getStackTrace( tr );
	}

}
