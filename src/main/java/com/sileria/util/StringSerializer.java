/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.util;

import java.io.*;

/**
 * StringSerializer.
 *
 * @author Ahmed Shakil
 * @date 09-30-2012
 */
public class StringSerializer implements ObjectSerializer<String> {

	private String enc;

	private static final int BUFF_SIZE  = 5120;

	/**
	 * Constructor, default.
	 */
	public StringSerializer () {
	}

	/**
	 * Constructor specified string encoding.
	 * @param enc encoding
	 */
	public StringSerializer (String enc) {
		this.enc = enc;
	}

	/**
	 * Write a string to the specified stream. You are responsible for closing the <code>OutputStream</code>.
	 * @param os output stream to write to
	 * @param str String to write
	 * @throws java.io.IOException in case of IO errors
	 */
	public boolean write (OutputStream os, String str) throws IOException {

		OutputStreamWriter out = null;
		try {
			out = enc == null ? new OutputStreamWriter( os ) : new OutputStreamWriter( os, enc );
			out.write( str );
		}
		finally {
			IO.close( out );
		}
		return true;
	}

	/**
	 * Read object from to the stream. You are responsible for closing the <code>InputStream</code>.
	 * @param in output stream to write to
	 * @return object object to write
	 * @throws java.io.IOException in case of IO errors
	 */
	public String read (InputStream in) throws IOException {
		InputStreamReader reader = enc == null ? new InputStreamReader( in ) : new InputStreamReader( in, enc );

		int available = in.available();
		int chunkSize = Utils.clamp( Math.min( 32, BUFF_SIZE ), Math.max( 32, BUFF_SIZE ), available );
		StringBuilder builder = new StringBuilder( Math.max( chunkSize, available ) );
		char[] chunk = new char[chunkSize];

		int result;
		while ((result = reader.read( chunk, 0, chunk.length )) != -1) {
			builder.append( chunk, 0, result );
		}

		return builder.toString();
	}
}


