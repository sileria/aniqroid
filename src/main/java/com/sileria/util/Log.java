/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.util;

/**
 * An abstraction layer for logging mechanism to be shared between platforms.
 * e.g. if you are writing code to share between Android and other Java based
 * platforms then this class can be used as the base class to any logging
 * framework that exists on each of those platfoms.
 *
 * @author Ahmed Shakil
 * @date Feb 16, 2009
 */
public abstract class Log {

    private static Log log = null;

	protected Level level = Level.VERBOSE;

	/**
	 * Set the global logger.
	 * @param logger a subclass of <code>Log</code> class.
	 */
	public static void setLogger (Log logger) {
		log = logger;
	}

	/**
	 * Set the global logger and the debug level
	 * @param logger a subclass of <code>Log</code> class.
	 * @param level - debug log Level to be set
	 */
	public static void setLogger (Log logger, Level level) {
		setLogger( logger );
		if (logger != null)
			logger.setLevel( level );
	}

    /**
     * Constructor, protected.
     */
    protected Log () {
        log = this;
    }

    /**
     * Send a DEBUG log message and log the exception.
     */
    public static void d (String tag, String msg, Throwable e) {
        if (log != null && log.level.log( Level.DEBUG ))
            log.debug( tag, msg, e );
    }

    /**
     * Send a DEBUG log message.
     */
    public static void d (String tag, String msg) {
        if (log != null && log.level.log( Level.DEBUG ))
            log.debug( tag, msg, null );
    }

	/**
	 * Send a WARN log message and log the exception.
	 */
	public static void w (String tag, String msg, Throwable e) {
		if (log != null && log.level.log( Level.WARN ))
			log.warn( tag, msg, e );
	}

	/**
	 * Send a WARN log message.
	 */
	public static void w (String tag, String msg) {
		if (log != null && log.level.log( Level.WARN ))
			log.warn( tag, msg, null );
	}

	/**
     * Send a ERROR log message and log the exception.
     */
    public static void e (String tag, String msg, Throwable e) {
		if (log != null && log.level.log( Level.ERROR ))
            log.error( tag, msg, e );
    }

    /**
     * Send a ERROR log message.
     */
    public static void e (String tag, String msg) {
		if (log != null && log.level.log( Level.ERROR ))
            log.error( tag, msg, null );
    }

    /**
     * Send a INFO log message and log the exception.
     */
    public static void i (String tag, String msg, Throwable e) {
		if (log != null && log.level.log( Level.INFO ))
            log.info( tag, msg, e );
    }

    /**
     * Send a INFO log message.
     */
    public static void i (String tag, String msg) {
		if (log != null && log.level.log( Level.INFO ))
            log.info( tag, msg, null );
    }

    /**
     * Send a VERBOSE log message and log the exception.
     */
    public static void v (String tag, String msg, Throwable e) {
		if (log != null && log.level.log( Level.VERBOSE ))
            log.verbose( tag, msg, e );
    }

    /**
     * Send a VERBOSE log message.
     */
    public static void v (String tag, String msg) {
		if (log != null && log.level.log( Level.VERBOSE ))
            log.verbose( tag, msg, null );
    }

	/**
	 * Method to set the debug message Level.
	 * Any message with level equal or greater than this level will be logged.
	 *
	 * @param level - debug log Level to be set
	 */
	public void setLevel (Level level) {
		this.level = Utils.defaultIfNull( level, Level.VERBOSE );
	}

	/**
     * Send a DEBUG log message and log the exception.
     */
    protected abstract void debug (String tag, String msg, Throwable e);

    /**
     * Send a ERROR log message and log the exception.
     */
    protected abstract void error (String tag, String msg, Throwable e);

    /**
     * Send a INFO log message and log the exception.
     */
    protected abstract void info (String tag, String msg, Throwable e);

    /**
     * Send a VERBOSE log message and log the exception.
     */
    protected abstract void verbose (String tag, String msg, Throwable e);

    /**
     * Send a WARN log message and log the exception.
     */
    protected abstract void warn (String tag, String msg, Throwable e);

	/**
	 * Enumeration for the Message Levels
	 */
	public enum Level {
		ALL     ( 1 ),
		VERBOSE ( 2 ),
		DEBUG   ( 3 ),
		INFO    ( 4 ),
		WARN    ( 5 ),
		ERROR   ( 6 ),
		ASSERT  ( 7 ),
		OFF     (0xFF);

		public final int priority;

		private Level (int priority) {
			this.priority = priority;
		}

		boolean log (Level l) {
			return l.priority >= priority;
		}
	}

}