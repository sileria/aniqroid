/*
 * Copyright (c) 2001 - 2015 Sileria, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

package com.sileria.util;

import java.io.Serializable;

/**
 * TypedEventDispatcher; convenience base class to add/remove
 * listeners to an internal {@link TypedListenerList} and
 * being able to fire events in the implementation.
 *
 * @author Ahmed Shakil
 * @date Aug 19, 2013
 * @param <L> listener type
 * @param <E> event type
 */
public class TypedEventDispatcher<L, E> implements Serializable {

	private static final long serialVersionUID = 6641315501000226425L;

	protected final TypedListenerList<L, E> listeners;

	public TypedEventDispatcher (Class<L> clazz) {
		listeners = new TypedListenerList<L, E>( clazz );
	}

	/**
	 * Add specified <code>listener</code>.
	 */
	public void addListener (L listener, E event) {
		listeners.add( listener, event );
	}

	/**
	 * Add specified <code>listener</code>.
	 */
	public void addListener (L listener, E ... events) {
		listeners.add( listener, events );
	}

	/**
	 * Remove specified <code>listener</code>.
	 */
	public void removeListener (L listener, E event) {
		listeners.remove( listener, event );
	}

	/**
	 * Remove specified <code>listener</code>.
	 */
	public void removeListener (L listener, E ... events) {
		listeners.remove( listener, events );
	}

	/**
	 * Remove all listeners.
	 */
	public void clear () {
		listeners.clear();
	}
}
